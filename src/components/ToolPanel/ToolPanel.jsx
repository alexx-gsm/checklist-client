import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { string, func, bool, object } from 'prop-types'
// material-ui components
import { Grid, TextField, InputAdornment } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add'
import ArrowBack from '@material-ui/icons/ArrowBack'
import Autorenew from '@material-ui/icons/Autorenew'
import IconSearch from '@material-ui/icons/Search'
import IconRemove from '@material-ui/icons/RemoveCircleOutline'
// react icon kit
import { Icon } from 'react-icons-kit'
import { download2 } from 'react-icons-kit/icomoon/download2'
// useful tools
import isEmpty from '../../helpers/is-empty'

const ToolPanel = ({
  link,
  title,
  label,
  returnButton,
  upload,
  onReload,
  loading,
  color,
  search,
  classes
}) => {
  return (
    <Grid container className={classes.tools}>
      <Grid item xs={12} sm={6} className={classes.gridTools}>
        {!isEmpty(search) ? (
          <Grid container alignItems='center' wrap='nowrap'>
            <Grid item xs={10}>
              <TextField
                id='search'
                type='text'
                value={isEmpty(search.filter) ? '' : search.filter}
                onChange={search.onSearch}
                className={classes.select}
                margin='dense'
                fullWidth
                variant='outlined'
                label={label ? label : ''}
                InputProps={{
                  classes: {
                    root: classes.search
                  },
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconSearch />
                    </InputAdornment>
                  )
                }}
              />
            </Grid>
            <Grid item xs={2} style={{ padding: '0 8px' }}>
              <IconButton
                aria-label='Clear'
                className={classes.buttonClearFilter}
                onClick={search.handleFilterClear}
                disabled={!search.filter}
              >
                <IconRemove fontSize='large' />
              </IconButton>
            </Grid>
          </Grid>
        ) : (
          <Typography variant='display1' className={classes.title}>
            {title}
          </Typography>
        )}
      </Grid>
      <Grid container item xs={12} sm={6} className={classes.gridTools}>
        {onReload && (
          <div className={classes.wrapper}>
            {loading ? (
              <CircularProgress
                size={44}
                color='secondary'
                className={classes.fabProgress}
              />
            ) : (
              <IconButton
                onClick={onReload}
                className={classes.button}
                aria-label='Reload'
              >
                <Autorenew />
              </IconButton>
            )}
          </div>
        )}
        {upload && (
          <Fab
            size='small'
            aria-label='Back'
            className={classes.buttonUpload}
            to={`${link}/upload`}
            component={Link}
            color='secondary'
          >
            <Icon icon={download2} />
          </Fab>
        )}
        {returnButton ? (
          <Fab
            size='small'
            aria-label='Back'
            className={classes.buttonReturn}
            to={`${link}`}
            component={Link}
            color='secondary'
          >
            <ArrowBack />
          </Fab>
        ) : (
          <Fab
            size='small'
            aria-label='Add'
            className={classes.buttonAdd}
            to={`${link}/edit`}
            component={Link}
            color='primary'
          >
            <AddIcon />
          </Fab>
        )}
      </Grid>
    </Grid>
  )
}

ToolPanel.defaultProps = {
  link: '',
  title: '',
  upload: false,
  color: 'default',
  returnButton: false,
  loading: false
}

ToolPanel.propTypes = {
  link: string,
  title: string,
  returnButton: bool,
  onReload: func,
  loading: bool.isRequired,
  classes: object.isRequired
}

export default ToolPanel
