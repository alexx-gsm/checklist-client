import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
import {
  getDepartmentById,
  storeDepartment,
  saveDepartment,
  updateDepartment
} from '../../../redux/modules/department'
import { getCompanies } from '../../../redux/modules/company'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import DepartmentEdit from './DepartmentEdit'

export default compose(
  connect(
    ({ departmentStore, companyStore }) => {
      const { department, error, loading } = departmentStore
      const { companies } = companyStore
      return {
        department,
        companies,
        error,
        loading
      }
    },
    {
      getDepartmentById,
      storeDepartment,
      saveDepartment,
      updateDepartment,
      getCompanies
    }
  ),
  withState('isColorPicker', 'setIsColorPicker', false),
  withRouter,
  withHandlers({
    onReload: ({ getDepartmentById, match }) => () =>
      getDepartmentById(match.params.id),
    onChange: ({ department, storeDepartment }) => index => event => {
      if (index === 'color') {
        storeDepartment({
          ...department,
          [index]: event.hex
        })
      } else {
        storeDepartment({
          ...department,
          [index]: event.target.value
        })
      }
    },
    onSave: ({
      department,
      saveDepartment,
      updateDepartment,
      history,
      basePath
    }) => () => {
      if (department.id) {
        updateDepartment(department, () => history.push(basePath))
      } else {
        saveDepartment(department, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      this.props.getCompanies()
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getDepartmentById(id)
      }
    }
  }),
  withStyles(theme => ({ ...CardStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(DepartmentEdit)
