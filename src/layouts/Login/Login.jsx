import React from 'react'
// material-ui components
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
// @material-ui/icons
import CloseIcon from '@material-ui/icons/Close'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import AccountCircle from '@material-ui/icons/AccountCircle'
import isEmpty from '../../helpers/is-empty'

const Login = ({
  classes,
  userData,
  onChange,
  onLogin,
  isOpen,
  handleClose,
  error
}) => {
  const { email, password } = userData
  return (
    <Grid
      container
      className={classes.root}
      alignItems='center'
      justify='center'
    >
      <Paper className={classes.container} elevation={1}>
        <form className={classes.form} noValidate autoComplete='off'>
          <Grid container justify='center'>
            <AccountCircle className={classes.avatar} />
          </Grid>

          <TextField
            required
            id='required'
            label='Login'
            className={classes.textField}
            margin='normal'
            value={email || ''}
            name='email'
            onChange={onChange}
            error={Boolean(error.login)}
          />

          <TextField
            required
            id='password-input'
            label='Password'
            className={classes.textField}
            type='password'
            autoComplete='current-password'
            margin='normal'
            name='password'
            value={password || ''}
            onChange={onChange}
            error={Boolean(error.login)}
          />

          <Button
            variant='contained'
            color='primary'
            className={classes.button}
            onClick={onLogin}
          >
            Login
          </Button>
        </form>
      </Paper>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        open={Boolean(error.login)}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <SnackbarContent
          className={classes.ErrorBar}
          aria-describedby='message-id'
          message={<span id='message-id'>Не верные логин и/или пароль</span>}
          action={[
            <IconButton
              key='close'
              aria-label='Close'
              color='inherit'
              className={classes.close}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
      </Snackbar>
    </Grid>
  )
}

Login.defaultProps = {
  error: {}
}
export default Login
