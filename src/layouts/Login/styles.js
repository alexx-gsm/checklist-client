export default theme => ({
  root: {
    height: '70%'
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  avatar: {
    color: '#ddd',
    fontSize: '80px'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    padding: '30px',
    background: '#fbfbfb'
  },
  textField: {
    flexBasis: 'auto'
  },
  margin: {
    margin: theme.spacing.unit
  },
  button: {
    marginTop: '20px'
  },
  ErrorBar: {
    background: theme.palette.error.dark
  }
})
