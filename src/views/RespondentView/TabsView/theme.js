import { createMuiTheme } from '@material-ui/core/styles'
import { teal } from '@material-ui/core/colors'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#00796b'
    },
    secondary: {
      main: '#f57c00',
      contrastText: '#fff'
    },
    extraCardColor: teal
  }
})
