import React from 'react'
import { object, func } from 'prop-types'
// material-ui components
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Avatar from '@material-ui/core/Avatar'
import Hidden from '@material-ui/core/Hidden'
import Divider from '@material-ui/core/Divider'
// @material-ui/icons
import MenuIcon from '@material-ui/icons/Menu'
import IconButton from '@material-ui/core/IconButton'
import AccountCircle from '@material-ui/icons/AccountCircle'

const Appbar = ({
  auth,
  classes,
  onSidebarToggle,
  anchorEl,
  onOpen,
  onClose,
  onLogin,
  onLogout
}) => {
  const open = Boolean(anchorEl)

  const { isAuthenticated, user } = auth

  return (
    <AppBar className={classes.appBar}>
      <Toolbar className={classes.toolBar}>
        <IconButton
          color='inherit'
          aria-label='Open drawer'
          onClick={onSidebarToggle}
          className={classes.navIconHide}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant='title' color='inherit' noWrap>
          Опрос
        </Typography>
        {/* {isAuthenticated && (
          <Grid item>
            <Grid container alignItems='center'>
              <Hidden smDown>
                <Typography variant='title' color='inherit' noWrap>
                  {user.displayName || user.email}
                </Typography>
              </Hidden>
              {user.avatar ? (
                <Avatar
                  alt='Remy Sharp'
                  src={user.avatar}
                  className={classes.avatar}
                  onClick={onOpen}
                />
              ) : (
                <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup='true'
                  onClick={onOpen}
                  color='inherit'
                >
                  <AccountCircle />
                </IconButton>
              )}
            </Grid>
            <Menu
              id='menu-appbar'
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right'
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right'
              }}
              open={open}
              onClose={onClose}
            >
              <MenuItem onClick={onClose}>Profile</MenuItem>
              <Divider />
              <MenuItem onClick={onLogout}>Logout</MenuItem>
            </Menu>
          </Grid>
        )} */}
      </Toolbar>
    </AppBar>
  )
}

Appbar.propTypes = {
  classes: object.isRequired,
  onSidebarToggle: func.isRequired
}

export default Appbar
