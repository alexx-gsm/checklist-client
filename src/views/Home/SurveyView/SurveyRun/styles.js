export default theme => ({
  FioTypo: {
    paddingBottom: '20px',
    marginTop: '-30px'
  },
  QuestionPaper: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    marginBottom: '3px'
  },
  QuestionTitle: {
    padding: '12px 17px',
    lineHeight: 1.2,
    color: theme.palette.common.white,
    background: theme.palette.grey[600]
  },
  GridFinishButton: {
    marginTop: 'auto',
    marginBottom: '20px'
  },

  GridStepperContainer: {},
  GridStepperLeft: {
    position: ' absolute',
    top: '50vh',
    left: '10px',
    zIndex: 5
  },
  GridStepperRight: {
    position: 'absolute',
    top: '50vh',
    right: '10px',
    zIndex: 5
  }
  // GridStepperContent: {
  //   flex: 1
  // }
})
