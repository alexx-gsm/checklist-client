import blueGrey from '@material-ui/core/colors/blueGrey'

export default theme => ({
  buttonClearFilter: {
    height: '20px',
    width: '20px',
    boxSizing: 'content-box',
    color: 'rgba(255, 255, 255, 0.8)'
  },
  underlineWhite: {
    '&:before': {
      borderBottomColor: 'rgba(255, 255, 255, 0.3)'
    },
    '&:after': {
      borderBottomColor: theme.palette.common.white
    },
    '&:hover:not(.disabled):not(.focused):not(.error)': {
      '&:before': {
        borderBottomColor: theme.palette.common.white
      }
    }
  },
  search: {
    color: 'rgba(255, 255, 255, 0.8)'
  },
  Card: {
    maxWidth: '940px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    padding: '5px 10px',
    color: theme.palette.common.white,
    '&.blueGrey': {
      background: `linear-gradient(60deg, ${blueGrey[500]}, ${blueGrey[600]})`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'nowrap',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  // TABLE
  Table: {
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  TableRow: {
    '& td': { background: blueGrey[100] },
    '&:nth-child(odd) td': {
      background: blueGrey[50]
    },
    '&:hover td': { background: blueGrey[200] }
  },
  cellData: {
    width: '100%'
  },
  cellContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      alignItems: 'flex-start',
      marginLeft: 0,
      flexDirection: 'column'
    }
  },
  cellSubItem: {
    marginLeft: '10px',
    paddingLeft: '10px',
    borderLeft: '1px solid',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0
    }
  },
  //
  buttonMore: {
    padding: 0,
    width: '32px',
    minWidth: '32px',
    borderRadius: '50%'
  },
  buttonMoreLabel: {}
})
