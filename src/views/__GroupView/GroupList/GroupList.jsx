import React from 'react'
// Components
import ToolPanel from '../../../components/ToolPanel'

const GroupList = () => {
  return (
    <div>
      <ToolPanel title='Группы' color='blue' link='/admin/groups' />
    </div>
  )
}

export default GroupList
