export default theme => ({
  CellBordered: {
    width: '100%',
    padding: '5px 10px'
  },
  typoInn: {
    marginRight: '10px',
    paddingRight: '10px',
    borderRight: '1px solid'
  }
})
