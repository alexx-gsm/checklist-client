import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getResearches, clearResearch } from '../../../redux/modules/research'
import { getCompanies } from '../../../redux/modules/company'
import { getDepartments } from '../../../redux/modules/department'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

import ResearchList from './ResearchList'

const FILTER_KEY = 'RESEARCH'

export default compose(
  connect(
    ({ researchStore, filterStore, companyStore, departmentStore }) => {
      const { researches, loading, error } = researchStore
      const { companies } = companyStore
      const { departments } = departmentStore
      const { filter } = filterStore
      const filteredResearches =
        filter[FILTER_KEY] === ''
          ? researches
          : Object.keys(researches).reduce((acc, key) => {
              const item = researches[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        researches: filteredResearches,
        companies,
        departments,
        filter,
        loading: loading || companyStore.loading || departmentStore.loading,
        error
      }
    },
    { getResearches, clearResearch, getCompanies, getDepartments }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getResearches }) => () => getResearches(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearResearch()
      this.props.getResearches()
      this.props.getCompanies()
      this.props.getDepartments()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(ResearchList)
