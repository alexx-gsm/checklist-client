import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getActivitys, clearActivity } from '../../../redux/modules/activity'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

import ActivityList from './ActivityList'

const FILTER_KEY = 'ACTIVITY'

export default compose(
  connect(
    ({ activityStore, filterStore }) => {
      const { activities, loading, error } = activityStore
      const { filter } = filterStore
      const filteredActivitys =
        filter[FILTER_KEY] === ''
          ? activities
          : Object.keys(activities).reduce((acc, key) => {
              const item = activities[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        activities: filteredActivitys,
        loading,
        error
      }
    },
    { getActivitys, clearActivity }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getActivitys }) => () => getActivitys(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearActivity()
      this.props.getActivitys()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(ActivityList)
