import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// Layouts
import Landing from '../layouts/Landing'
import Home from '../layouts/Home'
import Admin from '../layouts/Admin'
import Login from '../layouts/Login'
import Survey from '../layouts/Survey'
// Private Route (only for authorized users)
import PrivateRoute from './PrivateRoute'

const indexRoutes = [
  { path: '/admin', component: Admin },
  { path: '/home', component: Home },
  { path: '/survey', component: Survey }
]

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Landing} />
          <Route exact path='/login' component={Login} />
          {indexRoutes.map((prop, key) => (
            <PrivateRoute
              path={prop.path}
              component={prop.component}
              key={key}
            />
          ))}
        </Switch>
      </Router>
    )
  }
}

export default Routes
