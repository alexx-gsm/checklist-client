import { compose } from 'recompose'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Home from './Home'
// styles
import styles from './styles'

export default compose(
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Home)
