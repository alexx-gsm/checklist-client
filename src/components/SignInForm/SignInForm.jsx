import React from "react";

const SignInForm = ({ email, password, onChange, onSubmit }) => {
  return (
    <div>
      <form action="">
        <h2>Вход</h2>
        <input type="email" name="email" id="email" />
        <input type="password" name="password" id="password" />
        <button type="submit">Отправить</button>
      </form>
    </div>
  );
};

export default SignInForm;
