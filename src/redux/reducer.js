import { combineReducers } from 'redux'
import sidebarReducer, { moduleName as sidebarModule } from './modules/sidebar'
import authReducer, { moduleName as authModule } from './modules/auth'
// RESEARCH GROUP
import researchReducer, {
  moduleName as researchModule
} from './modules/research'
// RESPONDENT GROUP
import respondentReducer, {
  moduleName as respondentModule
} from './modules/respondent'
import companyReducer, { moduleName as companyModule } from './modules/company'
import departmentReducer, {
  moduleName as departmentModule
} from './modules/department'
import activityReducer, {
  moduleName as activityModule
} from './modules/activity'
import staffReducer, { moduleName as staffModule } from './modules/staff'
import qtypeReducer, { moduleName as qtypeModule } from './modules/qtype'
import groupReducer, { moduleName as groupModule } from './modules/group'
import userReducer, { moduleName as userModule } from './modules/user'
import roleReducer, { moduleName as roleModule } from './modules/role'
import filterReducer, { moduleName as filterModule } from './modules/filter'
// SURVEY GROUP
import surveyReducer, { moduleName as surveyModule } from './modules/survey'
import answerReducer, { moduleName as answerModule } from './modules/answer'
import criteriaReducer, {
  moduleName as criteriaModule
} from './modules/criteria'
import questionReducer, {
  moduleName as questionModule
} from './modules/question'

export default combineReducers({
  [sidebarModule]: sidebarReducer,
  [authModule]: authReducer,
  [researchModule]: researchReducer,
  [respondentModule]: respondentReducer,
  [companyModule]: companyReducer,
  [departmentModule]: departmentReducer,
  [activityModule]: activityReducer,
  [staffModule]: staffReducer,
  [qtypeModule]: qtypeReducer,
  [groupModule]: groupReducer,
  [userModule]: userReducer,
  [roleModule]: roleReducer,
  [filterModule]: filterReducer,
  [surveyModule]: surveyReducer,
  [answerModule]: answerReducer,
  [criteriaModule]: criteriaReducer,
  [questionModule]: questionReducer
})
