import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { getRoles, clearRole } from '../../../redux/modules/role'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'

import RoleList from './RoleList'
export default compose(
  connect(
    ({ roleStore }) => {
      const { roles, loading, error } = roleStore
      return {
        roles,
        loading,
        error
      }
    },
    { getRoles, clearRole }
  ),
  withRouter,
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getRoles }) => () => getRoles(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearRole()
      this.props.getRoles()
    }
  }),
  withStyles(styles, { withTheme: true })
)(RoleList)
