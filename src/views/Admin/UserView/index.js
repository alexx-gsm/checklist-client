import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const UserList = lazy(() => import('./UserList'))
const UserEdit = lazy(() => import('./UserEdit'))

const basePath = '/admin/users'

const UserView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <UserList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <UserEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default UserView
