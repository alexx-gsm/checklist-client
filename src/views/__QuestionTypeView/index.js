import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'

const QuestionTypeList = lazy(() => import('./QuestionTypeList'))
const QuestionTypeEdit = lazy(() => import('./QuestionTypeEdit'))

const basePath = '/admin/questiontypes'

const QuestionTypeView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <QuestionTypeList basePath={basePath} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <QuestionTypeEdit basePath={basePath} />}
    />
  </Suspense>
)

export default QuestionTypeView
