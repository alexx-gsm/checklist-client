import deepOrange from '@material-ui/core/colors/deepOrange'

export default theme => ({
  Card: {
    maxWidth: '740px',
    margin: '40px 0',
    padding: '0 0 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    margin: '0 20px',
    padding: '5px 10px',
    color: theme.palette.common.white,
    '&.orange': {
      background: `linear-gradient(60deg, ${deepOrange[500]}, ${
        deepOrange[600]
      })`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  // TABLE
  Table: {
    margin: '0 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  TableRow: {
    '& td': { background: deepOrange[100] },
    '&:nth-child(odd) td': {
      background: deepOrange[50]
    },
    '&:hover td': { background: deepOrange[200] }
  }
})
