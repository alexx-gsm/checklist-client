import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getSurveyById,
  storeSurvey,
  saveSurvey,
  updateSurvey
} from '../../../redux/modules/survey'
import { getQuestions } from '../../../redux/modules/question'
import { getAnswers } from '../../../redux/modules/answer'
// Drag and Drop Helpers
import arrayMove from 'array-move'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import СardStyles from '../../../styles/CardStyles'
import CardTableStyles from '../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import SurveyEdit from './SurveyEdit'

export default compose(
  connect(
    ({ surveyStore, questionStore }) => {
      const { survey, error, loading } = surveyStore
      const { questions } = questionStore
      const restQuestions =
        !isEmpty(questions) && !isEmpty(survey.questions)
          ? Object.keys(questions).reduce(
              (acc, q) =>
                survey.questions.indexOf(q) === -1
                  ? { ...acc, [q]: questions[q] }
                  : acc,
              {}
            )
          : {}
      return {
        survey,
        questionList: questions,
        restQuestions,
        error,
        loading: loading || questionStore.loading
      }
    },
    {
      getSurveyById,
      storeSurvey,
      saveSurvey,
      updateSurvey,
      getQuestions
    }
  ),
  withRouter,
  withState('showSelect', 'setShowSelect', false),
  withHandlers({
    onReload: ({ getSurveyById, match }) => () =>
      getSurveyById(match.params.id),
    onChange: ({ survey, storeSurvey }) => index => event => {
      if (index === 'checkBox') {
        storeSurvey({
          ...survey,
          [event.target.name]: event.target.checked
        })
      } else {
        storeSurvey({
          ...survey,
          [index]: event.target.value
        })
      }
    },
    onChangeItem: ({ survey, storeSurvey, setShowSelect }) => event => {
      setShowSelect(false)
      storeSurvey({
        ...survey,
        questions: [...survey.questions, event.target.value]
      })
    },
    onAddRow: ({ setShowSelect }) => () => {
      setShowSelect(true)
    },
    onDeleteRow: ({ survey, storeSurvey, setShowSelect }) => index => () => {
      setShowSelect(false)
      storeSurvey({
        ...survey,
        questions: [
          ...survey.questions.slice(0, index),
          ...survey.questions.slice(index + 1)
        ]
      })
    },
    onSortEnd: ({ survey, storeSurvey }) => ({ newIndex, oldIndex }) => {
      storeSurvey({
        ...survey,
        questions: arrayMove(survey.questions, oldIndex, newIndex)
      })
    },
    onSave: ({ survey, saveSurvey, updateSurvey, history, basePath }) => () => {
      if (survey.id) {
        updateSurvey(survey, () => history.push(basePath))
      } else {
        saveSurvey(survey, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getSurveyById(id)
      }
      this.props.getQuestions()
    }
  }),
  withStyles(
    theme => ({
      ...СardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(SurveyEdit)
