export default theme => ({
  ResearchList: {
    padding: 0
  },
  CellBordered: {
    width: '100%',
    padding: '5px 10px'
  },
  Title: {
    borderBottom: `1px dotted ${theme.palette.grey[400]}`,
    marginBottom: '5px'
  }
})
