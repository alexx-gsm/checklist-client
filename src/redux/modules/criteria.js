import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'criteriaStore'

// --- ACTIONS ---
export const CRITERIA_LOADING = `${appName}/${moduleName}/CRITERIA_LOADING`
export const CRITERIA_STORE = `${appName}/${moduleName}/CRITERIA_STORE`
export const CRITERIA_SAVE = `${appName}/${moduleName}/CRITERIA_SAVE`
export const CRITERIA_GET_ALL = `${appName}/${moduleName}/CRITERIA_GET_ALL`
export const CRITERIA_UPLOAD = `${appName}/${moduleName}/CRITERIA_UPLOAD`
export const CRITERIA_ERROR = `${appName}/${moduleName}/CRITERIA_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  criterias: {},
  criteria: {},
  uploadedCriterias: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case CRITERIA_LOADING:
      return state.set('loading', true)
    case CRITERIA_STORE:
      return state
        .set('loading', false)
        .set('criteria', payload)
        .set('error', {})
    case CRITERIA_GET_ALL:
      return state
        .set('loading', false)
        .set('criterias', payload)
        .set('error', {})
    case CRITERIA_UPLOAD:
      return state
        .set('loading', false)
        .set('uploadedCriterias', payload)
        .set('error', {})
    case CRITERIA_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---

/**
 * * STORE CRITERIA
 */
export const storeCriteria = payload => ({
  type: CRITERIA_STORE,
  payload
})

/**
 * * SAVE CRITERIA
 */
export const saveCriteria = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/criterias`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: CRITERIA_ERROR,
        error: error.response.data
      })
    })
}

/**
 * * UPDATE CRITERIA
 */
export const updateCriteria = (payload, cb) => dispatch => {
  axios
    .put(`${HOST}/api/criterias`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: CRITERIA_ERROR,
        error: error.response.data
      })
    })
}

/**
 * * GET ALL COMPANIES
 */
export const getCriterias = () => dispatch => {
  dispatch({
    type: CRITERIA_LOADING
  })
  axios
    .post(`${HOST}/api/criterias/all`)
    .then(res => {
      dispatch({
        type: CRITERIA_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: CRITERIA_ERROR,
        error: error
      })
    })
}

/**
 * * GET CRITERIA BY ID
 */
export const getCriteriaById = id => dispatch => {
  dispatch({
    type: CRITERIA_LOADING
  })
  axios
    .post(`${HOST}/api/criterias/${id}`)
    .then(res => {
      dispatch({
        type: CRITERIA_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: CRITERIA_ERROR,
        error
      })
    )
}

/**
 * * CRITERIA_UPLOAD
 */
export const uploadCriterias = payload => dispatch => {
  if (payload.file) {
    const formData = new FormData()
    Object.keys(payload).map(key => formData.append(key, payload[key]))
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    axios
      .put(`${HOST}/api/criterias/upload`, formData, config)
      .then(res =>
        dispatch({
          type: CRITERIA_UPLOAD,
          payload: res.data
        })
      )
      .catch(error => {
        dispatch({
          type: CRITERIA_ERROR,
          error
        })
      })
  }
}

/**
 * * CLEAR CRITERIA
 */
export const clearCriteria = () => ({
  type: CRITERIA_STORE,
  payload: {}
})

/**
 * * CLEAR UPLOADED CRITERIAS
 */
export const clearUploadedCriterias = () => ({
  type: CRITERIA_UPLOAD,
  payload: {}
})

export default reducer
