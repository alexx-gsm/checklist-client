import { compose, withHandlers, lifecycle, withProps } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  getResearchByUser,
  storeResearch,
  saveResearch,
  updateResearch
} from '../../../../redux/modules/research'
import { getRoles } from '../../../../redux/modules/role'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import ResearchEdit from './ResearchEdit'

export default compose(
  connect(
    ({ researchStore }) => {
      const { research, error, loading } = researchStore
      return {
        research,
        error,
        loading
      }
    },
    { getResearchByUser, storeResearch, saveResearch, updateResearch }
  ),
  withRouter,
  withHandlers({
    onReload: () => () => {},
    onChange: ({ research, storeResearch }) => index => event => {
      storeResearch({
        ...research,
        [index]: event.target.value
      })
    },
    onSave: ({ research, saveResearch, history, basePath }) => () =>
      saveResearch(research, () => history.push(basePath)),
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (id) {
        this.props.getResearchByUser(id)
      }
    }
  }),
  withStyles(styles, { theme: true })
)(ResearchEdit)
