import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
import classNames from 'classnames'
import SwipeableViews from 'react-swipeable-views'
import { Paper, Typography } from '@material-ui/core'
import { AppBar, Tabs, Tab } from '@material-ui/core'
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'
// TAB Components
const ResearchList = lazy(() => import('../ResearchList'))
const ResearchEdit = lazy(() => import('../ResearchEdit'))

const getTab = (label, classes) => (
  <Tab
    key={label}
    label={label}
    classes={{
      label: classes.TabLabel
    }}
  />
)

const TabsView = ({
  tabs,
  tabIndex,
  onTabChange,
  onChangeIndex,
  classes,
  basePath,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Paper className={classNames(classes.Card, classes.CardTabs)}>
        <Paper
          className={classNames(classes.CardHeader, classes.CardTabsHeader)}
        >
          <AppBar
            position='static'
            color='primary'
            classes={{ colorPrimary: classes.AppBarPrimary }}
          >
            <Tabs
              value={tabIndex}
              onChange={onTabChange}
              classes={{
                indicator: classes.tabsIndicator
              }}
              fullWidth
              variant='fullWidth'
            >
              {tabs.map(tab => getTab(tab.label, classes))}
            </Tabs>
          </AppBar>
        </Paper>
        <SwipeableViews
          axis={'x'}
          index={tabIndex}
          onChangeIndex={onChangeIndex}
        >
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${basePath}`}
              render={() => <ResearchList theme={theme} basePath={basePath} />}
            />
            <Route
              path={`${basePath}/edit/:id?`}
              render={() => <ResearchEdit theme={theme} basePath={basePath} />}
            />
          </Suspense>
        </SwipeableViews>
      </Paper>
    </MuiThemeProvider>
  )
}

export default TabsView
