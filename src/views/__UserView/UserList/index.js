import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { getUsers, filterUsers, clearUser } from '../../../redux/modules/user'
import { getRoles } from '../../../redux/modules/role'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import styles from './styles'

import UserList from './UserList'

export default compose(
  connect(
    ({ userStore, roleStore }) => {
      const { users, filter, loading, error } = userStore
      const { roles } = roleStore
      const filteredUsers = Object.keys(users).reduce((acc, key) => {
        const user = users[key]
        const regexp = new RegExp(filter, 'i')
        return user.name.search(regexp) !== -1 ||
          user.email.search(regexp) !== -1
          ? {
              ...acc,
              [key]: user
            }
          : acc
      }, {})
      return {
        users: filteredUsers,
        roles,
        filter,
        loading,
        error
      }
    },
    { getUsers, filterUsers, clearUser, getRoles }
  ),
  withRouter,
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onSearch: ({ filterUsers }) => event => {
      filterUsers(event.target.value)
    },
    handleFilterClear: ({ filterUsers }) => () => filterUsers(''),
    onReload: ({ getUsers }) => () => getUsers(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearUser()
      this.props.getUsers()
      this.props.getRoles()
    },
    componentWillUnmount() {
      this.props.filterUsers('')
    }
  }),
  withStyles(styles, { withTheme: true })
)(UserList)
