import React from 'react'
import { NavLink } from 'react-router-dom'

const Landing = () => {
  return (
    <div>
      <h1>Landing Page</h1>
      <ul>
        <li>
          <NavLink to='/home'>Home</NavLink>
        </li>
        <li>
          <NavLink to='/admin'>Admin</NavLink>
        </li>
      </ul>
    </div>
  )
}

export default Landing
