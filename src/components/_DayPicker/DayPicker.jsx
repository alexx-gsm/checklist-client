import React from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'

import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'

import moment from 'moment'
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment'
import 'moment/locale/ru'
import './styles.css'

moment.locale('ru')

function CustomOverlay({ classNames, selectedDay, children, ...props }) {
  console.log('props', props)
  return (
    <div
      className={classNames.overlayWrapper}
      style={{ marginLeft: -100 }}
      {...props}
    >
      <div className={classNames.overlay}>
        <h3>Hello day picker!</h3>
        <p>
          <input />
          <button onClick={() => console.log('clicked!')}>button</button>
        </p>

        {children}
      </div>
    </div>
  )
}

let refTo = null

const showFromMonth = ({ from, to }) => {
  if (!from) {
    return
  }
  if (moment(to).diff(moment(from), 'months') < 2) {
    refTo.getDayPicker().showMonth(from)
  }
}

const DayPicker = ({
  range,
  setRange,
  surveyHandleRange,
  format,
  openDialog,
  setOpenDialog
}) => {
  const { from, to } = range
  const modifiers = { start: from, end: to }

  const handleFromChange = from => {
    setRange({ ...range, from })
    surveyHandleRange({ ...range, from })
  }

  const handleToChange = to => {
    setRange({ ...range, to })
    showFromMonth(range)
    surveyHandleRange({ ...range, to })
  }

  return (
    <div>
      <DayPickerInput
        overlayComponent={props => (
          <CustomOverlay open={openDialog} {...props} />
        )}
        value={moment(from).format(format)}
        placeholder='From'
        format={format}
        formatDate={formatDate}
        parseDate={parseDate}
        open={openDialog}
        dayPickerProps={{
          selectedDays: [from, { from, to }],
          disabledDays: { after: to },
          toMonth: to,
          modifiers,
          numberOfMonths: 2,
          onDayClick: () => refTo.getInput().focus(),
          open: 'fff'
        }}
        onDayChange={handleFromChange}
        keepFocus={false}
      />{' '}
      —{' '}
      <span className='InputFromTo-to'>
        <DayPickerInput
          overlayComponent={CustomOverlay}
          ref={el => (refTo = el)}
          value={moment(to).format(format)}
          placeholder='To'
          format={format}
          formatDate={formatDate}
          parseDate={parseDate}
          dayPickerProps={{
            selectedDays: [from, { from, to }],
            disabledDays: { before: from },
            modifiers,
            month: from,
            fromMonth: from,
            numberOfMonths: 2,
            locale: 'ru',
            localeUtils: MomentLocaleUtils
          }}
          onDayChange={handleToChange}
          keepFocus={false}
        />
      </span>
    </div>
  )
}

export default DayPicker
