import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  storeStageResult,
  setSurveyStage,
  storeAndUpdateSurvey
} from '../../redux/modules/survey'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Survey from './Survey'
// styles
import styles from './styles'

export default compose(
  connect(
    ({ surveyStore, questionStore, respondentStore }) => {
      const { stage, survey, result } = surveyStore
      const { questions } = questionStore
      const { respondent } = respondentStore
      return {
        survey,
        stage,
        result,
        questionList: survey ? survey.questions : [],
        questions,
        respondent
      }
    },
    { storeStageResult, setSurveyStage, storeAndUpdateSurvey }
  ),
  withHandlers({
    handleClick: ({
      stage,
      survey,
      result,
      respondent,
      setSurveyStage,
      storeStageResult,
      storeAndUpdateSurvey
    }) => index => () => {
      const { results = {} } = survey
      const id = respondent.code

      storeAndUpdateSurvey({
        ...survey,
        results: {
          ...results,
          [id]: {
            ...results[id],
            [stage]: result
          }
        }
      })
      const nextResult =
        survey &&
        survey.results &&
        survey.results[respondent.code] &&
        survey.results[respondent.code][index]
          ? survey.results[respondent.code][index]
          : []

      console.log('nextResult', nextResult)
      storeStageResult(nextResult)
      setSurveyStage(index)
    },
    handleFinish: ({
      survey,
      result,
      respondent,
      stage,
      storeAndUpdateSurvey,
      history
    }) => () => {
      const { results = {} } = survey
      const id = respondent.code

      storeAndUpdateSurvey({
        ...survey,
        results: {
          ...results,
          [id]: {
            ...results[id],
            [stage]: result
          }
        }
      })

      history.push('/home/surveys')
    }
  }),
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Survey)
