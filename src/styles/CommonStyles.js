export default theme => ({
  MT20: {
    marginTop: '20px'
  },
  MB20: {
    marginBottom: '20px'
  },
  _PageTitle: {
    color: theme.palette.grey[600],
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.2,
    padding: `${theme.spacing.unit * 2}px 0`,

    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
      padding: '0 5px'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '2rem',
      paddingTop: '5px'
    }
  },
  // FORM
  _Form: {
    [theme.breakpoints.down('sm')]: {
      padding: '0 20px'
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0 8px'
    }
  },
  // Autocomplete
  PaperAuto: {
    margin: '30px 20px',
    boxShadow: 'none'
  }
})
