import blueGrey from '@material-ui/core/colors/blueGrey'

export default theme => ({
  Card: {
    maxWidth: '640px',
    margin: '40px 0',
    padding: '0 0 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    margin: '0 20px',
    padding: '5px 10px',
    color: theme.palette.common.white,
    '&.blueGrey': {
      background: `linear-gradient(60deg, ${blueGrey[500]}, ${blueGrey[600]})`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  // TABLE
  Table: {
    margin: '0 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  TableRow: {
    '& td': { background: blueGrey[100] },
    '&:nth-child(odd) td': {
      background: blueGrey[50]
    },
    '&:hover td': { background: blueGrey[200] }
  }
})
