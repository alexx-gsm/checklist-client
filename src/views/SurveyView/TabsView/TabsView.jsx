import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
import classNames from 'classnames'
import SwipeableViews from 'react-swipeable-views'
import { Paper, Typography } from '@material-ui/core'
import { AppBar, Tabs, Tab } from '@material-ui/core'
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'
// TAB Components
const SurveyList = lazy(() => import('../SurveyList'))
const SurveyEdit = lazy(() => import('../SurveyEdit'))
const QuestionList = lazy(() => import('../QuestionList'))
const QuestionEdit = lazy(() => import('../QuestionEdit'))
const AnswerList = lazy(() => import('../AnswerList'))
const AnswerEdit = lazy(() => import('../AnswerEdit'))
const CriteriaList = lazy(() => import('../CriteriaList'))
const CriteriaEdit = lazy(() => import('../CriteriaEdit'))
const CriteriaUpload = lazy(() => import('../CriteriaUpload'))

const getTab = (label, classes) => (
  <Tab
    key={label}
    label={label}
    classes={{
      label: classes.TabLabel
    }}
  />
)

const TabsView = ({
  tabs,
  tabIndex,
  onTabChange,
  onChangeIndex,
  classes,
  basePath,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Paper className={classNames(classes.Card, classes.CardTabs)}>
        <Paper
          className={classNames(classes.CardHeader, classes.CardTabsHeader)}
        >
          <AppBar
            position='static'
            color='primary'
            classes={{ colorPrimary: classes.AppBarPrimary }}
          >
            <Tabs
              value={tabIndex}
              onChange={onTabChange}
              classes={{
                indicator: classes.tabsIndicator
              }}
              fullWidth
              variant='fullWidth'
            >
              {tabs.map(tab => getTab(tab.label, classes))}
            </Tabs>
          </AppBar>
        </Paper>
        <SwipeableViews
          axis={'x'}
          index={tabIndex}
          onChangeIndex={onChangeIndex}
        >
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${basePath}`}
              render={() => <SurveyList theme={theme} basePath={basePath} />}
            />
            <Route
              path={`${basePath}/edit/:id?`}
              render={() => <SurveyEdit theme={theme} basePath={basePath} />}
            />
          </Suspense>
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${basePath}/questions`}
              render={() => (
                <QuestionList
                  theme={theme}
                  basePath={`${basePath}/questions`}
                />
              )}
            />
            <Route
              path={`${basePath}/questions/edit/:id?`}
              render={() => (
                <QuestionEdit
                  theme={theme}
                  basePath={`${basePath}/questions`}
                />
              )}
            />
          </Suspense>
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${basePath}/criterias`}
              render={() => (
                <CriteriaList
                  theme={theme}
                  basePath={`${basePath}/criterias`}
                />
              )}
            />
            <Route
              path={`${basePath}/criterias/edit/:id?`}
              render={() => (
                <CriteriaEdit
                  theme={theme}
                  basePath={`${basePath}/criterias`}
                />
              )}
            />
            <Route
              path={`${basePath}/criterias/upload`}
              render={() => (
                <CriteriaUpload
                  theme={theme}
                  basePath={`${basePath}/criterias`}
                />
              )}
            />
          </Suspense>
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${basePath}/answers`}
              render={() => (
                <AnswerList theme={theme} basePath={`${basePath}/answers`} />
              )}
            />
            <Route
              path={`${basePath}/answers/edit/:id?`}
              render={() => (
                <AnswerEdit theme={theme} basePath={`${basePath}/answers`} />
              )}
            />
          </Suspense>
        </SwipeableViews>
      </Paper>
    </MuiThemeProvider>
  )
}

export default TabsView
