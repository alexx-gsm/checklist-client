import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { setFilter } from '../redux/modules/filter'

const withFilter = key => Component =>
  compose(
    connect(
      ({ filterStore }) => ({ filter: filterStore.filter[key] }),
      { setFilter }
    ),
    withHandlers({
      onSearch: ({ setFilter }) => event => {
        setFilter({ key, value: event.target.value })
      },
      handleFilterClear: ({ setFilter }) => () => setFilter({ key, value: '' })
    })
  )(Component)

export default withFilter
