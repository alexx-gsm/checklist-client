export default theme => ({
  extraTable: {
    marginBottom: '20px'
  },
  typoAutoInput: {
    lineHeight: '1.2',
    padding: '8px 5px'
  },
  typoSelectInputTitle: {
    lineHeight: 1
  },
  GridActionButton: {
    padding: '20px'
  }
})
