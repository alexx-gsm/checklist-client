export default theme => ({
  // labelPrimary: {
  //   '&$cssFocused': {
  //     color: blueGrey[700]
  //   }
  // },
  // cssFocused: {},
  // underlinePrimary: {
  //   '&:after': {
  //     borderBottomColor: blueGrey[500]
  //   }
  // },
  Wrap: {
    maxWidth: '960px'
  },
  Form: {
    marginBottom: theme.spacing.unit * 3,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  textName: {},
  inputName: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  // buttonDelete: {
  //   color: 'white',
  //   backgroundColor: red[600],
  //   '&:hover': { backgroundColor: red[700] }
  // },
  btnPrimary: {
    marginLeft: '10px'
  },
  //
  PaperLogin: {
    marginTop: '20px',
    padding: '10px 20px 20px',
    background: theme.palette.grey[50]
  }
})
