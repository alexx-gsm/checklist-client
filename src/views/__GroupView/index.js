import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'

const Groups = lazy(() => import('./GroupList'))
const GroupEdit = lazy(() => import('./GroupEdit'))

const GroupView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route exact path='/admin/groups' render={() => <Groups />} />
    <Route path='/admin/groups/edit/:id?' render={() => <GroupEdit />} />
  </Suspense>
)

export default GroupView