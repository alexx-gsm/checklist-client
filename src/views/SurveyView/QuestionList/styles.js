export default theme => ({
  QuestionList: {},
  ColoredTableRow: {
    '&.common td': {
      background: theme.palette.colorCommon[50]
    },
    '&.member td': {
      background: theme.palette.colorMember[50]
    },
    '&.boss td': {
      background: theme.palette.colorBoss[50]
    }
  }
})
