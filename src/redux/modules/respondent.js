import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'respondentStore'

// --- ACTIONS ---
export const RESPONDENT_LOADING = `${appName}/${moduleName}/RESPONDENT_LOADING`
export const RESPONDENT_STORE = `${appName}/${moduleName}/RESPONDENT_STORE`
export const RESPONDENT_SAVE = `${appName}/${moduleName}/RESPONDENT_SAVE`
export const RESPONDENT_GET_ALL = `${appName}/${moduleName}/RESPONDENT_GET_ALL`
export const RESPONDENT_UPLOAD = `${appName}/${moduleName}/RESPONDENT_UPLOAD`
export const RESPONDENT_ERROR = `${appName}/${moduleName}/RESPONDENT_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  respondents: {},
  respondent: {},
  uploadedRespondents: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case RESPONDENT_LOADING:
      return state.set('loading', true)
    case RESPONDENT_STORE:
      return state
        .set('loading', false)
        .set('respondent', payload)
        .set('error', {})
    case RESPONDENT_GET_ALL:
      return state
        .set('loading', false)
        .set('respondents', payload)
        .set('error', {})
    case RESPONDENT_UPLOAD:
      return state
        .set('loading', false)
        .set('uploadedRespondents', payload)
        .set('error', {})
    case RESPONDENT_ERROR:
      return state.set('loading', false).set('error', error.response.data)
    default:
      return state
  }
}

// --- AC ---

/**
 * * STORE RESPONDENT
 */
export const storeRespondent = payload => ({
  type: RESPONDENT_STORE,
  payload
})

/**
 * * SAVE RESPONDENT
 */
export const saveRespondent = (payload, cb) => dispatch => {
  console.log('AC --- SAVE RESPONDENT')
  axios
    .post(`${HOST}/api/respondents`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: RESPONDENT_ERROR,
        error: error.response.data
      })
    })
}

/**
 * * UPDATE RESPONDENT
 */
export const updateRespondent = (payload, cb) => dispatch => {
  console.log('AC --- PUT RESPONDENT')
  axios
    .put(`${HOST}/api/respondents`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: RESPONDENT_ERROR,
        error: error.response.data
      })
    })
}

/**
 * * GET ALL COMPANIES
 */
export const getRespondents = () => dispatch => {
  dispatch({
    type: RESPONDENT_LOADING
  })
  axios
    .post(`${HOST}/api/respondents/all`)
    .then(res => {
      console.log('AC --- GET ALL RESPONDENTS: ok')
      dispatch({
        type: RESPONDENT_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL RESPONDENTS: err')
      dispatch({
        type: RESPONDENT_ERROR,
        error
      })
    })
}

/**
 * * GET RESPONDENT BY ID
 */
export const getRespondentById = id => dispatch => {
  dispatch({
    type: RESPONDENT_LOADING
  })
  axios
    .post(`${HOST}/api/respondents/${id}`)
    .then(res => {
      dispatch({
        type: RESPONDENT_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: RESPONDENT_ERROR,
        error
      })
    )
}

/**
 * * RESPONDENT_UPLOAD
 */
export const uploadRespondents = payload => dispatch => {
  console.log('--- AC')
  if (payload.file) {
    const formData = new FormData()
    Object.keys(payload).map(key => formData.append(key, payload[key]))
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    axios
      .put(`${HOST}/api/respondents/upload`, formData, config)
      .then(res =>
        dispatch({
          type: RESPONDENT_UPLOAD,
          payload: res.data
        })
      )
      .catch(error => {
        dispatch({
          type: RESPONDENT_ERROR,
          error
        })
      })
  }
}

/**
 * * CLEAR RESPONDENT
 */
export const clearRespondent = () => ({
  type: RESPONDENT_STORE,
  payload: {}
})

/**
 * * CLEAR UPLOADED RESPONDENTS
 */
export const clearUploadedRespondents = () => ({
  type: RESPONDENT_UPLOAD,
  payload: {}
})

export default reducer
