import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// AC
import { getSurveys, clearSurvey } from '../../../../redux/modules/survey'
import { clearRespondent } from '../../../../redux/modules/respondent'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
import CardStyles from '../../../../styles/CardStyles'
import ListTableStyles from '../../../../styles/ListTableStyles'

// Component
import SurveyList from './SurveyList'

export default compose(
  connect(
    ({ surveyStore }) => {
      const { surveys, loading, error } = surveyStore

      return {
        surveys,
        loading,
        error
      }
    },
    { getSurveys, clearSurvey, clearRespondent }
  ),
  withRouter,
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getSurveys }) => () => getSurveys(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearSurvey()
      this.props.getSurveys()
      this.props.clearRespondent()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CommonStyles(theme),
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(SurveyList)
