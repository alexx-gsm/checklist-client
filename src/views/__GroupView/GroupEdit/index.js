import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { storeGroup } from '../../../redux/modules/group'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import GroupEdit from './GroupEdit'

export default compose(
  connect(
    ({ groupStore }) => {
      const { group, error, loading } = groupStore
      return {
        group,
        error,
        loading
      }
    },
    { storeGroup }
  ),
  withHandlers({
    onChange: ({ group, storeGroup }) => index => event => {
      console.log('index: ', index)
      storeGroup({
        ...group,
        [index]: event.target.value
      })
    },
    onSave: ({ group, saveGroup }) => () => {}
  }),
  withStyles(styles, { theme: true })
)(GroupEdit)
