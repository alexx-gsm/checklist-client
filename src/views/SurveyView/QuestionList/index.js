import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getQuestions, clearQuestion } from '../../../redux/modules/question'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

// Component
import QuestionList from './QuestionList'

const FILTER_KEY = 'QUESTION'

export default compose(
  connect(
    ({ questionStore, filterStore }) => {
      const { questions, loading, error } = questionStore
      const { filter } = filterStore
      const filteredQuestions =
        filter[FILTER_KEY] === ''
          ? questions
          : Object.keys(questions).reduce((acc, key) => {
              const item = questions[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        questions: filteredQuestions,
        loading,
        error
      }
    },
    { getQuestions, clearQuestion }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getQuestions }) => () => getQuestions(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearQuestion()
      this.props.getQuestions()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(QuestionList)
