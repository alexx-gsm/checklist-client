import { compose, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { loginUser, clearError } from '../../redux/modules/auth'
import { withStyles } from '@material-ui/core/styles'
import Login from './Login'
// styles
import styles from './styles'

export default compose(
  connect(
    ({ auth }) => ({ error: auth.error }),
    { loginUser, clearError }
  ),
  withRouter,
  withState('userData', 'setUserData', {}),
  withState('isPasswordVisible', 'setPasswordVisible', false),
  withHandlers({
    onChange: ({ userData, setUserData }) => event =>
      setUserData({
        ...userData,
        [event.target.name]: event.target.value
      }),
    onShowPassword: ({ setPasswordVisible }) => () => setPasswordVisible(true),
    onLogin: ({ userData, loginUser, history }) => () => {
      loginUser(userData, history)
      // history.push('/')
    },
    handleClose: ({ clearError }) => () => clearError()
  }),
  withStyles(styles, { withTheme: true })
)(Login)
