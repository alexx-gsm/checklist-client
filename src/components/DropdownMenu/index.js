import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { withStyles } from '@material-ui/core'
import styles from './styles'

import DropdownMenu from './DropdownMenu'

export default compose(
  withRouter,
  withHandlers({
    handleClose: ({ updateAnchorEl }) => () => {
      updateAnchorEl(null)
    }
  }),
  withStyles(styles, { withTheme: true })
)(DropdownMenu)
