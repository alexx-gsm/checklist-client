import React from 'react'
import classNames from 'classnames'
import Autocomplete from '../../../../../components/Autocomplete'
// @material-ui components
import { Table, TableBody, TableRow, TableCell } from '@material-ui/core'
import { Paper, Grid, Typography } from '@material-ui/core'
// @material-ui/icons
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'

const LinkAnswer = ({
  isAuto,
  result,
  onAddRow,
  onDeleteRow,
  respondents,
  handleAutocomplete,
  classes
}) => {
  return (
    <React.Fragment>
      <Table className={classNames(classes.Table, classes.extraTable)}>
        <TableBody>
          {result &&
            result.map((item, index) => {
              return (
                <TableRow key={item.code} className={classes.TableBodyRow}>
                  <TableCell
                    className={classNames(classes.removeCell, 'icon-remove')}
                  >
                    <RemoveCircleOutline
                      className={classes.removeIcon}
                      onClick={onDeleteRow(index)}
                    />
                  </TableCell>
                  <TableCell className={classes.TableCellTitle}>
                    <Typography variant='h6' className={classes.typoAutoInput}>
                      {item.fio}
                    </Typography>
                  </TableCell>
                </TableRow>
              )
            })}
        </TableBody>
      </Table>

      {isAuto ? (
        <Paper square className={classes.PaperAuto}>
          <Autocomplete
            items={respondents}
            field='fio'
            onChange={handleAutocomplete}
            placeholder='ФИО сотрудника'
          />
        </Paper>
      ) : (
        <Grid container justify='center' className={classes.MB20}>
          <AddCircle className={classes.addIcon} onClick={onAddRow} />
        </Grid>
      )}
    </React.Fragment>
  )
}

LinkAnswer.defaultProps = {
  respondents: {}
}

export default LinkAnswer
