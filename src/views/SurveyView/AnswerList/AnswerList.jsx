import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'
// react icon kit
import { Icon } from 'react-icons-kit'
import { circle } from 'react-icons-kit/fa/circle'

const AnswerList = ({
  answers,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  filter,
  onSearch,
  handleFilterClear,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        label={'Название'}
        link={basePath}
        loading={loading}
        onReload={onReload}
        search={{ filter, onSearch, handleFilterClear }}
      />
      <Paper className={classes.PaperTable}>
        <Grid container className={classes.gridTable}>
          <Table>
            <TableHead className={classes.TableHead}>
              <TableRow>
                <TableCell className={classes.headCellData} padding='checkbox'>
                  <Typography variant='caption'>Название</Typography>
                </TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(answers).map(key => {
                const answer = answers[key]
                return (
                  <TableRow key={key} className={classes.TableRow}>
                    <TableCell
                      padding='checkbox'
                      width='100%'
                      className={classes.CellBordered}
                    >
                      <Typography variant='h6' className={classes.Title}>
                        {answer.title}
                      </Typography>
                    </TableCell>
                    <TableCell padding='none'>
                      <DropdownMenu
                        anchorEl={anchorEl}
                        updateAnchorEl={updateAnchorEl}
                        menuId={key}
                        menuList={[
                          {
                            icon: <IconEdit />,
                            label: 'Edit',
                            handler: editHandler(key)
                          },
                          {
                            icon: <IconDelete />,
                            label: 'Delete',
                            handler: deleteHandler(key)
                          }
                        ]}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

AnswerList.defaultProps = {
  answers: {},
  companies: {}
}

export default AnswerList
