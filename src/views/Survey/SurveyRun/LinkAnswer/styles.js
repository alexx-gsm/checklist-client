export default theme => ({
  extraTable: {
    marginBottom: '20px'
  },
  typoAutoInput: {
    lineHeight: '1.2',
    padding: '8px 5px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '1rem'
    }
  },
  GridButtonAdd: {
    marginBottom: '20px',
    paddingLeft: '8px',
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center'
    }
  },
  root: {
    flexGrow: 1,
    height: 250
  },
  container: {
    flexGrow: 1,
    position: 'relative'
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  inputRoot: {
    flexWrap: 'wrap'
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1
  },
  divider: {
    height: theme.spacing.unit * 2
  }
})
