import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getSurveys, clearSurvey } from '../../../redux/modules/survey'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

// Component
import SurveyList from './SurveyList'

const FILTER_KEY = 'SURVEY'

export default compose(
  connect(
    ({ surveyStore, filterStore }) => {
      const { surveys, loading, error } = surveyStore
      const { filter } = filterStore
      const filteredSurveys =
        filter[FILTER_KEY] === ''
          ? surveys
          : Object.keys(surveys).reduce((acc, key) => {
              const item = surveys[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        surveys: filteredSurveys,
        loading,
        error
      }
    },
    { getSurveys, clearSurvey }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getSurveys }) => () => getSurveys(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearSurvey()
      this.props.getSurveys()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(SurveyList)
