export default theme => ({
  appBar: {
    position: 'absolute',
    zIndex: '1201'
  },
  toolBar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  avatar: {
    margin: '10px'
  },
  btnLogin: {
    color: 'white'
  }
});
