export default theme => ({
  QuestionEdit: {},
  typoWithWeight: {
    fontWeight: theme.typography.fontWeightMedium,
    '&.is-checked': {
      color: theme.palette.common.white
    },
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  textCenter: {
    textAlign: 'center'
  },
  autoHeight: {
    height: 'auto'
  },
  PaperTarget: {
    marginTop: '30px',
    padding: '20px',
    '&.common': {
      background: theme.palette.colorCommon[50]
    },
    '&.member': {
      background: theme.palette.colorMember[50]
    },
    '&.boss': {
      background: theme.palette.colorBoss[50]
    }
  },
  RadioRoot: {
    '&.common': {
      color: theme.palette.colorCommon[600],
      '&.$RadioChecked': {
        color: theme.palette.colorCommon[500]
      }
    },
    '&.member': {
      color: theme.palette.colorMember[600],
      '&.$RadioChecked': {
        color: theme.palette.colorMember[500]
      }
    },
    '&.boss': {
      color: theme.palette.colorBoss[600],
      '&.$RadioChecked': {
        color: theme.palette.colorBoss[500]
      }
    }
  },
  RadioChecked: {},
  extraTable: {
    marginBottom: '40px'
  },
  Upload: {
    display: 'none'
  },
  UploadButton: {
    margin: theme.spacing.unit
  },
  UploadList: {
    maxHeight: '60vh',
    overflow: 'auto'
  }
})
