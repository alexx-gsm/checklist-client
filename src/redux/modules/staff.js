import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'staffStore'

// --- ACTIONS ---
export const STAFF_LOADING = `${appName}/${moduleName}/STAFF_LOADING`
export const STAFF_GET_ALL = `${appName}/${moduleName}/STAFF_GET_ALL`
export const STAFF_STORE = `${appName}/${moduleName}/STAFF_STORE`
export const STAFF_ERROR = `${appName}/${moduleName}/STAFF_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  staffs: {},
  staff: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case STAFF_LOADING:
      return state.set('loading', true)
    case STAFF_GET_ALL:
      return state
        .set('loading', false)
        .set('staffs', payload)
        .set('error', {})
    case STAFF_STORE:
      return state
        .set('loading', false)
        .set('staff', payload)
        .set('error', {})
    case STAFF_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * get all staffs
 */
export const getAllStaffs = () => dispatch => {
  dispatch({
    type: STAFF_LOADING
  })
  axios
    .post(`${HOST}/api/users/`)
    .then(res => {
      console.log('--- ok')
      dispatch({
        type: STAFF_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('--- err')
      dispatch({
        type: STAFF_ERROR,
        error
      })
    })
}

/**
 * * store staff
 */
export const storeStaff = payload => ({
  type: STAFF_STORE,
  payload
})

export default reducer
