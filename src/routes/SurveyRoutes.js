// views
import Survey from '../views/Survey'

const layout = '/survey'

const SurveyRoutes = [
  {
    path: `${layout}`,
    exact: true,
    component: Survey
  },
  {
    redirect: true,
    path: `${layout}`,
    to: `/home/surveys`,
    navbarName: 'Redirect'
  }
]

export default SurveyRoutes
