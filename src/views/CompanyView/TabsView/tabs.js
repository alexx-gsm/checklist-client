export default [
  {
    label: 'Companies',
    link: ''
  },
  {
    label: 'Departments',
    link: 'departments'
  },
  {
    label: 'Activities',
    link: 'activities'
  }
]
