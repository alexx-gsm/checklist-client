import { withRouter } from 'react-router-dom'
import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import {
  getRoleById,
  storeRole,
  saveRole,
  updateRole
} from '../../../redux/modules/role'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import RoleEdit from './RoleEdit'

export default compose(
  connect(
    ({ roleStore }) => {
      const { role, error, loading } = roleStore
      return {
        role,
        error,
        loading
      }
    },
    { getRoleById, storeRole, saveRole, updateRole }
  ),
  withRouter,
  withHandlers({
    onReload: ({ getRoleById, match }) => () => getRoleById(match.params.id),
    onChange: ({ role, storeRole }) => index => event => {
      console.log('index: ', index)
      storeRole({
        ...role,
        [index]: event.target.value
      })
    },
    onSave: ({ role, saveRole, updateRole, history, basePath }) => () => {
      if (role.id) {
        updateRole(role, () => history.push(basePath))
      } else {
        saveRole(role, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getRoleById(id)
      }
    }
  }),
  withStyles(styles, { theme: true })
)(RoleEdit)
