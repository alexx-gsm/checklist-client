import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getCompanies, clearCompany } from '../../../redux/modules/company'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

import CompanyList from './CompanyList'

const FILTER_KEY = 'COMPANY'

export default compose(
  connect(
    ({ companyStore, filterStore }) => {
      const { companies, loading, error } = companyStore
      const { filter } = filterStore
      const filteredCompanies =
        filter[FILTER_KEY] === ''
          ? companies
          : Object.keys(companies).reduce((acc, key) => {
              const item = companies[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1 ||
                item.inn.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        companies: filteredCompanies,
        loading,
        error
      }
    },
    { getCompanies, clearCompany }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getCompanies }) => () => getCompanies(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearCompany()
      this.props.getCompanies()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(CompanyList)
