import React from 'react'
import { object, bool, func } from 'prop-types'
import { Route, Switch, Redirect, NavLink } from 'react-router-dom'
import { AppBar, Drawer, Toolbar } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { Grid, Typography, Button } from '@material-ui/core'
// @material-ui/icons
import MenuIcon from '@material-ui/icons/Menu'
import IconButton from '@material-ui/core/IconButton'
import isEmpty from '../../helpers/is-empty'
// components
import SurveyRoutes from '../../routes/SurveyRoutes'

const switchRoutes = (
  <Switch>
    {SurveyRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />
      if (prop.divider) return ''
      return (
        <Route
          path={prop.path}
          component={prop.component}
          key={key}
          exact={prop.exact && false}
        />
      )
    })}
  </Switch>
)

const Survey = ({
  stage,
  questionList,
  questions,
  handleClick,
  handleFinish,
  classes,
  theme,
  isOpen,
  onSidebarToggle
}) => {
  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          <Grid container justify='space-between' alignItems='center'>
            <Typography variant='title' color='inherit' noWrap>
              Опрос
            </Typography>
            {stage > -1 && (
              <IconButton
                color='inherit'
                aria-label='Open drawer'
                onClick={onSidebarToggle}
                className={classes.navIconHide}
              >
                <MenuIcon />
              </IconButton>
            )}
          </Grid>
        </Toolbar>
      </AppBar>

      <Drawer anchor='right' open={isOpen} onClose={onSidebarToggle}>
        <div
          tabIndex={0}
          role='button'
          onClick={onSidebarToggle}
          onKeyDown={onSidebarToggle}
        >
          <Grid container justify='center' className={classes.GridFinishButton}>
            <Button variant='contained' color='primary' onClick={handleFinish}>
              Завершить опрос
            </Button>
          </Grid>
          {!isEmpty(questionList) && !isEmpty(questions) && (
            <List classes={{ root: classes.ListRoot }}>
              {questionList.map((itemId, index) => (
                <ListItem
                  onClick={handleClick(index)}
                  key={itemId}
                  classes={{ root: classes.ListItemRoot }}
                >
                  <ListItemText secondary={questions[itemId].title} />
                </ListItem>
              ))}
            </List>
          )}
        </div>
      </Drawer>

      <main className={classes.Content}>
        <div className={classes.toolbar} />
        {switchRoutes}
      </main>
    </div>
  )
}

Survey.propTypes = {
  classes: object.isRequired,
  theme: object.isRequired,
  isOpen: bool.isRequired,
  onSidebarToggle: func.isRequired
}

export default Survey
