import React from 'react'
import { NavLink } from 'react-router-dom'
// material-ui components
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
// react-icon-kit
import { Icon } from 'react-icons-kit'

const Sidebar = ({ routes, showLabel, classes }) => {
  return (
    <div>
      <div className={classes.toolbar} />
      <List component='nav'>
        {routes.map((route, i) => {
          if (route.divider) return <Divider key={i} />
          if (!route.redirect) {
            return (
              <ListItem
                key={route.path}
                button
                component={NavLink}
                to={route.path}
                className={classes.listItem}
                classes={{
                  root: classes.listItemRoot
                }}
              >
                {route.icon && (
                  <ListItemIcon classes={{ root: classes.listItemIconRoot }}>
                    <Icon size={26} icon={route.icon} />
                  </ListItemIcon>
                )}
                {showLabel && (
                  <ListItemText
                    primary={route.sidebarName}
                    primaryTypographyProps={{
                      classes: { root: classes.listItemTextRoot }
                    }}
                  />
                )}
              </ListItem>
            )
          }
        })}
      </List>
    </div>
  )
}

Sidebar.defaultProps = {
  showLabel: true
}

export default Sidebar
