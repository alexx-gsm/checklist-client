export default theme => ({
  extraTable: {
    // marginBottom: '20px'
  },
  typoAutoInput: {
    lineHeight: '1.2',
    padding: '8px 5px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.1rem',
      lineHeight: 1.1
    },
    [theme.breakpoints.down('xs')]: {
      padding: '8px 0',
      fontSize: '1rem',
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette.extraCardColor[800]
    }
  },
  typoSelectInputTitle: {
    lineHeight: 1,
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.2rem'
    }
  },
  GridContainerMobile: {},
  GridItemMobile: {
    padding: '0 10px 8px',
    borderBottom: `1px solid ${theme.palette.grey[400]}`,
    '&:nth-child(even)': {
      background: theme.palette.grey[100]
    }
  }
})
