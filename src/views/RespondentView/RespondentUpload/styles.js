export default theme => ({
  UploadPaper: {
    marginTop: '30px',
    padding: '20px'
  },
  Upload: {
    display: 'none'
  },
  UploadButton: {
    margin: theme.spacing.unit
  },
  UploadList: {
    maxHeight: '60vh',
    overflow: 'auto'
  }
})
