import { compose, withState, withHandlers } from 'recompose'
import Appbar from './Appbar'

export default compose(
  withState('isOpen', 'setIsOpen', false),
  withHandlers({
    toggleDrawer: ({ setIsOpen }) => state => () => setIsOpen(state)
  })
)(Appbar)
