import React, { lazy, Suspense } from 'react'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'

const TabsView = lazy(() => import('./TabsView'))

const basePath = '/admin/surveys'

const CompanyView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <TabsView basePath={basePath} />
  </Suspense>
)

export default CompanyView
