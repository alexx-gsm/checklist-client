import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import {
  Paper,
  Grid,
  Typography,
  TextField,
  InputAdornment,
  IconButton
} from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
// import {search} from 'react-icons-kit/fa/search'
import IconSearch from '@material-ui/icons/Search'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'
import IconClear from '@material-ui/icons/Clear'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'blueGrey'

const UserList = ({
  users,
  filter,
  handleFilterClear,
  onSearch,
  roles,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  error,
  classes
}) => {
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link={basePath}
        loading={loading}
        onReload={onReload}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Grid item container alignItems='flex-end'>
              <TextField
                id='search'
                type='text'
                value={isEmpty(filter) ? '' : filter}
                onChange={onSearch}
                className={classes.select}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    root: classes.search,
                    underline: classes.underlineWhite
                  },
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconSearch />
                    </InputAdornment>
                  )
                }}
              />
              <IconButton
                aria-label='Clear'
                className={classes.buttonClearFilter}
                disabled={isEmpty(filter)}
                onClick={handleFilterClear}
              >
                <IconClear fontSize='small' />
              </IconButton>
            </Grid>
            <Typography variant='display1' className={classes.typoCardHeader}>
              Пользователи
            </Typography>
          </Grid>
        </Paper>
        <Paper className={classes.Table}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.TableHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding='checkbox'
                  >
                    <Typography variant='caption'>ФИО | email</Typography>
                  </TableCell>

                  <TableCell
                    align='right'
                    padding='checkbox'
                    className={classes.headerCellInfo}
                  >
                    Role
                  </TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(users).map(key => {
                  const user = users[key]
                  return (
                    <TableRow key={user.id} className={classes.TableRow}>
                      <TableCell
                        className={classes.cellData}
                        padding='checkbox'
                      >
                        <Grid container className={classes.cellContainer}>
                          <Typography
                            className={classes.cellItem}
                            variant='title'
                          >
                            {user.name}
                          </Typography>
                          <Typography
                            className={classes.cellSubItem}
                            variant='caption'
                          >
                            {user.email}
                          </Typography>
                        </Grid>
                      </TableCell>
                      <TableCell padding='checkbox'>
                        <Typography variant='caption' align='right'>
                          {!isEmpty(roles) ? roles[user.role].title : '-'}
                        </Typography>
                      </TableCell>
                      <TableCell padding='none'>
                        <DropdownMenu
                          anchorEl={anchorEl}
                          updateAnchorEl={updateAnchorEl}
                          menuId={key}
                          menuList={[
                            {
                              icon: <IconEdit />,
                              label: 'Edit',
                              handler: editHandler(key)
                            },
                            {
                              icon: <IconDelete />,
                              label: 'Delete',
                              handler: deleteHandler(key)
                            }
                          ]}
                        />
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </div>
  )
}

UserList.defaultProps = {
  users: {},
  roles: {}
}

UserList.propTypes = {
  users: PropTypes.object,
  roles: PropTypes.object,
  loading: PropTypes.bool,
  onReaload: PropTypes.func,
  handleClick: PropTypes.func,
  handleEdit: PropTypes.func,
  handleClose: PropTypes.func,
  error: PropTypes.object,
  classes: PropTypes.object
}

export default UserList
