import React from 'react'
import { ChromePicker } from 'react-color'
import { Dialog, DialogActions, DialogContent } from '@material-ui/core'
import { Button } from '@material-ui/core'

const ColorPicker = ({ open, color, onChange, onSubmit }) => {
  console.log('color', color)
  return (
    <Dialog open={open} onClose={onSubmit} aria-labelledby='color-picker'>
      <DialogContent>
        <ChromePicker color={color} onChangeComplete={onChange('color')} />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={onSubmit}
          color='primary'
          variant='contained'
          style={{ background: color }}
        >
          Select
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ColorPicker
