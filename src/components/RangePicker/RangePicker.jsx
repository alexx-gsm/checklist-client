import React from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from '@material-ui/core'
import { Button } from '@material-ui/core'
// useful tools
import isEmpty from '../../helpers/is-empty'
// DayPicker
import DayPicker from 'react-day-picker'
import MomentLocaleUtils from 'react-day-picker/moment'
import 'moment/locale/ru'
import 'react-day-picker/lib/style.css'
import './style.css'

const RangePicker = ({
  range,
  handleDayClick,
  handleClose,
  open,
  onReset,
  onSubmit
}) => {
  const { from, to } = range
  const modifiers = { start: from, end: to }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='simple-dialog-title'
      open={open}
    >
      <DialogTitle id='simple-dialog-title'>Select research range</DialogTitle>
      <DialogContent>
        <DayPicker
          className='Selectable'
          numberOfMonths={2}
          selectedDays={[from, { from, to }]}
          modifiers={modifiers}
          onDayClick={handleDayClick}
          locale='ru'
          localeUtils={MomentLocaleUtils}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onReset} color='secondary'>
          Reset
        </Button>
        <Button onClick={handleClose} color='primary'>
          Cancel
        </Button>
        <Button color='primary' onClick={onSubmit}>
          Select
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default RangePicker
