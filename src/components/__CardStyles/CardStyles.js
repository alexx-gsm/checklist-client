import red from '@material-ui/core/colors/red'

export default theme => ({
  Card: {
    maxWidth: '960px',
    margin: '40px 0 0',
    background: theme.palette.common.white,
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    margin: '0 20px',
    padding: '5px 10px',
    color: theme.palette.common.white,
    background: `linear-gradient(60deg, ${theme.palette.primary.light}, ${
      theme.palette.primary.main
    })`,
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  iconNewWrap: {
    position: 'absolute',
    top: '-3px',
    left: '-30px'
  },
  iconNew: {
    color: theme.palette.common.white
  },
  Input40px: {
    fontSize: '40px',
    color: theme.palette.grey[700],
    padding: 0,
    [theme.breakpoints.down('sm')]: {
      fontSize: '28px'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '24px'
    }
  },
  CardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between',
    background: theme.palette.grey[50]
  },
  CardButtonDelete: {
    color: 'white',
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[700] }
  },
  CardButtonSave: {
    marginLeft: '10px',
    color: theme.palette.common.white
  }
})
