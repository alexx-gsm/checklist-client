// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { cubes } from 'react-icons-kit/fa/cubes'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { legal } from 'react-icons-kit/fa/legal'
import { archive } from 'react-icons-kit/entypo/archive'
import { users } from 'react-icons-kit/fa/users'
// views
import Dashboard from '../views/Home/Dashboard'
import SurveyView from '../views/Home/SurveyView'

const layout = '/home'

const HomeRoutes = [
  {
    path: `${layout}/dashboard`,
    exact: true,
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: ic_dashboard,
    component: Dashboard
  },
  {
    path: `${layout}/surveys`,
    exact: true,
    sidebarName: 'Surveys',
    navbarName: 'Surveys',
    icon: cubes,
    component: SurveyView
  },
  { divider: true },
  {
    redirect: true,
    path: `${layout}`,
    to: `${layout}/dashboard`,
    navbarName: 'Redirect'
  }
]

export default HomeRoutes
