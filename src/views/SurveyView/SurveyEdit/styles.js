export default theme => ({
  SurveyEdit: {},
  textCenter: {
    textAlign: 'center'
  },
  autoHeight: {
    height: 'auto'
  },
  TypoListHeader: {
    marginTop: '30px',
    padding: '0 10px 0 90px',
    background: theme.palette.extraCardColor[600],
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  DND_List: {
    width: '100%',
    padding: 0
  },
  DND_Item: {
    background: theme.palette.extraCardColor[50],
    paddingLeft: '12px',
    paddingRight: '12px',
    zIndex: 5
  },
  IconArrowsV: {
    cursor: 'move'
  },
  TypoNumber: {
    margin: '0 10px',
    minWidht: '30px',
    width: '30px',
    textAlign: 'right',
    color: theme.palette.extraCardColor[600]
  },
  extraAddIcon: {
    marginTop: '30px'
  }
})
