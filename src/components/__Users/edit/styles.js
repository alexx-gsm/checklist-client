import deepOrange from '@material-ui/core/colors/deepOrange'
import grey from '@material-ui/core/colors/grey'
import blue from '@material-ui/core/colors/blue'
import red from '@material-ui/core/colors/red'

export default theme => ({
  labelPrimary: {
    '&$cssFocused': {
      color: deepOrange[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&:after': {
      borderBottomColor: deepOrange[500]
    }
  },
  //
  Card: {
    maxWidth: '940px',
    margin: '40px 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    padding: '5px 10px',
    margin: '0 20px',
    color: theme.palette.common.white,
    '&.blue': {
      background: `linear-gradient(60deg, ${blue[500]}, ${blue[600]})`
    },
    '&.orange': {
      background: `linear-gradient(60deg, ${deepOrange[500]}, ${
        deepOrange[600]
      })`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  iconNewWrap: {
    position: 'absolute',
    top: '-3px',
    left: '-30px'
  },
  iconNew: {
    color: 'white'
  },
  Form: {
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  textName: {
    // marginBottom: '0'
  },
  inputName: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonDelete: {
    color: 'white',
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[700] }
  },
  btnPrimary: {
    marginLeft: '10px',
    color: theme.palette.common.white,
    '&.orange': {
      backgroundColor: deepOrange[600],
      '&:hover': {
        backgroundColor: deepOrange[700]
      }
    },
    '&.blue': {
      backgroundColor: blue[600],
      '&:hover': {
        backgroundColor: blue[700]
      }
    }
  }
})
