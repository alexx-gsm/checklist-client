import { compose, withProps, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { getAllStaffs } from '../../redux/modules/staff'
// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'

import Users from './Users'

export default compose(
  connect(
    ({ staffStore }) => {
      const { staffs, loading, error } = staffStore
      return {
        staffs,
        loading,
        error
      }
    },
    { getAllStaffs }
  ),
  withHandlers({
    onReload: ({ getAllStaffs }) => () => getAllStaffs()
  }),
  lifecycle({
    componentDidMount() {
      this.props.getAllStaffs()
    }
  }),
  withStyles(styles, { withTheme: true })
)(Users)
