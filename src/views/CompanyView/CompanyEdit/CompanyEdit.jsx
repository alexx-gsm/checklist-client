import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'teal'
const COLOR_2 = 'grey'

const CompanyEdit = ({
  company,
  activities,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onReload,
  basePath
}) => {
  const { id, title, activityId, inn, quantity, boss, contact } = company
  return (
    <div>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Company
            </Typography>
          </Grid>
        </Paper>
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            className={classes.textName}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes[COLOR],
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputName,
              classes: {
                underline: classNames(classes.underlinePrimary, COLOR)
              }
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          <TextField
            select
            fullWidth
            id='activity'
            label='Activity'
            value={isEmpty(activityId) ? '' : activityId}
            onChange={onChange('activityId')}
            margin='normal'
            className={classes.wrapTitle}
            InputProps={{
              className: classes.Title
            }}
            error={Boolean(error.role)}
            helperText={error.role ? error.role : null}
          >
            {Object.keys(activities).map(key => {
              const item = activities[key]
              return (
                <MenuItem key={key} value={key}>
                  <Grid
                    container
                    alignItems='baseline'
                    className={classes.gridType}
                    direction='row'
                    wrap='wrap'
                  >
                    <Typography
                      className={classes.typoSelectInputTitle}
                      variant='h6'
                    >
                      {item.title}
                    </Typography>
                  </Grid>
                </MenuItem>
              )
            })}
          </TextField>

          <Grid container justify='center' spacing={16}>
            <Grid item xs={8}>
              <TextField
                fullWidth
                id='inn'
                label='ИНН'
                value={isEmpty(inn) ? '' : inn}
                onChange={onChange('inn')}
                className={classes.select}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes[COLOR],
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    underline: classNames(classes.underlinePrimary, COLOR)
                  }
                }}
                error={Boolean(error.inn)}
                helperText={error.inn ? error.inn : null}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                id='quantity'
                label='Численность'
                type='number'
                value={isEmpty(quantity) ? '' : quantity}
                onChange={onChange('quantity')}
                className={classes.select}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes[COLOR],
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    underline: classNames(classes.underlinePrimary, COLOR)
                  }
                }}
                error={Boolean(error.quantity)}
                helperText={error.quantity ? error.quantity : null}
              />
            </Grid>
          </Grid>

          <Paper className={classNames(classes.PaperPerson, COLOR)}>
            <Typography
              variant='title'
              className={classNames(classes.typoPersonTitle, COLOR)}
            >
              Руководитель
            </Typography>
            <Grid className={classes.gridPersonWrap}>
              <TextField
                fullWidth
                id='boss'
                label='ФИО'
                name='name'
                value={isEmpty(boss) ? '' : isEmpty(boss.name) ? '' : boss.name}
                onChange={onChange('boss')}
                className={classes.PersonField}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes[COLOR],
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    root: classes.inputPersonName,
                    underline: classNames(classes.underlinePrimary, COLOR)
                  }
                }}
                error={Boolean(error.boss)}
                helperText={error.boss ? error.boss : null}
              />
              <Grid container spacing={16}>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    id='email'
                    label='Email'
                    type='email'
                    name='email'
                    value={
                      isEmpty(boss) ? '' : isEmpty(boss.email) ? '' : boss.email
                    }
                    onChange={onChange('boss')}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes[COLOR],
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: {
                        underline: classNames(classes.underlinePrimary, COLOR)
                      }
                    }}
                    // error={Boolean(error.boss.email)}
                    // helperText={error.boss.email ? error.boss.email : null}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    id='phone'
                    label='Телефон'
                    name='phone'
                    value={
                      isEmpty(boss) ? '' : isEmpty(boss.phone) ? '' : boss.phone
                    }
                    onChange={onChange('boss')}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes[COLOR],
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: {
                        underline: classNames(classes.underlinePrimary, COLOR)
                      }
                    }}
                    // error={Boolean(error.boss.phone)}
                    // helperText={error.boss.phone ? error.boss.phone : null}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Paper>

          <Paper className={classes.PaperPerson}>
            <Typography variant='title' className={classes.typoPersonTitle}>
              Контактное лицо
            </Typography>
            <Grid className={classes.gridPersonWrap}>
              <TextField
                fullWidth
                id='contact'
                label='ФИО'
                name='name'
                value={
                  isEmpty(contact)
                    ? ''
                    : isEmpty(contact.name)
                    ? ''
                    : contact.name
                }
                onChange={onChange('contact')}
                className={classes.PersonField}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes[COLOR_2],
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: {
                    root: classes.inputPersonName,
                    underline: classNames(classes.underlinePrimary, COLOR_2)
                  }
                }}
                error={Boolean(error.contact)}
                helperText={error.contact ? error.contact : null}
              />
              <Grid container spacing={16}>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    id='email'
                    label='Email'
                    type='email'
                    name='email'
                    value={
                      isEmpty(contact)
                        ? ''
                        : isEmpty(contact.email)
                        ? ''
                        : contact.email
                    }
                    onChange={onChange('contact')}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes[COLOR_2],
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: {
                        underline: classNames(classes.underlinePrimary, COLOR_2)
                      }
                    }}
                    // error={Boolean(error.boss.email)}
                    // helperText={error.boss.email ? error.boss.email : null}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    id='phone'
                    label='Телефон'
                    name='phone'
                    value={
                      isEmpty(contact)
                        ? ''
                        : isEmpty(contact.phone)
                        ? ''
                        : contact.phone
                    }
                    onChange={onChange('contact')}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes[COLOR_2],
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: {
                        underline: classNames(classes.underlinePrimary, COLOR_2)
                      }
                    }}
                    // error={Boolean(error.contact.phone)}
                    // helperText={error.boss.phone ? error.boss.phone : null}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.buttonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              className={classNames(classes.btnPrimary, COLOR)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

CompanyEdit.defaultProps = {
  activities: {}
}

CompanyEdit.propTypes = {
  company: PropTypes.shape({
    title: PropTypes.string,
    answers: PropTypes.arrayOf(PropTypes.string)
  }),
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default CompanyEdit
