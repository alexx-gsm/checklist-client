// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { cubes } from 'react-icons-kit/fa/cubes'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { legal } from 'react-icons-kit/fa/legal'
import { archive } from 'react-icons-kit/entypo/archive'
import { users } from 'react-icons-kit/fa/users'
// views
import UserView from '../views/Admin/UserView'
import ResearchView from '../views/Admin/ResearchView'

import Dashboard from '../views/Dashboard'
import CompanyView from '../views/CompanyView'
// import ResearchView from '../views/ResearchView'
import RoleView from '../views/RoleView'
import SurveyView from '../views/SurveyView'
import RespondentView from '../views/RespondentView'

const layout = '/admin'

const AdminRoutes = [
  {
    path: `${layout}/dashboard`,
    exact: true,
    sidebarName: 'Dashboard',
    navbarName: 'Material Dashboard',
    icon: ic_dashboard,
    component: Dashboard
  },
  { divider: true },
  {
    path: `${layout}/researches`,
    sidebarName: 'Researches',
    navbarName: 'Researches',
    icon: cubes,
    component: ResearchView
  },
  { divider: true },
  {
    path: `${layout}/respondents`,
    sidebarName: 'Respondents',
    navbarName: 'Respondents',
    icon: users,
    component: RespondentView
  },
  {
    path: `${layout}/companies`,
    sidebarName: 'Companies',
    navbarName: 'Companies',
    icon: legal,
    component: CompanyView
  },
  {
    path: `${layout}/surveys`,
    sidebarName: 'Surveys',
    navbarName: 'Surveys',
    icon: archive,
    component: SurveyView
  },
  { divider: true },
  {
    path: `${layout}/users`,
    sidebarName: 'Users',
    navbarName: 'Users',
    icon: ic_people,
    component: UserView
  },
  {
    path: `${layout}/roles`,
    sidebarName: 'User Roles',
    navbarName: 'Roles',
    icon: legal,
    component: RoleView
  },
  { divider: true },
  {
    redirect: true,
    path: `${layout}`,
    to: `${layout}/dashboard`,
    navbarName: 'Redirect'
  }
]

export default AdminRoutes
