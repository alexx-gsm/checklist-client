import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'
import theme from './theme'

const SurveyRun = lazy(() => import('./SurveyRun'))
const SurveyGraph = lazy(() => import('./SurveyGraph'))

const basePath = '/survey'

const Survey = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Switch>
      <Route
        path={`${basePath}/graph`}
        exact
        render={() => <SurveyGraph basePath={'/survey'} theme={theme} />}
      />
      <Route
        path={`${basePath}/:id`}
        exact
        render={() => <SurveyRun basePath={'/survey'} theme={theme} />}
      />
    </Switch>
  </Suspense>
)

export default Survey
