import { withRouter } from 'react-router-dom'
import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import {
  getQtypeById,
  storeQtype,
  saveQtype,
  updateQtype
} from '../../../redux/modules/qtype'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import QuestionTypeEdit from './QuestionTypeEdit'

export default compose(
  connect(
    ({ qtypeStore }) => {
      const { qtype, error, loading } = qtypeStore
      return {
        qtype,
        error,
        loading
      }
    },
    { getQtypeById, storeQtype, saveQtype, updateQtype }
  ),
  withRouter,
  withHandlers({
    onReload: ({ getRoleById, match }) => () => getRoleById(match.params.id),
    onChange: ({ qtype, storeQtype }) => index => event => {
      storeQtype({
        ...qtype,
        [index]: event.target.value
      })
    },
    onChangeItem: ({ qtype, storeQtype }) => index => event => {
      storeQtype({
        ...qtype,
        answers: [
          ...qtype.answers.slice(0, index),
          event.target.value,
          ...qtype.answers.slice(index + 1)
        ]
      })
    },
    onAddRow: ({ qtype, storeQtype }) => () => {
      storeQtype({
        ...qtype,
        answers: [...qtype.answers.filter(item => !isEmpty(item)), '']
      })
    },
    onDeleteRow: ({ qtype, storeQtype }) => index => () => {
      storeQtype({
        ...qtype,
        answers: [
          ...qtype.answers.slice(0, index),
          ...qtype.answers.slice(index + 1)
        ]
      })
    },
    onSave: ({ qtype, saveQtype, updateQtype, history, basePath }) => () => {
      if (qtype.id) {
        updateQtype(qtype, () => history.push(basePath))
      } else {
        saveQtype(qtype, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getQtypeById(id)
      }
    }
  }),
  withStyles(styles, { theme: true })
)(QuestionTypeEdit)
