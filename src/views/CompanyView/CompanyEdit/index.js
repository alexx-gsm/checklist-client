import { withRouter } from 'react-router-dom'
import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import {
  getCompanyById,
  storeCompany,
  saveCompany,
  updateCompany
} from '../../../redux/modules/company'
import { getActivitys } from '../../../redux/modules/activity'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import CompanyEdit from './CompanyEdit'

export default compose(
  connect(
    ({ companyStore, activityStore }) => {
      const { company, error, loading } = companyStore
      const { activities } = activityStore
      return {
        company,
        activities,
        error,
        loading
      }
    },
    { getCompanyById, storeCompany, saveCompany, updateCompany, getActivitys }
  ),
  withRouter,
  withHandlers({
    onReload: ({ getCompanyById, match }) => () =>
      getCompanyById(match.params.id),
    onChange: ({ company, storeCompany }) => index => event => {
      if (index === 'boss' || index === 'contact') {
        storeCompany({
          ...company,
          [index]: {
            ...company[index],
            [event.target.name]: event.target.value
          }
        })
      } else {
        storeCompany({
          ...company,
          [index]: event.target.value
        })
      }
    },
    onSave: ({
      company,
      saveCompany,
      updateCompany,
      history,
      basePath
    }) => () => {
      if (company.id) {
        updateCompany(company, () => history.push(basePath))
      } else {
        saveCompany(company, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getCompanyById(id)
      }
      this.props.getActivitys()
    }
  }),
  withStyles(theme => styles(theme), { theme: true })
)(CompanyEdit)
