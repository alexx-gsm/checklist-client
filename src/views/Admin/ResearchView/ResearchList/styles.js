export default theme => ({
  Wrap: {
    maxWidth: '960px'
  },
  GridButtons: {
    marginTop: '20px',
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center'
    }
  }
})
