export default theme => ({
  DepartmentEdit: {},
  Form: {
    padding: '20px'
  },
  PaperInnerBlock: {
    margin: '20px',
    padding: '0 20px 20px'
  },
  ButtonColorPicker: {
    marginTop: '15px'
  }
})
