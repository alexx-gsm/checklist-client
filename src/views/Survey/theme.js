import { createMuiTheme } from '@material-ui/core/styles'
import { blueGrey, green, amber, red } from '@material-ui/core/colors'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#546e7a',
      contrastText: '#fff'
    },
    secondary: {
      main: '#d50000',
      contrastText: '#fff'
    },
    extraCardColor: blueGrey,
    colorCommon: green,
    colorMember: amber,
    colorBoss: red
  }
})
