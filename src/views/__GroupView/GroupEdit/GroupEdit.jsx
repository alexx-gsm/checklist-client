import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
// Material UI/icons
import FiberNew from '@material-ui/icons/FiberNew'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'blue'

const GroupEdit = ({ group, error, classes, onChange, onSave, onDelete }) => {
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link='/admin/groups'
        returnButton={true}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              {isEmpty([]) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Группа
            </Typography>
          </Grid>
        </Paper>
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(group.title) ? '' : group.title}
            onChange={onChange('title')}
            margin='dense'
            className={classes.textName}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes[COLOR],
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputName,
              classes: {
                underline: classNames(classes.underlinePrimary, COLOR)
              }
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.buttonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={'/admin/groups'}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              className={classNames(classes.btnPrimary, COLOR)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

GroupEdit.defaultProps = {
  group: {},
  error: {}
}

GroupEdit.propTypes = {
  group: PropTypes.object,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default GroupEdit
