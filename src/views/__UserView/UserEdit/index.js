import { compose, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  getUserById,
  storeUser,
  saveUser,
  updateUser
} from '../../../redux/modules/user'
import { getRoles } from '../../../redux/modules/role'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
// Useful tools
import isEmpty from '../../../helpers/is-empty'

import UserEdit from './UserEdit'

export default compose(
  connect(
    ({ userStore, roleStore }) => {
      const { user, error, loading } = userStore
      const { roles } = roleStore
      return {
        user,
        roles,
        error,
        loading
      }
    },
    { getUserById, storeUser, saveUser, updateUser, getRoles }
  ),
  withRouter,
  withHandlers({
    onReload: ({ user, getUserById }) => () => getUserById(user.id),
    onChange: ({ user, storeUser }) => index => event => {
      console.log('index: ', index)
      storeUser({
        ...user,
        [index]: event.target.value
      })
    },
    onSave: ({ user, saveUser, updateUser, history, basePath }) => () => {
      if (user.id) {
        updateUser(user, () => history.push(basePath))
      } else {
        saveUser(user, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getUserById(id)
      }
      this.props.getRoles()
    }
  }),
  withStyles(styles, { theme: true })
)(UserEdit)
