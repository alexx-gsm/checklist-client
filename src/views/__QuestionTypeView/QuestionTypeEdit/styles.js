import deepOrange from '@material-ui/core/colors/deepOrange'
import red from '@material-ui/core/colors/red'
import grey from '@material-ui/core/colors/grey'

export default theme => ({
  orange: {
    '&$cssFocused': {
      color: deepOrange[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&.orange': {
      '&:before': {
        borderBottomColor: deepOrange[500] + '!important'
      },
      '&:after': {
        borderBottomColor: deepOrange[500]
      }
    },
    '&.NoColor': {
      '&:before': {
        borderBottomColor: 'transparent !important'
      },
      '&:after': {
        borderBottomColor: deepOrange[500]
      }
    }
  },
  //
  Card: {
    maxWidth: '740px',
    margin: '40px 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    },
    '&.orange': {}
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    padding: '5px 10px',
    margin: '0 20px',
    color: theme.palette.common.white,
    '&.orange': {
      background: `linear-gradient(60deg, ${deepOrange[500]}, ${
        deepOrange[600]
      })`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  iconNewWrap: {
    position: 'absolute',
    top: '-3px',
    left: '-30px'
  },
  iconNew: {
    color: 'white'
  },
  Form: {
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  textName: {
    // marginBottom: '0'
  },
  inputName: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonDelete: {
    color: 'white',
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[700] }
  },
  btnPrimary: {
    marginLeft: '10px',
    color: theme.palette.common.white,
    '&.orange': {
      backgroundColor: deepOrange[600],
      '&:hover': {
        backgroundColor: deepOrange[700]
      }
    }
  },
  // INNER TABLE
  Table: {
    marginTop: '40px'
  },
  TableHeadRow: {
    height: '28px',
    minHeight: '28px',
    '& th': {
      backgroundColor: deepOrange[500],
      color: theme.palette.common.white,
      padding: '5px',
      textTransform: 'uppercase'
    }
  },
  TableHeadCell: {
    fontWeight: 'bold'
  },
  TableBodyRow: {
    height: '28px',
    '&:first-child td': { paddingTop: '10px' },
    '&:nth-child(even) td': { backgroundColor: deepOrange[50] },
    '&:last-child td': { backgroundColor: 'white' },
    '& td': {
      borderBottom: 'none',
      padding: '0',
      fontSize: '1rem',
      '&:not(:first-child)': { padding: '0 5px' },
      '&:last-child': { padding: '5px' },
      '&.icon-remove': {
        paddingLeft: '5px',
        paddingTop: '5px'
      },
      '&.icon-add': {
        paddingTop: '10px'
      }
    },
    '& td input': {
      width: '100%',
      padding: 0
    }
  },
  TableCellTitle: {
    width: '100%'
  },
  removeIcon: {
    color: grey[400],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: red[800],
      transition: 'color ease 0.3s'
    }
  },
  addIcon: {
    fontSize: '35px',
    color: deepOrange[600],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: deepOrange[700],
      transition: 'color ease 0.3s'
    }
  }
})
