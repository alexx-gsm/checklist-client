import { compose, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// AC
import { logoutUser } from '../../redux/modules/auth'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Appbar from './Appbar'
// styles
import styles from './style'

export default compose(
  connect(
    ({ auth }) => ({ auth }),
    { logoutUser }
  ),
  withRouter,
  withState('anchorEl', 'setAnchorEl', null),
  withHandlers({
    onOpen: ({ setAnchorEl }) => event => setAnchorEl(event.currentTarget),
    onClose: ({ setAnchorEl }) => () => setAnchorEl(null),
    onLogin: ({ history }) => () => history.push('/login'),
    onLogout: ({ logoutUser, history }) => () => {
      logoutUser(history)
      history.push('/login')
    }
  }),
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Appbar)
