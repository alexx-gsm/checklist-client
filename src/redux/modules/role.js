import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'roleStore'

// --- ACTIONS ---
export const ROLE_LOADING = `${appName}/${moduleName}/ROLE_LOADING`
export const ROLE_STORE = `${appName}/${moduleName}/ROLE_STORE`
export const ROLE_SAVE = `${appName}/${moduleName}/ROLE_SAVE`
export const ROLE_GET_ALL = `${appName}/${moduleName}/ROLE_GET_ALL`
export const ROLE_ERROR = `${appName}/${moduleName}/ROLE_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  roles: {},
  role: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case ROLE_LOADING:
      return state.set('loading', true)
    case ROLE_STORE:
      return state
        .set('loading', false)
        .set('role', payload)
        .set('error', {})
    case ROLE_GET_ALL:
      return state
        .set('loading', false)
        .set('roles', payload)
        .set('error', {})
    case ROLE_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE ROLE
 */
export const storeRole = payload => ({
  type: ROLE_STORE,
  payload
})
/**
 * * SAVE ROLE
 */
export const saveRole = (payload, cb) => dispatch => {
  console.log('AC --- SAVE ROLE')
  axios
    .post(`${HOST}/api/roles/`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ROLE_ERROR,
        error
      })
    })
}
/**
 * * UPDATE ROLE
 */
export const updateRole = (payload, cb) => dispatch => {
  console.log('AC --- PUT ROLE')
  axios
    .put(`${HOST}/api/roles/`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ROLE_ERROR,
        error
      })
    })
}
/**
 * * GET ALL ROLES
 */
export const getRoles = () => dispatch => {
  dispatch({
    type: ROLE_LOADING
  })
  axios
    .post(`${HOST}/api/roles/all`)
    .then(res => {
      console.log('AC --- GET ALL ROLES: ok')
      dispatch({
        type: ROLE_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL ROLES: err')
      dispatch({
        type: ROLE_ERROR,
        error
      })
    })
}
/**
 * * GET ROLE BY ID
 */
export const getRoleById = id => dispatch => {
  dispatch({
    type: ROLE_LOADING
  })
  axios
    .post(`${HOST}/api/roles/${id}`)
    .then(res => {
      dispatch({
        type: ROLE_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: ROLE_ERROR,
        error
      })
    )
}

/**
 * * clear role
 */
export const clearRole = () => ({
  type: ROLE_STORE,
  payload: {}
})

export default reducer
