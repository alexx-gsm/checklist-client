const drawerWidth = 200

export default theme => ({
  root: {
    flexGrow: 1,
    minHeight: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
    backgroundColor: theme.palette.background.default
  },
  toolbar: theme.mixins.toolbar,
  wrap: {
    height: '100%'
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100%',
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    }
  },
  Content: {
    position: 'relative',
    paddingLeft: '65px',
    paddingRight: '65px',
    maxWidth: '1024px',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    paddingBottom: 0,
    boxSizing: 'border-box',
    [theme.breakpoints.down('sm')]: {
      padding: '12px'
    },
    [theme.breakpoints.down('xs')]: {
      padding: '8px'
    }
  }
})
