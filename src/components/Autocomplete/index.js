import Autocomplete from './Autocomplete'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

export default withStyles(styles, {
  theme: true
})(Autocomplete)
