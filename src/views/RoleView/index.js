import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'

const RoleList = lazy(() => import('./RoleList'))
const RoleEdit = lazy(() => import('./RoleEdit'))

const basePath = '/admin/roles'

const RoleView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <RoleList basePath={basePath} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <RoleEdit basePath={basePath} />}
    />
  </Suspense>
)

export default RoleView
