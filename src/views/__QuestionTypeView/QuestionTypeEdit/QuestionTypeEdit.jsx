import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'orange'

const QuestionTypeEdit = ({
  qtype,
  error,
  classes,
  onChange,
  onChangeItem,
  onSave,
  onDelete,
  onReload,
  onAddRow,
  onDeleteRow,
  basePath
}) => {
  const { id, title, answers } = qtype
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link={basePath}
        returnButton={true}
        onReload={onReload}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Question Type
            </Typography>
          </Grid>
        </Paper>
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            className={classes.textName}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes[COLOR],
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputName,
              classes: {
                underline: classNames(classes.underlinePrimary, COLOR)
              }
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          <Table className={classes.Table}>
            <TableHead>
              <TableRow className={classes.TableHeadRow}>
                <TableCell />
                <TableCell className={classes.TableHeadCell}>
                  Варианты ответа
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {!isEmpty(answers) &&
                answers.map((answer, index) => {
                  return (
                    <TableRow key={index} className={classes.TableBodyRow}>
                      <TableCell
                        className={classNames(
                          classes.removeCell,
                          'icon-remove'
                        )}
                      >
                        <RemoveCircleOutline
                          className={classes.removeIcon}
                          onClick={onDeleteRow(index)}
                        />
                      </TableCell>
                      <TableCell className={classes.TableCellTitle}>
                        <TextField
                          fullWidth
                          error={Boolean(error.title)}
                          className={classes.selectItem}
                          value={!isEmpty(answer) ? answer : ''}
                          onChange={onChangeItem(index)}
                          InputProps={{
                            classes: {
                              underline: classNames(
                                classes.underlinePrimary,
                                isEmpty(answer) ? COLOR : 'NoColor'
                              )
                            }
                          }}
                          helperText={
                            !isEmpty(error.dishes) ? error.dishes : ''
                          }
                          FormHelperTextProps={{
                            className: classes.selectError
                          }}
                          margin='none'
                        />
                      </TableCell>
                    </TableRow>
                  )
                })}
              <TableRow className={classes.TableBodyRow}>
                <TableCell className={'icon-add'}>
                  <AddCircle className={classes.addIcon} onClick={onAddRow} />
                </TableCell>
                <TableCell />
              </TableRow>
            </TableBody>
          </Table>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.buttonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              className={classNames(classes.btnPrimary, COLOR)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

QuestionTypeEdit.propTypes = {
  qtype: PropTypes.shape({
    title: PropTypes.string,
    answers: PropTypes.arrayOf(PropTypes.string)
  }),
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default QuestionTypeEdit
