import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
// Material UI/icons
import FiberNew from '@material-ui/icons/FiberNew'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO } from 'react-icons-kit/fa/envelopeO'
import { trashO } from 'react-icons-kit/fa/trashO'
import { key } from 'react-icons-kit/fa/key'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'blueGrey'

const UserEdit = ({
  user,
  roles,
  error,
  classes,
  onReload,
  onChange,
  onSave,
  onDelete,
  loading,
  basePath
}) => {
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link={basePath}
        returnButton={true}
        loading={loading}
        onReload={onReload}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              {isEmpty(user) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Пользователь
            </Typography>
          </Grid>
        </Paper>
        <form noValidate className={classes.Form} noValidate autoComplete='off'>
          <Paper className={classes.PaperLogin}>
            <TextField
              fullWidth
              id='email'
              label='Почта'
              type='email'
              value={isEmpty(user.email) ? '' : user.email}
              onChange={onChange('email')}
              className={classes.select}
              margin='normal'
              InputLabelProps={{
                FormLabelClasses: {
                  root: classes.labelPrimary,
                  focused: classes.cssFocused
                }
              }}
              InputProps={{
                classes: { underline: classes.underlinePrimary },
                endAdornment: (
                  <InputAdornment position='end'>
                    <Icon icon={envelopeO} />
                  </InputAdornment>
                )
              }}
              error={Boolean(error.email)}
              helperText={error.email ? error.email : null}
            />
            {isEmpty(user.id) && (
              <React.Fragment>
                <TextField
                  fullWidth
                  id='password'
                  label='Пароль'
                  type='password'
                  value={isEmpty(user.password) ? '' : user.password}
                  onChange={onChange('password')}
                  className={classes.select}
                  margin='normal'
                  InputLabelProps={{
                    FormLabelClasses: {
                      root: classes.labelPrimary,
                      focused: classes.cssFocused
                    }
                  }}
                  InputProps={{
                    classes: { underline: classes.underlinePrimary },
                    endAdornment: (
                      <InputAdornment position='end'>
                        <Icon icon={key} />
                      </InputAdornment>
                    )
                  }}
                  error={Boolean(error.password)}
                  helperText={error.password ? error.password : null}
                />
                <TextField
                  fullWidth
                  id='password2'
                  label='Пароль 2'
                  type='password'
                  value={isEmpty(user.password2) ? '' : user.password2}
                  onChange={onChange('password2')}
                  className={classes.select}
                  margin='normal'
                  InputLabelProps={{
                    FormLabelClasses: {
                      root: classes.labelPrimary,
                      focused: classes.cssFocused
                    }
                  }}
                  InputProps={{
                    classes: { underline: classes.underlinePrimary },
                    endAdornment: (
                      <InputAdornment position='end'>
                        <Icon icon={key} />
                      </InputAdornment>
                    )
                  }}
                  error={Boolean(error.password2)}
                  helperText={error.password2 ? error.password2 : null}
                />
              </React.Fragment>
            )}
          </Paper>
          <TextField
            fullWidth
            id='name'
            label='ФИО'
            value={isEmpty(user.name) ? '' : user.name}
            onChange={onChange('name')}
            margin='dense'
            className={classes.textName}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputName,
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position='end'>
                  <Typography variant='caption'>RU</Typography>
                </InputAdornment>
              )
            }}
            error={Boolean(error.name)}
            helperText={error.name ? error.name : null}
          />
          <TextField
            fullWidth
            id='nameEn'
            label='Nickname'
            type='nameEn'
            value={isEmpty(user.nameEn) ? '' : user.nameEn}
            onChange={onChange('nameEn')}
            className={classes.select}
            margin='normal'
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position='end'>
                  <Typography variant='caption'>EN</Typography>
                </InputAdornment>
              )
            }}
            error={Boolean(error.nameEn)}
            helperText={error.nameEn ? error.nameEn : null}
          />
          <TextField
            select
            fullWidth
            id='role-select'
            label='Роль'
            value={isEmpty(user.role) ? '' : user.role}
            onChange={onChange('role')}
            margin='normal'
            className={classes.wrapTitle}
            InputLabelProps={{
              // shrink: true,
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputTitle,
              classes: { underline: classes.underlinePrimary }
            }}
            SelectProps={{
              classes: {
                select: classes.selectInput
              }
            }}
            error={Boolean(error.role)}
            helperText={error.role ? error.role : null}
          >
            {Object.keys(roles).map(key => {
              const role = roles[key]
              return (
                <MenuItem key={key} value={key}>
                  <Grid
                    container
                    alignItems='baseline'
                    className={classes.gridType}
                    direction='row'
                    wrap='wrap'
                  >
                    <Typography
                      className={classes.typoSelectInputTitle}
                      variant='title'
                    >
                      {role.title}
                    </Typography>
                    <Typography
                      className={classes.typoSelectInputSubtitle}
                      variant='caption'
                      style={{ marginLeft: '5px' }}
                    >
                      ({role.alias})
                    </Typography>
                  </Grid>
                </MenuItem>
              )
            })}
          </TextField>
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.buttonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              className={classNames(classes.btnPrimary, COLOR)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

UserEdit.defaultProps = {
  user: {},
  roles: {},
  error: {}
}

UserEdit.propTypes = {
  user: PropTypes.object,
  roles: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onReload: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  basePath: PropTypes.string
}

export default UserEdit
