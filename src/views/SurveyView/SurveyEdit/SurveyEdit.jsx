import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// DnD
import {
  sortableContainer,
  sortableElement,
  sortableHandle
} from 'react-sortable-hoc'

// Components
import ToolPanel from '../../../components/ToolPanel'
import ColorPicker from '../../../components/ColorPicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import Switch from '@material-ui/core/Switch'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  List,
  ListItem
} from '@material-ui/core'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'
// icons
import { Icon } from 'react-icons-kit'
import { plusCircle } from 'react-icons-kit/fa/plusCircle'
import { ic_remove_circle_outline } from 'react-icons-kit/md/ic_remove_circle_outline'
import { trashO } from 'react-icons-kit/fa/trashO'
import { arrowsV } from 'react-icons-kit/fa/arrowsV'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const SurveyEdit = ({
  survey,
  questionList,
  restQuestions,
  loading,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onReload,
  onChangeItem,
  onAddRow,
  onDeleteRow,
  theme,
  basePath,
  showSelect,
  onSortEnd
}) => {
  const { id, title, questions } = survey

  const DragHandle = sortableHandle(() => (
    <Icon icon={arrowsV} className={classes.IconArrowsV} />
  ))

  const SortableItem = sortableElement(({ num, value }) => (
    <ListItem className={classes.DND_Item} divider>
      <Grid container alignItems='center' justify='space-between' wrap='nowrap'>
        <Grid item>
          <Icon
            className={classes.removeIcon}
            onClick={onDeleteRow(num)}
            icon={ic_remove_circle_outline}
            size={28}
          />
        </Grid>
        <Grid item container wrap='nowrap'>
          <Typography variant='h5' className={classes.TypoNumber}>
            {num + 1}.
          </Typography>
          <Typography variant='h6'>{questionList[value].title}</Typography>
        </Grid>
        <Grid item container xs={2} sm={1} justify='flex-end'>
          <DragHandle />
        </Grid>
      </Grid>
    </ListItem>
  ))

  const SortableContainer = sortableContainer(({ children }) => {
    return <List className={classes.DND_List}>{children}</List>
  })

  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
        loading={loading}
      />
      <Paper className={classNames(classes.Card, classes.SurveyEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Survey
            </Typography>
          </Grid>
        </Paper>
        {/* Form */}
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            multiline
            rowsMax='4'
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            InputProps={{
              className: classes.Input40px
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          <Typography variant='h6' className={classes.TypoListHeader}>
            Вопросы
          </Typography>

          {!isEmpty(questions) && (
            <SortableContainer onSortEnd={onSortEnd} useDragHandle>
              {questions.map((value, index) => (
                <SortableItem
                  key={`item-${index}`}
                  index={index}
                  num={index}
                  value={value}
                />
              ))}
            </SortableContainer>
          )}

          {!isEmpty(restQuestions) && showSelect && (
            <TextField
              select
              fullWidth
              label='Добавить вопрос'
              id='question'
              value={''}
              onChange={onChangeItem}
              margin='normal'
              variant='outlined'
              className={classes.wrapTitle}
              InputLabelProps={{
                shrink: true
              }}
              InputProps={{
                className: classes.Title
              }}
              error={Boolean(error.role)}
              helperText={error.role ? error.role : null}
            >
              {Object.keys(restQuestions).map(key => {
                const item = restQuestions[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>
          )}

          {!isEmpty(restQuestions) && !showSelect && (
            <Grid container justify='center'>
              <div
                className={classNames(classes.addIcon, classes.extraAddIcon)}
                onClick={onAddRow}
              >
                <Icon icon={plusCircle} size={48} />
              </div>
            </Grid>
          )}
        </form>

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.CardButtonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              question='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

SurveyEdit.defaultProps = {
  answer: {},
  companies: {}
}

export default SurveyEdit
