import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import TopPanel from '../../../../components/TopPanel'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import { MuiThemeProvider } from '@material-ui/core/styles'
// Material UI/icons
import FiberNew from '@material-ui/icons/FiberNew'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO } from 'react-icons-kit/fa/envelopeO'
import { trashO } from 'react-icons-kit/fa/trashO'
import { key } from 'react-icons-kit/fa/key'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const ResearchEdit = ({
  research,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  loading,
  basePath,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <TopPanel title='Исследование' />

            <form
              noValidate
              className={classes.Form}
              noValidate
              autoComplete='off'
            >
              <TextField
                fullWidth
                id='title'
                label='Название'
                value={isEmpty(research.title) ? '' : research.title}
                onChange={onChange('title')}
                margin='dense'
                className={classes.textName}
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  className: classes.inputName,
                  classes: { underline: classes.underlinePrimary }
                }}
                error={Boolean(error.title)}
                helperText={error.title ? error.title : null}
              />
            </form>

            <Grid container className={classes.MT20} justify='space-between'>
              <Grid item>
                <Button variant='outlined' to={`${basePath}`} component={Link}>
                  К списку
                </Button>
              </Grid>
              <Grid item>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                >
                  Отмена
                </Button>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  className={classNames(classes.btnPrimary)}
                  color='primary'
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>

    //   <Divider />

    //   <Grid container className={classes.cardFooter}>
    //     <Grid item>
    //       <Fab
    //         size='small'
    //         aria-label='Delete'
    //         className={classes.buttonDelete}
    //         onClick={onDelete}
    //       >
    //         <Icon size={18} icon={trashO} />
    //       </Fab>
    //     </Grid>
    //     <Grid item>

    //     </Grid>
    //   </Grid>
    // </Paper>
  )
}

ResearchEdit.defaultProps = {
  reseach: {},
  error: {}
}

ResearchEdit.propTypes = {
  user: PropTypes.object,
  roles: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onReload: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  basePath: PropTypes.string
}

export default ResearchEdit
