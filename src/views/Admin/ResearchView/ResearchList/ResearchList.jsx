import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// @material-ui components
import { Grid, Typography, Button } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
import TopPanel from '../../../../components/TopPanel'

const ResearchList = ({
  researches,
  basePath,
  loading,
  editHandler,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <TopPanel title='Исследования' />
            <List>
              {Object.keys(researches).map(key => {
                const research = researches[key]
                return (
                  <ListItem
                    key={research.id}
                    button
                    divider
                    onClick={editHandler(key)}
                  >
                    <ListItemText primary={research.title} />
                  </ListItem>
                )
              })}
            </List>
            <Grid container className={classes.GridButtons} justify-xs-center>
              <Button
                variant='outlined'
                color='secondary'
                to={`${basePath}/edit`}
                component={Link}
              >
                Добавить
              </Button>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

ResearchList.defaultProps = {
  researches: {}
}

ResearchList.propTypes = {
  researches: PropTypes.object,
  loading: PropTypes.bool,
  classes: PropTypes.object
}

export default ResearchList
