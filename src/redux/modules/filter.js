import { Record } from 'immutable'
import { appName } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'filterStore'

// --- ACTIONS ---
export const FILTER_SET = `${appName}/${moduleName}/FILTER_SET`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  filter: {}
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload }) => {
  switch (type) {
    case FILTER_SET:
      return state.set('filter', {
        ...state.filter,
        [payload.key]: payload.value
      })
    default:
      return state
  }
}

// --- AC ---
/**
 * * SET FILTER
 */
export const setFilter = payload => ({
  type: FILTER_SET,
  payload
})

/**
 * * CLEAR FILTER
 */
export const clearFilter = () => ({
  type: FILTER_SET,
  payload: {}
})

export default reducer
