import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import { getAnswers, clearAnswer } from '../../../redux/modules/answer'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

// Component
import AnswerList from './AnswerList'

const FILTER_KEY = 'ANSWER'

export default compose(
  connect(
    ({ answerStore, filterStore }) => {
      const { answers, loading, error } = answerStore
      const { filter } = filterStore
      const filteredAnswers =
        filter[FILTER_KEY] === ''
          ? answers
          : Object.keys(answers).reduce((acc, key) => {
              const item = answers[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        answers: filteredAnswers,
        loading,
        error
      }
    },
    { getAnswers, clearAnswer }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getAnswers }) => () => getAnswers(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearAnswer()
      this.props.getAnswers()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(AnswerList)
