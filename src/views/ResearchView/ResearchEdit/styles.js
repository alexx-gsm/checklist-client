export default theme => ({
  PaperRange: {
    margin: '20px 0',
    padding: '20px',
    background: theme.palette.extraCardColor[50]
  },
  Upload: {
    display: 'none'
  },
  UploadButton: {
    margin: theme.spacing.unit
  }
})
