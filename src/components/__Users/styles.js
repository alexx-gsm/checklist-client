import deepOrange from '@material-ui/core/colors/deepOrange'
import grey from '@material-ui/core/colors/grey'
import blue from '@material-ui/core/colors/blue'
import red from '@material-ui/core/colors/red'

export default theme => ({
  Card: {
    maxWidth: '940px',
    margin: '40px 0',
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    padding: '5px 10px',
    color: theme.palette.common.white,
    '&.blue': {
      background: `linear-gradient(60deg, ${blue[500]}, ${blue[600]})`
    },
    '&.orange': {
      background: `linear-gradient(60deg, ${deepOrange[500]}, ${
        deepOrange[600]
      })`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  // TABLE
  Table: {
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  TableRow: {
    cursor: 'pointer',
    '& td': { background: blue[100] },
    '&:nth-child(odd) td': {
      background: blue[50]
    },
    '&:hover td': { background: blue[200] }
  },
  cellData: {
    width: '100%'
  }
})
