import React from 'react'
import CriteriaAnswerDesktop from './CriteriaAnswerDesktop'
import CriteriaAnswerMobile from './CriteriaAnswerMobile'
import { Hidden } from '@material-ui/core'

const CriteriaAnswer = props => (
  <React.Fragment>
    <Hidden xsDown>
      <CriteriaAnswerDesktop {...props} />
    </Hidden>
    <Hidden smUp>
      <CriteriaAnswerMobile {...props} />
    </Hidden>
  </React.Fragment>
)

export default CriteriaAnswer
