import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { withRouter } from 'react-router-dom'
// styles & theme
import CardStyles from '../../../styles/CardStyles'
import styles from './styles'
import theme from './theme'
import tabs from './tabs'
// @material ui
import { withStyles } from '@material-ui/core'
// Component
import TabsView from './TabsView'

export default compose(
  withState('tabIndex', 'setTabIndex', 0),
  withProps({
    theme,
    tabs
  }),
  withRouter,
  withHandlers({
    onTabChange: ({ setTabIndex, basePath, history }) => (event, value) => {
      setTabIndex(value)
      history.push(`${basePath}/${tabs[value].link}`)
    },
    onChangeIndex: ({ setTabIndex, basePath, history }) => index => {
      setTabIndex(index)
      history.push(`${basePath}/${tabs[index].link}`)
    }
  }),
  lifecycle({
    componentDidMount() {
      const { pathname } = this.props.location
      this.props.tabs.map((tab, index) => {
        if (pathname.includes(tab.link)) {
          this.props.setTabIndex(index)
        }
      })
    }
  }),
  withStyles(
    { ...CardStyles(theme), ...styles(theme) },
    {
      withTheme: true
    }
  )
)(TabsView)
