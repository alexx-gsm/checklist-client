import red from '@material-ui/core/colors/red'
export default theme => ({
  PaperTable: {
    margin: '20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0',
      margin: '20px 0 0'
    }
  },
  Table: {
    marginTop: '0'
  },
  TableHeadRow: {
    height: '28px',
    minHeight: '28px',
    '& th': {
      backgroundColor:
        theme.palette.extraCardColor[500] || theme.palette.grey[500],
      color: theme.palette.common.white,
      padding: '5px',
      textTransform: 'uppercase',
      '&:last-child': { padding: '5px' }
    }
  },
  TableHeadCell: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.common.white
  },
  TableBodyRow: {
    height: '28px',
    // '&:first-child td': { paddingTop: '10px' },
    '&:nth-child(even) td': {
      backgroundColor:
        theme.palette.extraCardColor[50] || theme.palette.grey[50]
    },
    // '&:last-child td': { backgroundColor: 'white' },
    '& td': {
      borderBottom: 'none',
      // padding: '0',
      fontSize: '1rem',
      // '&:not(:first-child)': { padding: '0 5px' },
      // '&:last-child': { padding: '5px' },
      '&.icon-remove': {
        paddingLeft: '10px',
        paddingRight: '10px',
        paddingTop: '5px',
        minWidth: '35px',
        width: '55px',
        boxSizing: 'border-box'
      },
      '&.icon-add': {
        paddingTop: '10px'
      }
    },
    '& td input': {
      width: '100%'
      // padding: 0
    }
  },
  TableCellTitle: {
    // width: '100%'
  },
  removeIcon: {
    fontSize: '35px',
    color: theme.palette.grey[400],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: red[800],
      transition: 'color ease 0.3s'
    }
  },
  CellAddButton: {
    width: '45px'
  },
  addIcon: {
    fontSize: '40px',
    color: theme.palette.grey[600],
    transition: 'color ease 0.3s',
    cursor: 'pointer',
    '&:hover': {
      color: theme.palette.grey[700],
      transition: 'color ease 0.3s'
    }
  },
  underlineMainCardColor: {
    '&:before': {
      borderBottomColor: 'transparent'
      // theme.palette.extraCardColor[500] ||
      // theme.palette.grey[500] + '!important'
    },
    '&.is-empty:before': {
      borderBottomColor:
        theme.palette.extraCardColor[500] ||
        theme.palette.grey[500] + '!important'
    },
    '&:after': {
      borderBottomColor:
        theme.palette.extraCardColor[500] || theme.palette.grey[500]
    }
  }
})
