import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const ResearchList = lazy(() => import('./ResearchList'))
const ResearchEdit = lazy(() => import('./ResearchEdit'))

const basePath = '/admin/researches'

const ResearchView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <ResearchList basePath={basePath} theme={theme} />}
    />
    <Route
      path={`${basePath}/edit/:id?`}
      render={() => <ResearchEdit basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default ResearchView
