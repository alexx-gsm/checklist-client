import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import Switch from '@material-ui/core/Switch'
import { MobileStepper, Stepper, Step, StepLabel } from '@material-ui/core'
// @material-ui/icons
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'
// Addition Components
import LinkAnswer from './LinkAnswer'
import CriteriaAnswer from './CriteriaAnswer'
import AssessmentAnswer from './AssessmentAnswer'

const SurveyRun = ({
  survey,
  stage,
  surveyQuestions,
  question,
  answer,
  respondent,
  onChange,
  onClick,
  onFind,
  error,
  loading,
  theme,
  classes,
  handleNext,
  handleBack,
  handleFinish
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid container direction='column' className={classes.GridContent}>
        {loading && <LinearIndeterminate />}

        {!loading && stage === -1 && (
          <form
            className={classNames(classes._Form, classes.MT20)}
            onSubmit={onFind}
            noValidate
            autoComplete='off'
          >
            <Grid
              container
              spacing={16}
              alignItems='flex-end'
              wrap='nowrap'
              className={classNames(classes.Container, classes.MB20)}
            >
              <Grid item>
                <TextField
                  fullWidth
                  label='Респондент'
                  id='respondent'
                  margin='none'
                  value={survey.respondentId ? survey.respondentId : ''}
                  onChange={onChange('respondentId')}
                  className={classes.wrapTitle}
                  placeholder='S00'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={Boolean(error.respondent)}
                  helperText={error.respondent ? error.respondent : null}
                />
              </Grid>
              <Grid item>
                <Button
                  onClick={onFind}
                  variant='contained'
                  className={classes.button}
                  color='primary'
                  size='large'
                >
                  Найти
                </Button>
              </Grid>
            </Grid>

            <Divider />

            <Grid
              container
              spacing={16}
              className={classNames(classes.Container, classes.MT20)}
            >
              {!isEmpty(survey.results) &&
                Object.keys(survey.results).map(key => {
                  return (
                    <Grid item key={key}>
                      <Button
                        variant='contained'
                        color='secondary'
                        className={classes.button}
                        onClick={() => onClick(key)}
                      >
                        {key}
                      </Button>
                    </Grid>
                  )
                })}
            </Grid>
          </form>
        )}

        {!loading && stage > -1 && (
          <React.Fragment>
            <Paper className={classes.QuestionPaper}>
              <Typography variant='h6' className={classes.QuestionTitle}>
                {question.title}
              </Typography>
              <Grid
                container
                direction='column'
                wrap='nowrap'
                className={classes.QuestionContent}
              >
                {answer.isMembersInAnswer && (
                  <LinkAnswer
                    onChange={onChange}
                    question={question}
                    theme={theme}
                  />
                )}
                {answer.isCriterias && (
                  <CriteriaAnswer
                    criterias={question.criterias}
                    answer={answer}
                  />
                )}
                {answer.isMembersInQuestion && (
                  <AssessmentAnswer answer={answer} />
                )}
              </Grid>

              {survey.questions && stage === survey.questions.length - 1 && (
                <Grid
                  container
                  justify='center'
                  className={classes.GridFinishButton}
                >
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={handleFinish}
                  >
                    Завершить опрос
                  </Button>
                </Grid>
              )}
            </Paper>

            <Grid className={classes.GridStepper}>
              <MobileStepper
                variant='progress'
                steps={Object.keys(surveyQuestions).length}
                position='static'
                activeStep={stage}
                className={classes.mobileStepper}
                classes={{
                  root: classes.StepperRoot
                }}
                nextButton={
                  <Button
                    size='small'
                    onClick={handleNext}
                    disabled={stage === Object.keys(surveyQuestions).length - 1}
                  >
                    След.
                    <KeyboardArrowRight />
                  </Button>
                }
                backButton={
                  <Button
                    size='small'
                    onClick={handleBack}
                    disabled={stage === 0}
                  >
                    <KeyboardArrowLeft />
                    Пред.
                  </Button>
                }
              />
            </Grid>
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

export default SurveyRun
