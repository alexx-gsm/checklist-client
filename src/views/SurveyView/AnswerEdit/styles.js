export default theme => ({
  AnswerEdit: {},
  typoWithWeight: {
    fontWeight: theme.typography.fontWeightMedium,
    '&.is-checked': {
      color: theme.palette.common.white
    },
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    }
  },
  textCenter: {
    textAlign: 'center'
  },
  autoHeight: {
    height: 'auto'
  },
  PaperSelectLinkAnswer: {
    marginTop: '30px',
    padding: '20px',
    background: theme.palette.extraCardColor[50]
  },
  LabelTargetSwitch: {
    color: theme.palette.grey[600],
    fontSize: '18px'
  },
  FormControlRadio: {
    marginLeft: '45px'
  }
})
