import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import ColorPicker from '../../../components/ColorPicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import Switch from '@material-ui/core/Switch'
import {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio
} from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
import { download2 } from 'react-icons-kit/icomoon/download2'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const QuestionEdit = ({
  question,
  answers,
  uniqueQuestionId,
  filteredCriterias,
  loading,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onUpload,
  onReload,
  onChangeItem,
  onAddRow,
  onDeleteRow,
  theme,
  basePath
}) => {
  const { id, title, target, answerId, criteriaQuestionId, file } = question
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
        loading={loading}
      />
      <Paper className={classNames(classes.Card, classes.QuestionEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Question
            </Typography>
          </Grid>
        </Paper>
        {/* Form */}
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            multiline
            rowsMax='6'
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            InputProps={{
              className: classes.Input40px
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          {answers && (
            <TextField
              select
              fullWidth
              id='answerId'
              label='Answer'
              value={isEmpty(answerId) ? '' : answerId}
              onChange={onChange('answerId')}
              margin='normal'
              className={classes.wrapTitle}
              InputProps={{
                className: classes.Title
              }}
              error={Boolean(error.role)}
              helperText={error.role ? error.role : null}
            >
              {Object.keys(answers).map(key => {
                const item = answers[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>
          )}

          {answers && answerId && answers[answerId].isCriterias && (
            <Table className={classNames(classes.Table, classes.extraTable)}>
              <TableHead>
                <TableRow className={classes.TableHeadRow}>
                  <TableCell />
                  <TableCell width='100%'>
                    <Grid container justify='space-between' alignItems='center'>
                      <Grid item>
                        <Typography
                          variant='caption'
                          className={classes.TableHeadCell}
                        >
                          Критерии
                        </Typography>
                      </Grid>
                      <Grid item>
                        <TextField
                          select
                          id='answerId'
                          value={
                            isEmpty(criteriaQuestionId)
                              ? ''
                              : criteriaQuestionId
                          }
                          onChange={onChange('criteriaQuestionId')}
                          margin='none'
                          className={classes.wrapTitle}
                          InputProps={{
                            className: classes.Title
                          }}
                          error={Boolean(error.role)}
                          helperText={error.role ? error.role : null}
                        >
                          {uniqueQuestionId.map(_id => {
                            return (
                              <MenuItem key={_id} value={_id}>
                                <Grid
                                  container
                                  alignItems='baseline'
                                  className={classes.gridType}
                                  direction='row'
                                  wrap='wrap'
                                >
                                  <Typography
                                    className={classes.typoSelectInputTitle}
                                    variant='h6'
                                  >
                                    {_id}
                                  </Typography>
                                </Grid>
                              </MenuItem>
                            )
                          })}
                        </TextField>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredCriterias &&
                  Object.keys(filteredCriterias).map((key, index) => {
                    const item = filteredCriterias[key]
                    return (
                      <TableRow key={key} className={classes.TableBodyRow}>
                        <TableCell
                          className={classNames(
                            classes.removeCell,
                            'icon-remove'
                          )}
                        >
                          <RemoveCircleOutline
                            className={classes.removeIcon}
                            onClick={onDeleteRow(index)}
                          />
                        </TableCell>
                        <TableCell className={classes.TableCellTitle}>
                          <TextField
                            fullWidth
                            error={Boolean(
                              !isEmpty(error.item) && error.item.title
                            )}
                            className={classes.selectItem}
                            value={
                              !isEmpty(item.criterion) ? item.criterion : ''
                            }
                            onChange={onChangeItem(index)}
                            name='criterion'
                            InputProps={{
                              classes: {
                                underline: classNames(
                                  classes.underlineMainCardColor,
                                  isEmpty(item.criterion) ? 'is-empty' : ''
                                )
                              }
                            }}
                            FormHelperTextProps={{
                              className: classes.selectError
                            }}
                            margin='none'
                          />
                        </TableCell>
                      </TableRow>
                    )
                  })}
                <TableRow className={classes.TableBodyRow}>
                  <TableCell
                    colSpan={2}
                    className={classNames(classes.CellAddButton, 'icon-add')}
                  >
                    <Grid
                      container
                      alignItems='center'
                      spacing={16}
                      wrap='nowrap'
                    >
                      <Grid item>
                        <AddCircle
                          className={classes.addIcon}
                          onClick={onAddRow}
                        />
                      </Grid>
                      <Grid
                        item
                        container
                        wrap='nowrap'
                        alignItems='center'
                        spacing={16}
                      >
                        <Grid item>
                          <input
                            accept='file/*'
                            name='file'
                            className={classes.Upload}
                            id='upload'
                            type='file'
                            onChange={onChange('file')}
                          />
                          <label htmlFor='upload'>
                            <Button
                              variant='contained'
                              color='secondary'
                              component='span'
                              className={classes.UploadButton}
                              size='small'
                            >
                              Select
                            </Button>
                          </label>
                        </Grid>
                        {file ? (
                          <Grid
                            item
                            container
                            wrap='nowrap'
                            alignItems='center'
                            spacing={16}
                          >
                            <Grid item>
                              <Typography variant='caption'>
                                {file.name}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Fab
                                size='small'
                                aria-label='Back'
                                className={classes.buttonUpload}
                                color='secondary'
                                onClick={onUpload}
                              >
                                <Icon icon={download2} />
                              </Fab>
                            </Grid>
                          </Grid>
                        ) : (
                          ''
                        )}
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          )}

          <Paper className={classNames(classes.PaperTarget, target)}>
            <Grid container>
              <Grid item>
                <FormControl
                  component='fieldset'
                  className={classes.formControl}
                >
                  <FormLabel component='legend'>Target</FormLabel>
                  <RadioGroup
                    aria-label='Target'
                    name='target'
                    className={classes.group}
                    value={target}
                    onChange={onChange('target')}
                  >
                    <FormControlLabel
                      value='common'
                      control={
                        <Radio
                          classes={{
                            root: classNames(classes.RadioRoot, 'common'),
                            checked: classes.RadioChecked
                          }}
                        />
                      }
                      label='Общий (для всех)'
                    />
                    <FormControlLabel
                      value='member'
                      control={
                        <Radio
                          classes={{
                            root: classNames(classes.RadioRoot, 'member'),
                            checked: classes.RadioChecked
                          }}
                        />
                      }
                      label='Для сотрудников'
                    />
                    <FormControlLabel
                      value='boss'
                      control={
                        <Radio
                          classes={{
                            root: classNames(classes.RadioRoot, 'boss'),
                            checked: classes.RadioChecked
                          }}
                        />
                      }
                      label='Для руководителей'
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item />
            </Grid>
          </Paper>
        </form>

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.CardButtonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

QuestionEdit.defaultProps = {
  answer: {},
  companies: {}
}

export default QuestionEdit
