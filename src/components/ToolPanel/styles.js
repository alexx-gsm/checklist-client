export default theme => ({
  underlinePrimary: {
    '&:before': {
      borderBottomColor: theme.palette.primary.light
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main
    },
    '&:hover:not(.disabled):not(.focused):not(.error)': {
      '&:before': {
        borderBottomColor: theme.palette.primary.light
      }
    }
  },
  tools: {
    marginBottom: '10px',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
      marginTop: '20px'
    }
  },
  gridTools: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    }
  },
  gridBtnBack: {
    [theme.breakpoints.up('sm')]: {
      padding: '6px 20px'
    }
  },
  title: { textAlign: 'right' },
  buttonAdd: { marginLeft: '10px' },
  buttonUpload: { marginLeft: '10px' }
})
