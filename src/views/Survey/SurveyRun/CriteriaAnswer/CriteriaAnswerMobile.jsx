import React from 'react'
import classNames from 'classnames'
// @material-ui components
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
import { Typography, TextField, Grid, MenuItem } from '@material-ui/core'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const CriteriaAnswerMobile = ({
  result,
  criterias,
  answer,
  onChange,
  classes
}) => {
  return (
    <Grid
      container
      direction='column'
      wrap='nowrap'
      className={classes.GridContainerMobile}
    >
      {criterias &&
        Object.keys(criterias).map((itemId, index) => {
          return (
            <Grid item className={classes.GridItemMobile} key={index}>
              <Typography variant='h6' className={classes.typoAutoInput}>
                {criterias[itemId].criterion}
              </Typography>

              {answer.variants && (
                <TextField
                  select
                  fullWidth
                  id='answerId'
                  value={isEmpty(result[index]) ? '' : result[index]}
                  onChange={onChange(index)}
                  className={classes.wrapTitle}
                  InputProps={{
                    className: classes.Title
                  }}
                >
                  {answer.variants.map((item, index) => {
                    return (
                      <MenuItem key={index} value={index}>
                        <Grid
                          container
                          alignItems='baseline'
                          className={classes.gridType}
                          direction='row'
                          wrap='wrap'
                        >
                          <Typography
                            className={classes.typoSelectInputTitle}
                            variant='h6'
                          >
                            {item.title}
                          </Typography>
                        </Grid>
                      </MenuItem>
                    )
                  })}
                </TextField>
              )}
            </Grid>
          )
        })}
    </Grid>
  )
}

export default CriteriaAnswerMobile
