export default theme => ({
  PaperTable: {
    margin: '20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0',
      margin: '20px 0 0'
    }
  },
  Table: {
    margin: '0 20px',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    }
  },
  TableRow: {
    '& td': { background: theme.palette.grey[50] },
    '&:nth-child(odd) td': {
      background: theme.palette.grey[100]
    }
  }
})
