import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const RespondentList = ({
  respondents,
  companies,
  departments,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  filter,
  onSearch,
  handleFilterClear,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        label={'ФИО'}
        loading={loading}
        onReload={onReload}
        search={{ filter, onSearch, handleFilterClear }}
      />
      <Paper className={classes.PaperTable}>
        <Grid container className={classes.gridTable}>
          <Table>
            <TableHead className={classes.TableHead}>
              <TableRow>
                <TableCell padding='checkbox' />
                <TableCell className={classes.headCellData} padding='checkbox'>
                  <Typography variant='caption'>ФИО</Typography>
                </TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(respondents).map(key => {
                const respondent = respondents[key]
                return (
                  <TableRow key={key} className={classes.TableRow}>
                    <TableCell padding='checkbox'>
                      <Typography variant='h5' className={classes.Key}>
                        {key}
                      </Typography>
                    </TableCell>
                    <TableCell
                      padding='checkbox'
                      className={classes.CellBordered}
                    >
                      <Grid container>
                        <Typography variant='h6' className={classes.Title}>
                          {respondent.fio}
                        </Typography>
                        <Grid item container direction='row' spacing={16}>
                          <Grid item>
                            <Typography
                              variant='caption'
                              className={classes.typoInn}
                            >
                              {!isEmpty(companies)
                                ? companies[respondent.companyId].title
                                : ''}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography variant='caption'>
                              отдел:{' '}
                              {!isEmpty(departments)
                                ? departments[respondent.departmentId].title
                                : ''}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell padding='none'>
                      <DropdownMenu
                        anchorEl={anchorEl}
                        updateAnchorEl={updateAnchorEl}
                        menuId={key}
                        menuList={[
                          {
                            icon: <IconEdit />,
                            label: 'Edit',
                            handler: editHandler(key)
                          },
                          {
                            icon: <IconDelete />,
                            label: 'Delete',
                            handler: deleteHandler(key)
                          }
                        ]}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

RespondentList.defaultProps = {
  companies: {},
  departments: {}
}

export default RespondentList
