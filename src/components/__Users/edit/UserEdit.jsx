import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../ToolPanel'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
// Material UI/icons
import FiberNew from '@material-ui/icons/FiberNew'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO } from 'react-icons-kit/fa/envelopeO'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const COLOR = 'blue'

const UserEdit = ({ staff, error, classes, onChange, onSave, onDelete }) => {
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link='/admin/users'
        returnButton={true}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              {isEmpty([]) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Пользователь
            </Typography>
          </Grid>
        </Paper>
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='name'
            label='ФИО'
            value={isEmpty(staff.name) ? '' : staff.name}
            onChange={onChange('name')}
            margin='dense'
            className={classes.textName}
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              className: classes.inputName,
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position='end'>
                  <Typography variant='caption'>RU</Typography>
                </InputAdornment>
              )
            }}
            error={Boolean(error.name)}
            helperText={error.name ? error.name : null}
          />
          <TextField
            fullWidth
            id='nameEn'
            label='en: ФИО'
            type='nameEn'
            value={isEmpty(staff.nameEn) ? '' : staff.nameEn}
            onChange={onChange('nameEn')}
            className={classes.select}
            margin='normal'
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position='end'>
                  <Typography variant='caption'>EN</Typography>
                </InputAdornment>
              )
            }}
          />
          <TextField
            fullWidth
            id='email'
            label='Почта'
            type='email'
            value={isEmpty(staff.email) ? '' : staff.email}
            onChange={onChange('email')}
            className={classes.select}
            margin='normal'
            InputLabelProps={{
              FormLabelClasses: {
                root: classes.labelPrimary,
                focused: classes.cssFocused
              }
            }}
            InputProps={{
              classes: { underline: classes.underlinePrimary },
              endAdornment: (
                <InputAdornment position='end'>
                  <Icon icon={envelopeO} />
                </InputAdornment>
              )
            }}
          />
        </form>

        <Divider />

        <Grid container className={classes.cardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.buttonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={'/admin/groups'}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              className={classNames(classes.btnPrimary, COLOR)}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

UserEdit.defaultProps = {
  staff: {},
  error: {}
}

UserEdit.propTypes = {
  staff: PropTypes.object,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
}

export default UserEdit
