const firebase = require('firebase')
// Required for side-effects
require('firebase/firestore')

firebase.initializeApp({
  apiKey: 'AIzaSyDzSNnhfx-Heek3KQCzOganPPrcaUhXO94',
  authDomain: 'admin-checklist-f70c8.firebaseapp.com',
  databaseURL: 'https://admin-checklist-f70c8.firebaseio.com',
  projectId: 'admin-checklist-f70c8',
  storageBucket: 'admin-checklist-f70c8.appspot.com',
  messagingSenderId: '197405494444'
})

console.log('--- init firebase')

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore()

// Disable deprecated features
db.settings({
  timestampsInSnapshots: true
})

export default db
