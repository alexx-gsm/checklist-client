import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
// @material-ui components
import { Paper, Grid, Typography, Fab, Divider } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
// @material-ui icons
import { PlayArrow, ShowChart } from '@material-ui/icons'
import { MuiThemeProvider } from '@material-ui/core/styles'

const SurveyList = ({ surveys, basePath, classes, theme }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Typography variant='h3' className={classes._PageTitle}>
        Опросы
      </Typography>

      {Object.keys(surveys).map(key => {
        const survey = surveys[key]
        return (
          <Grid container alignItems='center' spacing={8} key={key}>
            <Grid item>
              <Fab
                size='small'
                aria-label='Back'
                className={classes.buttonReturn}
                to={`${basePath}/${key}`}
                component={Link}
                color='primary'
              >
                <PlayArrow />
              </Fab>
              <Fab
                size='small'
                aria-label='Back'
                className={classes.buttonReturn}
                to={`${basePath}/graph`}
                component={Link}
                color='secondary'
              >
                <ShowChart />
              </Fab>
            </Grid>
            <Grid item>
              <Typography variant='h5'>{survey.title}</Typography>
            </Grid>
          </Grid>
        )
      })}
    </MuiThemeProvider>
  )
}

SurveyList.defaultProps = {
  surveys: {}
}

export default SurveyList
