export default theme => ({
  buttonMore: {
    padding: 0,
    width: '32px',
    minWidth: '32px',
    borderRadius: '50%'
  },
  buttonMoreLabel: {}
})
