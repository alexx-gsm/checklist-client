import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'qtypeStore'

// --- ACTIONS ---
export const QTYPE_LOADING = `${appName}/${moduleName}/QTYPE_LOADING`
export const QTYPE_STORE = `${appName}/${moduleName}/QTYPE_STORE`
export const QTYPE_SAVE = `${appName}/${moduleName}/QTYPE_SAVE`
export const QTYPE_GET_ALL = `${appName}/${moduleName}/QTYPE_GET_ALL`
export const QTYPE_ERROR = `${appName}/${moduleName}/QTYPE_ERROR`

const initialQtype = {
  title: '',
  answers: []
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  qtypes: {},
  qtype: initialQtype,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case QTYPE_LOADING:
      return state.set('loading', true)
    case QTYPE_STORE:
      return state
        .set('loading', false)
        .set('qtype', payload)
        .set('error', {})
    case QTYPE_GET_ALL:
      return state
        .set('loading', false)
        .set('qtypes', payload)
        .set('error', {})
    case QTYPE_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE QTYPE
 */
export const storeQtype = payload => ({
  type: QTYPE_STORE,
  payload
})
/**
 * * SAVE QTYPE
 */
export const saveQtype = (payload, cb) => dispatch => {
  console.log('AC --- SAVE QTYPE')
  axios
    .post(`${HOST}/api/qtypes`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: QTYPE_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE QTYPE
 */
export const updateQtype = (payload, cb) => dispatch => {
  console.log('AC --- PUT QTYPE')
  axios
    .put(`${HOST}/api/qtypes`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: QTYPE_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL QTYPES
 */
export const getQtypes = () => dispatch => {
  dispatch({
    type: QTYPE_LOADING
  })
  axios
    .post(`${HOST}/api/qtypes/all`)
    .then(res => {
      console.log('AC --- GET ALL QTYPES: ok')
      dispatch({
        type: QTYPE_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL QTYPES: err')
      dispatch({
        type: QTYPE_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET QTYPE BY ID
 */
export const getQtypeById = id => dispatch => {
  dispatch({
    type: QTYPE_LOADING
  })
  axios
    .post(`${HOST}/api/qtypes/${id}`)
    .then(res => {
      dispatch({
        type: QTYPE_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: QTYPE_ERROR,
        error
      })
    )
}

/**
 * * clear qtype
 */
export const clearQtype = () => ({
  type: QTYPE_STORE,
  payload: initialQtype
})

export default reducer
