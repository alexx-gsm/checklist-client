import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { storeStageResult } from '../../../../redux/modules/survey'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../../styles/CardStyles'
import CardTableStyles from '../../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

import CriteriaAnswer from './CriteriaAnswer'

export default compose(
  connect(
    ({ respondentStore, surveyStore }) => {
      const { respondents } = respondentStore
      const { result } = surveyStore

      return {
        respondents,
        result
      }
    },
    { storeStageResult }
  ),
  withHandlers({
    onChange: ({ result, storeStageResult }) => index => event => {
      console.log('index', index)

      storeStageResult({
        ...result,
        [index]: event.target.value
      })
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(CriteriaAnswer)
