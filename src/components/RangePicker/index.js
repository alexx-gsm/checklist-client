import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { DateUtils } from 'react-day-picker'

import RangePicker from './RangePicker'
import moment from 'moment'

export default compose(
  withState('range', 'setRange', { from: undefined, to: undefined }),
  withHandlers({
    handleDayClick: ({ range, setRange }) => day => {
      const newRange = DateUtils.addDayToRange(day, range)
      setRange(newRange)
    },
    onSubmit: ({ range, handleRange, handleClose }) => () => {
      handleRange(range)
      handleClose()
    },
    onReset: ({ setRange }) => () => {
      setRange({ from: undefined, to: undefined })
    }
  }),
  lifecycle({
    componentDidUpdate(prevProps) {
      if (prevProps.initialRange === undefined && this.props.initialRange) {
        const { from, to } = this.props.initialRange
        this.props.setRange({
          from: moment(from).toDate(),
          to: moment(to).toDate()
        })
      }
    }
  })
)(RangePicker)
