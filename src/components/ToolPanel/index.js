// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
// Component
import ToolPanel from './ToolPanel'

export default withStyles(styles, { withTheme: true })(ToolPanel)
