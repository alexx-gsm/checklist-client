import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'userStore'

// --- ACTIONS ---
export const USER_LOADING = `${appName}/${moduleName}/USER_LOADING`
export const USER_GET_ALL = `${appName}/${moduleName}/USER_GET_ALL`
export const USER_GET_BY_ID = `${appName}/${moduleName}/USER_GET_BY_ID`
export const USER_STORE = `${appName}/${moduleName}/USER_STORE`
export const USER_SET_FILTER = `${appName}/${moduleName}/USER_SET_FILTER`
export const USER_ERROR = `${appName}/${moduleName}/USER_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  users: {},
  user: {},
  filter: '',
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case USER_LOADING:
      return state.set('loading', true)
    case USER_GET_ALL:
      return state
        .set('loading', false)
        .set('users', payload)
        .set('filter', {})
        .set('error', {})
    case USER_GET_BY_ID:
      return state
        .set('loading', false)
        .set('user', payload)
        .set('error', {})
    case USER_STORE:
      return state
        .set('loading', false)
        .set('user', payload)
        .set('error', {})
    case USER_SET_FILTER:
      return state.set('filter', payload)
    case USER_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * get all users
 */
export const getUsers = () => dispatch => {
  dispatch({
    type: USER_LOADING
  })
  axios
    .post(`${HOST}/api/users/all`)
    .then(res => {
      console.log('AC --- GET ALL USERS: ok')
      dispatch({
        type: USER_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL USERS: err')
      dispatch({
        type: USER_ERROR,
        error
      })
    })
}
/**
 * * get user by id
 */
export const getUserById = id => dispatch => {
  dispatch({
    type: USER_LOADING
  })
  axios
    .post(`${HOST}/api/users/${id}`)
    .then(res => {
      dispatch({
        type: USER_GET_BY_ID,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: USER_ERROR,
        error
      })
    )
}

/**
 * * store user
 */
export const storeUser = payload => ({
  type: USER_STORE,
  payload
})
/**
 * * REGISTER (SAVE NEW) USER
 */
export const saveUser = (payload, cb) => dispatch => {
  console.log('AC --- SAVE USER')
  axios
    .post(`${HOST}/api/users/register`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: USER_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE USER
 */
export const updateUser = (payload, cb) => dispatch => {
  console.log('AC --- PUT USER')
  axios
    .put(`${HOST}/api/users/`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: USER_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * FILTER USERS
 */
export const filterUsers = filter => ({
  type: USER_SET_FILTER,
  payload: filter
})

/**
 * * CLEAR USERS
 */
export const clearUser = () => ({
  type: USER_STORE,
  payload: {}
})

export default reducer
