import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import RangePicker from '../../../components/RangePicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import {
  Switch,
  Checkbox,
  FormGroup,
  FormControlLabel
} from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'
import moment from 'moment'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const ResearchEdit = ({
  research,
  companies,
  departments,
  surveys,
  loading,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onReload,
  open,
  handleClickOpen,
  handleClickClose,
  onChangeItem,
  onAddRow,
  onDeleteRow,
  theme,
  basePath
}) => {
  const { id, title, companyId, departmentId, surveyId, range, file } = research
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
        loading={loading}
      />
      <Paper className={classNames(classes.Card, classes.ResearchEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Research
            </Typography>
          </Grid>
        </Paper>
        {/* Form */}
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            InputProps={{
              className: classes.Input40px
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />

          {/* COMPANY */}
          <TextField
            select
            fullWidth
            id='company'
            label='Company'
            value={isEmpty(companyId) ? '' : companyId}
            onChange={onChange('companyId')}
            margin='normal'
            className={classes.wrapTitle}
            InputProps={{
              className: classes.Title
            }}
            error={Boolean(error.role)}
            helperText={error.role ? error.role : null}
          >
            {Object.keys(companies).map(key => {
              const company = companies[key]
              return (
                <MenuItem key={key} value={key}>
                  <Grid
                    container
                    alignItems='baseline'
                    className={classes.gridType}
                    direction='row'
                    wrap='wrap'
                  >
                    <Typography
                      className={classes.typoSelectInputTitle}
                      variant='h6'
                    >
                      {company.title}
                    </Typography>
                    <Typography
                      className={classes.typoSelectInputSubtitle}
                      variant='caption'
                      style={{ marginLeft: '5px' }}
                    >
                      ( ИНН: {company.inn} )
                    </Typography>
                  </Grid>
                </MenuItem>
              )
            })}
          </TextField>

          {/* DEPARTMENT */}
          {!isEmpty(departments) && (
            <TextField
              select
              fullWidth
              id='department'
              label='Department'
              value={isEmpty(departmentId) ? '' : departmentId}
              onChange={onChange('departmentId')}
              margin='normal'
              className={classes.wrapTitle}
              InputProps={{
                className: classes.Title
              }}
              error={Boolean(error.role)}
              helperText={error.role ? error.role : null}
            >
              {Object.keys(departments).map(key => {
                const item = departments[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>
          )}

          {/* SURVEYS */}
          {!isEmpty(surveys) && (
            <TextField
              select
              fullWidth
              id='survey'
              label='Survey'
              value={isEmpty(surveyId) ? '' : surveyId}
              onChange={onChange('surveyId')}
              margin='normal'
              className={classes.wrapTitle}
              InputProps={{
                className: classes.Title
              }}
              error={Boolean(error.role)}
              helperText={error.role ? error.role : null}
            >
              {Object.keys(surveys).map(key => {
                const item = surveys[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>
          )}

          <Paper className={classes.PaperRange} square>
            <Grid container spacing={16} alignItems='center'>
              <Grid item>
                <TextField
                  disabled
                  id='from'
                  label='From'
                  value={!isEmpty(range) ? moment(range.from).format('L') : '-'}
                  className={classes.textField}
                  margin='dense'
                />
              </Grid>
              <Grid item>
                <TextField
                  disabled
                  id='to'
                  label='To'
                  value={!isEmpty(range) ? moment(range.to).format('L') : '-'}
                  className={classes.textField}
                  margin='dense'
                />
              </Grid>
              <Grid item>
                <Button
                  variant='outlined'
                  color='primary'
                  onClick={handleClickOpen}
                >
                  Select range
                </Button>

                <RangePicker
                  handleRange={onChange('range')}
                  open={open}
                  handleClose={handleClickClose}
                  initialRange={range}
                />
              </Grid>
            </Grid>
          </Paper>

          <Paper>
            <input
              accept='file/*'
              name='file'
              className={classes.Upload}
              id='upload'
              type='file'
              onChange={onChange('file')}
            />
            <label htmlFor='upload'>
              <Button
                variant='contained'
                component='span'
                className={classes.UploadButton}
              >
                Upload
              </Button>
            </label>
            <Typography variant='caption'>{file ? file.name : '-'}</Typography>
          </Paper>
        </form>

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.CardButtonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

ResearchEdit.defaultProps = {
  research: {},
  companies: {},
  departments: {}
}

export default ResearchEdit
