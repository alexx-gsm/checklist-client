import teal from '@material-ui/core/colors/teal'
import red from '@material-ui/core/colors/red'
import grey from '@material-ui/core/colors/grey'

export default theme => ({
  teal: {
    '&$cssFocused': {
      color: teal[700]
    }
  },
  grey: {
    '&$cssFocused': {
      color: grey[700]
    }
  },
  cssFocused: {},
  underlinePrimary: {
    '&.grey': {
      '&:before': {
        borderBottomColor: grey[500] + '!important'
      },
      '&:after': {
        borderBottomColor: grey[500]
      }
    },
    '&.teal': {
      '&:before': {
        borderBottomColor: teal[500] + '!important'
      },
      '&:after': {
        borderBottomColor: teal[500]
      }
    }
  },
  //
  Card: {
    // maxWidth: '740px',
    margin: '40px 0 0',
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      padding: '0'
    },
    '&.teal': {}
  },
  CardHeader: {
    position: 'relative',
    top: '-20px',
    padding: '5px 10px',
    margin: '0 20px',
    color: theme.palette.common.white,
    '&.teal': {
      background: `linear-gradient(60deg, ${teal[500]}, ${teal[600]})`
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5px'
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0 15px'
    }
  },
  gridCardHeader: {
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  typoCardHeader: {
    position: 'relative',
    color: theme.palette.common.white,
    textTransform: 'uppercase'
  },
  iconNewWrap: {
    position: 'absolute',
    top: '-3px',
    left: '-30px'
  },
  iconNew: {
    color: 'white'
  },
  Form: {
    padding: '0 20px 20px',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  textName: {
    // marginBottom: '20px'
  },
  inputName: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonDelete: {
    color: 'white',
    backgroundColor: red[600],
    '&:hover': { backgroundColor: red[700] }
  },
  btnPrimary: {
    marginLeft: '10px',
    color: theme.palette.common.white,
    '&.teal': {
      backgroundColor: teal[600],
      '&:hover': {
        backgroundColor: teal[700]
      }
    }
  },
  // PERSON BLOCK
  PaperPerson: {
    margin: '20px 0',
    borderRadius: 0,
    background: grey[50],
    '&.teal': {
      background: teal[50]
    }
  },
  typoPersonTitle: {
    color: theme.palette.common.white,
    padding: '5px 20px',
    textTransform: 'uppercase',
    background: grey[500],
    '&.teal': { background: teal[400] }
  },
  gridPersonWrap: { padding: '0 20px 20px' },
  inputPersonName: {
    fontSize: '30px',
    color: grey[600]
  }
})
