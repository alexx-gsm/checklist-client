import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getQuestionById,
  storeQuestion,
  saveQuestion,
  updateQuestion,
  uploadCriterias
} from '../../../redux/modules/question'
import { getAnswers } from '../../../redux/modules/answer'
import { clearUploadedCriterias } from '../../../redux/modules/criteria'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
import CardTableStyles from '../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import QuestionEdit from './QuestionEdit'

export default compose(
  connect(
    ({ questionStore, answerStore }) => {
      const { question, error, loading } = questionStore
      const { answers } = answerStore

      const uniqueQuestionId = !isEmpty(question.criterias)
        ? Array.from(
            new Set(
              Object.keys(question.criterias).map(
                key => question.criterias[key]['idQuestion']
              )
            )
          )
        : []

      const filteredCriterias =
        !isEmpty(question.criterias) && !isEmpty(question.criteriaQuestionId)
          ? Object.keys(question.criterias).reduce((acc, key) => {
              return question.criterias[key]['idQuestion'] ===
                question.criteriaQuestionId
                ? { ...acc, [key]: question.criterias[key] }
                : acc
            }, {})
          : question.criterias

      return {
        question,
        uniqueQuestionId,
        filteredCriterias,
        answers,
        error,
        loading
      }
    },
    {
      getQuestionById,
      storeQuestion,
      saveQuestion,
      updateQuestion,
      getAnswers,
      uploadCriterias
    }
  ),
  withRouter,
  withState('uploadedFile', 'setUploadedFile', null),
  withHandlers({
    onReload: ({ getQuestionById, match }) => () =>
      getQuestionById(match.params.id),
    onChange: ({ question, storeQuestion }) => index => event => {
      if (index === 'checkBox') {
        storeQuestion({
          ...question,
          [event.target.name]: event.target.checked
        })
      } else if (index === 'file') {
        storeQuestion({
          ...question,
          file: event.target.files[0]
        })
      } else {
        storeQuestion({
          ...question,
          [index]: event.target.value
        })
      }
    },
    onChangeItem: ({ question, storeQuestion }) => index => event => {
      storeQuestion({
        ...question,
        variants: [
          ...question.variants.slice(0, index),
          {
            ...question.variants[index],
            [event.target.name]: event.target.value
          },
          ...question.variants.slice(index + 1)
        ]
      })
    },
    onAddRow: ({ question, storeQuestion }) => () => {
      storeQuestion({
        ...question,
        variants: [
          ...question.variants.filter(item => !isEmpty(item.title)),
          { title: '', weight: 0 }
        ]
      })
    },
    onDeleteRow: ({ question, storeQuestion }) => index => () => {
      storeQuestion({
        ...question,
        variants: [
          ...question.variants.slice(0, index),
          ...question.variants.slice(index + 1)
        ]
      })
    },
    onSave: ({
      question,
      filteredCriterias,
      saveQuestion,
      updateQuestion,
      history,
      basePath
    }) => () => {
      if (question.id) {
        updateQuestion({ ...question, criterias: filteredCriterias }, () =>
          history.push(basePath)
        )
      } else {
        saveQuestion({ ...question, criterias: filteredCriterias }, () =>
          history.push(basePath)
        )
      }
    },
    onDelete: () => () => {},
    onUpload: ({ question, uploadedCriterias, uploadCriterias }) => () => {
      if (!isEmpty(uploadedCriterias)) {
        clearUploadedCriterias()
      } else {
        uploadCriterias(question)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getQuestionById(id)
      }
      this.props.getAnswers()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(QuestionEdit)
