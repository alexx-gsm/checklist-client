import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getResearchById,
  storeResearch,
  saveResearch,
  updateResearch
} from '../../../redux/modules/research'
import { getCompanies } from '../../../redux/modules/company'
import { getDepartments } from '../../../redux/modules/department'
import { getSurveys } from '../../../redux/modules/survey'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import ResearchEdit from './ResearchEdit'

export default compose(
  connect(
    ({ researchStore, companyStore, departmentStore, surveyStore }) => {
      const { research, error, loading } = researchStore
      const { companies } = companyStore
      const { departments } = departmentStore
      const { surveys } = surveyStore

      const filteredDepartments = isEmpty(research.companyId)
        ? departments
        : Object.keys(departments).reduce((acc, id) => {
            return departments[id].companyId !== research.companyId
              ? acc
              : {
                  ...acc,
                  [id]: departments[id]
                }
          }, {})
      return {
        research,
        companies,
        departments: filteredDepartments,
        surveys,
        error,
        loading
      }
    },
    {
      getResearchById,
      storeResearch,
      saveResearch,
      updateResearch,
      getCompanies,
      getDepartments,
      getSurveys
    }
  ),
  withRouter,
  withState('open', 'setOpen', false),
  withHandlers({
    onReload: ({ getResearchById, match }) => () =>
      getResearchById(match.params.id),
    onChange: ({ research, storeResearch }) => index => event => {
      if (index === 'range') {
        storeResearch({
          ...research,
          range: event
        })
      } else if (index === 'file') {
        console.log('file', event.target.files[0].name)
        storeResearch({
          ...research,
          [index]: event.target.files[0]
        })
      } else {
        storeResearch({
          ...research,
          [index]: event.target.value
        })
      }
    },

    onChangeItem: ({ research, storeResearch }) => index => event => {
      storeResearch({
        ...research,
        variants: [
          ...research.variants.slice(0, index),
          {
            ...research.variants[index],
            [event.target.name]: event.target.value
          },
          ...research.variants.slice(index + 1)
        ]
      })
    },
    onAddRow: ({ research, storeResearch }) => () => {
      storeResearch({
        ...research,
        variants: [
          ...research.variants.filter(item => !isEmpty(item.title)),
          { title: '', weight: 0 }
        ]
      })
    },
    onDeleteRow: ({ research, storeResearch }) => index => () => {
      storeResearch({
        ...research,
        variants: [
          ...research.variants.slice(0, index),
          ...research.variants.slice(index + 1)
        ]
      })
    },
    onSave: ({
      research,
      saveResearch,
      updateResearch,
      history,
      basePath
    }) => () => {
      if (research.id) {
        updateResearch(research, () => history.push(basePath))
      } else {
        saveResearch(research, () => history.push(basePath))
      }
    },
    onDelete: () => () => {},
    handleClickOpen: ({ setOpen }) => () => setOpen(true),
    handleClickClose: ({ setOpen }) => () => setOpen(false)
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getResearchById(id)
      }
      this.props.getCompanies()
      this.props.getDepartments()
      this.props.getSurveys()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(ResearchEdit)
