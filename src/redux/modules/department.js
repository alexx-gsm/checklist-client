import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'departmentStore'

// --- ACTIONS ---
export const DEPARTMENT_LOADING = `${appName}/${moduleName}/DEPARTMENT_LOADING`
export const DEPARTMENT_STORE = `${appName}/${moduleName}/DEPARTMENT_STORE`
export const DEPARTMENT_SAVE = `${appName}/${moduleName}/DEPARTMENT_SAVE`
export const DEPARTMENT_GET_ALL = `${appName}/${moduleName}/DEPARTMENT_GET_ALL`
export const DEPARTMENT_ERROR = `${appName}/${moduleName}/DEPARTMENT_ERROR`

const initialDepartment = {
  title: '',
  companyId: '',
  prefix: '',
  color: ''
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  departments: {},
  department: initialDepartment,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case DEPARTMENT_LOADING:
      return state.set('loading', true)
    case DEPARTMENT_STORE:
      return state
        .set('loading', false)
        .set('department', payload)
        .set('error', {})
    case DEPARTMENT_GET_ALL:
      return state
        .set('loading', false)
        .set('departments', payload)
        .set('error', {})
    case DEPARTMENT_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE DEPARTMENT
 */
export const storeDepartment = payload => ({
  type: DEPARTMENT_STORE,
  payload
})
/**
 * * SAVE DEPARTMENT
 */
export const saveDepartment = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/departments`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: DEPARTMENT_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE DEPARTMENT
 */
export const updateDepartment = (payload, cb) => dispatch => {
  axios
    .put(`${HOST}/api/departments`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: DEPARTMENT_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL DEPARTMENTS
 */
export const getDepartments = () => dispatch => {
  dispatch({
    type: DEPARTMENT_LOADING
  })
  axios
    .post(`${HOST}/api/departments/all`)
    .then(res => {
      dispatch({
        type: DEPARTMENT_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: DEPARTMENT_ERROR,
        error
      })
    })
}
/**
 * * GET DEPARTMENT BY ID
 */
export const getDepartmentById = id => dispatch => {
  dispatch({
    type: DEPARTMENT_LOADING
  })
  axios
    .post(`${HOST}/api/departments/${id}`)
    .then(res => {
      dispatch({
        type: DEPARTMENT_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: DEPARTMENT_ERROR,
        error
      })
    )
}

/**
 * * clear department
 */
export const clearDepartment = () => ({
  type: DEPARTMENT_STORE,
  payload: initialDepartment
})

export default reducer
