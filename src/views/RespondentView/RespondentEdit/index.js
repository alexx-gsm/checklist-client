import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getRespondentById,
  storeRespondent,
  saveRespondent,
  updateRespondent
} from '../../../redux/modules/respondent'
import { getCompanies } from '../../../redux/modules/company'
import { getDepartments } from '../../../redux/modules/department'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
import CardTableStyles from '../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import RespondentEdit from './RespondentEdit'

export default compose(
  connect(
    ({ respondentStore, companyStore, departmentStore }) => {
      const { respondent, error, loading } = respondentStore
      const { companies } = companyStore
      const { departments } = departmentStore

      const filteredDepartments = isEmpty(respondent.companyId)
        ? departments
        : Object.keys(departments).reduce((acc, id) => {
            return departments[id].companyId !== respondent.companyId
              ? acc
              : {
                  ...acc,
                  [id]: departments[id]
                }
          }, {})
      return {
        respondent,
        companies,
        departments: filteredDepartments,
        error,
        loading
      }
    },
    {
      getRespondentById,
      storeRespondent,
      saveRespondent,
      updateRespondent,
      getCompanies,
      getDepartments
    }
  ),
  withRouter,
  withHandlers({
    onReload: ({ getRespondentById, match }) => () =>
      getRespondentById(match.params.id),
    onChange: ({ respondent, storeRespondent }) => index => event => {
      if (index === 'checkBox') {
        storeRespondent({
          ...respondent,
          [event.target.name]: event.target.checked
        })
      } else {
        storeRespondent({
          ...respondent,
          [index]: event.target.value
        })
      }
    },
    onChangeItem: ({ respondent, storeRespondent }) => index => event => {
      storeRespondent({
        ...respondent,
        variants: [
          ...respondent.variants.slice(0, index),
          {
            ...respondent.variants[index],
            [event.target.name]: event.target.value
          },
          ...respondent.variants.slice(index + 1)
        ]
      })
    },
    onAddRow: ({ respondent, storeRespondent }) => () => {
      storeRespondent({
        ...respondent,
        variants: [
          ...respondent.variants.filter(item => !isEmpty(item.title)),
          { title: '', weight: 0 }
        ]
      })
    },
    onDeleteRow: ({ respondent, storeRespondent }) => index => () => {
      storeRespondent({
        ...respondent,
        variants: [
          ...respondent.variants.slice(0, index),
          ...respondent.variants.slice(index + 1)
        ]
      })
    },
    onSave: ({
      respondent,
      saveRespondent,
      updateRespondent,
      history,
      basePath
    }) => () => {
      if (respondent.id) {
        updateRespondent(respondent, () => history.push(basePath))
      } else {
        saveRespondent(respondent, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getRespondentById(id)
      }
      this.props.getCompanies()
      this.props.getDepartments()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(RespondentEdit)
