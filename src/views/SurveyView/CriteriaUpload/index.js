import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  uploadCriterias,
  clearUploadedCriterias
} from '../../../redux/modules/criteria'
import { getQuestions } from '../../../redux/modules/question'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import CriteriaUpload from './CriteriaUpload'

export default compose(
  connect(
    ({ criteriaStore, questionStore }) => {
      const { uploadedCriterias, loading } = criteriaStore
      const { questions } = questionStore
      return {
        uploadedCriterias,
        questions,
        loading: loading || questionStore.loading
      }
    },
    {
      uploadCriterias,
      clearUploadedCriterias,
      getQuestions
    }
  ),
  withState('criteriaId', 'setCriteriaId', null),
  withState('uploadedFile', 'setUploadedFile', null),
  withHandlers({
    onChange: ({ setCriteriaId, setUploadedFile }) => index => event => {
      switch (index) {
        case 'criteriaId':
          setCriteriaId(event.target.value)
          break
        case 'file':
          setUploadedFile(event.target.files[0])
          break
        default:
          break
      }
    },
    onUpload: ({
      criteriaId,
      setCriteriaId,
      uploadedFile,
      setUploadedFile,
      uploadedCriterias,
      uploadCriterias
    }) => () => {
      if (!isEmpty(uploadedCriterias)) {
        console.log('--- upload')
        setCriteriaId(null)
        setUploadedFile(null)
        clearUploadedCriterias()
      } else {
        uploadCriterias({ criteriaId, file: uploadedFile })
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearUploadedCriterias()
      this.props.getQuestions()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(CriteriaUpload)
