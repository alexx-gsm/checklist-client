import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'companyStore'

// --- ACTIONS ---
export const COMPANY_LOADING = `${appName}/${moduleName}/COMPANY_LOADING`
export const COMPANY_STORE = `${appName}/${moduleName}/COMPANY_STORE`
export const COMPANY_SAVE = `${appName}/${moduleName}/COMPANY_SAVE`
export const COMPANY_GET_ALL = `${appName}/${moduleName}/COMPANY_GET_ALL`
export const COMPANY_ERROR = `${appName}/${moduleName}/COMPANY_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  companies: {},
  company: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case COMPANY_LOADING:
      return state.set('loading', true)
    case COMPANY_STORE:
      return state
        .set('loading', false)
        .set('company', payload)
        .set('error', {})
    case COMPANY_GET_ALL:
      return state
        .set('loading', false)
        .set('companies', payload)
        .set('error', {})
    case COMPANY_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE COMPANY
 */
export const storeCompany = payload => ({
  type: COMPANY_STORE,
  payload
})
/**
 * * SAVE COMPANY
 */
export const saveCompany = (payload, cb) => dispatch => {
  console.log('AC --- SAVE COMPANY')
  axios
    .post(`${HOST}/api/companies`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: COMPANY_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE COMPANY
 */
export const updateCompany = (payload, cb) => dispatch => {
  console.log('AC --- PUT COMPANY')
  axios
    .put(`${HOST}/api/companies`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: COMPANY_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL COMPANIES
 */
export const getCompanies = () => dispatch => {
  dispatch({
    type: COMPANY_LOADING
  })
  axios
    .post(`${HOST}/api/companies/all`)
    .then(res => {
      console.log('AC --- GET ALL COMPANYS: ok')
      dispatch({
        type: COMPANY_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL COMPANYS: err')
      dispatch({
        type: COMPANY_ERROR,
        error: error
      })
    })
}
/**
 * * GET COMPANY BY ID
 */
export const getCompanyById = id => dispatch => {
  dispatch({
    type: COMPANY_LOADING
  })
  axios
    .post(`${HOST}/api/companies/${id}`)
    .then(res => {
      dispatch({
        type: COMPANY_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: COMPANY_ERROR,
        error
      })
    )
}

/**
 * * CLEAR COMPANY
 */
export const clearCompany = () => ({
  type: COMPANY_STORE,
  payload: {}
})

export default reducer
