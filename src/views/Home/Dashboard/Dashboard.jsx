import React from 'react'
import { NavLink } from 'react-router-dom'

const Dashboard = () => (
  <React.Fragment>
    <NavLink to='/admin'>Admin</NavLink>
  </React.Fragment>
)

export default Dashboard
