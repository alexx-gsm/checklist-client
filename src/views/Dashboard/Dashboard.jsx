import React from 'react'
import { NavLink } from 'react-router-dom'

const Dashboard = () => (
  <React.Fragment>
    <NavLink to='/home'>Home</NavLink>
  </React.Fragment>
)

export default Dashboard
