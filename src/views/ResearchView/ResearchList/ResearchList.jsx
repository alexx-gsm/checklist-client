import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'
// useful tools
import isEmpty from '../../../helpers/is-empty'

const ResearchList = ({
  researches,
  companies,
  departments,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  filter,
  onSearch,
  handleFilterClear,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        label={'Название'}
        loading={loading}
        onReload={onReload}
        search={{ filter, onSearch, handleFilterClear }}
      />
      <Paper className={classes.PaperTable}>
        <Grid container className={classes.gridTable}>
          <Table>
            <TableHead className={classes.TableHead}>
              <TableRow>
                <TableCell className={classes.headCellData} padding='checkbox'>
                  <Typography variant='caption'>Название</Typography>
                </TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(researches).map(key => {
                const research = researches[key]
                return (
                  <TableRow key={key} className={classes.TableRow}>
                    <TableCell
                      padding='checkbox'
                      className={classes.CellBordered}
                    >
                      <Grid container>
                        <Typography variant='title' className={classes.Title}>
                          {research.title}
                        </Typography>
                        <Grid item container direction='row' spacing={16}>
                          <Grid item>
                            <Typography
                              variant='caption'
                              className={classes.typoInn}
                            >
                              {!isEmpty(companies)
                                ? companies[research.companyId].title
                                : ''}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography variant='caption'>
                              {!isEmpty(departments)
                                ? departments[research.departmentId].title
                                : ''}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell padding='none'>
                      <DropdownMenu
                        anchorEl={anchorEl}
                        updateAnchorEl={updateAnchorEl}
                        menuId={key}
                        menuList={[
                          {
                            icon: <IconEdit />,
                            label: 'Edit',
                            handler: editHandler(key)
                          },
                          {
                            icon: <IconDelete />,
                            label: 'Delete',
                            handler: deleteHandler(key)
                          }
                        ]}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

ResearchList.defaultProps = {
  companies: {},
  departments: {}
}

export default ResearchList
