import { createMuiTheme } from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#004d40'
    },
    secondary: {
      main: '#f57f17',
      contrastText: '#fff'
    },
    extraCardColor: green
  }
})
