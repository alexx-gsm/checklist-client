import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import { List, ListItem, ListItemText } from '@material-ui/core'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const RespondentUpload = ({
  companies,
  departments,
  uploadedRespondents,
  companyId,
  departmentId,
  uploadedFile,
  onChange,
  onUpload,
  classes,
  theme,
  basePath
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Paper className={classNames(classes.Card, classes.ResearchEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              Upload
            </Typography>
          </Grid>
        </Paper>
        {isEmpty(uploadedRespondents) ? (
          // Form
          <form className={classes.Form} noValidate autoComplete='off'>
            {/* COMPANY */}
            <TextField
              select
              fullWidth
              id='company'
              label='Company'
              value={isEmpty(companyId) ? '' : companyId}
              onChange={onChange('companyId')}
              margin='normal'
              className={classes.wrapTitle}
              InputProps={{
                className: classes.Title
              }}
              // error={Boolean(error.role)}
              // helperText={error.role ? error.role : null}
            >
              {Object.keys(companies).map(key => {
                const company = companies[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {company.title}
                      </Typography>
                      <Typography
                        className={classes.typoSelectInputSubtitle}
                        variant='caption'
                        style={{ marginLeft: '5px' }}
                      >
                        ( ИНН: {company.inn} )
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>

            {/* DEPARTMENT */}
            <TextField
              select
              fullWidth
              id='department'
              label='Department'
              value={isEmpty(departmentId) ? '' : departmentId}
              onChange={onChange('departmentId')}
              margin='normal'
              className={classes.wrapTitle}
              InputProps={{
                className: classes.Title
              }}
              // error={Boolean(error.role)}
              // helperText={error.role ? error.role : null}
            >
              {Object.keys(departments).map(key => {
                const item = departments[key]
                return (
                  <MenuItem key={key} value={key}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>
            {/* UPLOAD */}
            <Paper className={classes.UploadPaper}>
              <Grid container alignItems='center' spacing={16}>
                <Grid item>
                  <input
                    accept='file/*'
                    name='file'
                    className={classes.Upload}
                    id='upload'
                    type='file'
                    onChange={onChange('file')}
                  />
                  <label htmlFor='upload'>
                    <Button
                      variant='contained'
                      color='secondary'
                      component='span'
                      className={classes.UploadButton}
                    >
                      Upload
                    </Button>
                  </label>
                </Grid>
                <Grid item>
                  <Typography variant='caption'>
                    {uploadedFile ? uploadedFile.name : '-'}
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </form>
        ) : (
          <List dense className={classes.UploadList}>
            {Object.keys(uploadedRespondents).map(key => (
              <ListItem key={key}>
                <ListItemText
                  primary={`${uploadedRespondents[key].code}: ${
                    uploadedRespondents[key].fio
                  }`}
                />
              </ListItem>
            ))}
          </List>
        )}

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item />
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onUpload}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              {!isEmpty(uploadedRespondents) && 'New '}
              Upload
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

RespondentUpload.defaultProps = {
  companies: {},
  departments: {}
}

export default RespondentUpload
