import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import ColorPicker from '../../../components/ColorPicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const DepartmentEdit = ({
  department,
  companies,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onReload,
  theme,
  basePath,
  isColorPicker,
  setIsColorPicker
}) => {
  const { id, title, companyId, prefix, color } = department
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
      />
      <Paper className={classNames(classes.Card, classes.DepartmentEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Department
            </Typography>
          </Grid>
        </Paper>
        {/* Form */}
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            InputProps={{
              className: classes.Input40px
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          <TextField
            select
            fullWidth
            id='company'
            label='Company'
            value={isEmpty(companyId) ? '' : companyId}
            onChange={onChange('companyId')}
            margin='normal'
            className={classes.wrapTitle}
            InputProps={{
              className: classes.Title
            }}
            error={Boolean(error.role)}
            helperText={error.role ? error.role : null}
          >
            {Object.keys(companies).map(key => {
              const company = companies[key]
              return (
                <MenuItem key={key} value={key}>
                  <Grid
                    container
                    alignItems='baseline'
                    className={classes.gridType}
                    direction='row'
                    wrap='wrap'
                  >
                    <Typography
                      className={classes.typoSelectInputTitle}
                      variant='h6'
                    >
                      {company.title}
                    </Typography>
                    <Typography
                      className={classes.typoSelectInputSubtitle}
                      variant='caption'
                      style={{ marginLeft: '5px' }}
                    >
                      ( ИНН: {company.inn} )
                    </Typography>
                  </Grid>
                </MenuItem>
              )
            })}
          </TextField>
          <Grid container justify='center'>
            <Grid item xs={8}>
              <Paper className={classes.PaperInnerBlock}>
                <TextField
                  fullWidth
                  id='prefix'
                  label='Prefix'
                  value={isEmpty(prefix) ? '' : prefix}
                  onChange={onChange('prefix')}
                  margin='normal'
                  error={Boolean(error.prefix)}
                  helperText={error.prefix ? error.prefix : null}
                />
                <Button
                  variant='contained'
                  fullWidth
                  color='primary'
                  className={classes.ButtonColorPicker}
                  style={{ background: color || theme.palette.primary.dark }}
                  onClick={() => setIsColorPicker(true)}
                >
                  {color ? color : 'Select'}
                </Button>
                <ColorPicker
                  open={isColorPicker}
                  color={color || theme.palette.primary.dark}
                  onChange={onChange}
                  onSubmit={() => setIsColorPicker(false)}
                />
              </Paper>
            </Grid>
          </Grid>
          <Grid container direction='column' spacing={16}>
            <Grid item xs={12} sm={6} />
          </Grid>
        </form>

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.CardButtonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

DepartmentEdit.defaultProps = {
  department: {},
  companies: {}
}

export default DepartmentEdit
