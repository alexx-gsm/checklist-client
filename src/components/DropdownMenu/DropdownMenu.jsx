import React from 'react'
import PropTypes from 'prop-types'
// @material-ui components
import { Button } from '@material-ui/core'
import { Menu, MenuItem } from '@material-ui/core'
import { ListItemIcon, ListItemText } from '@material-ui/core'
// @material-ui icons
import MoreVert from '@material-ui/icons/MoreVert'
// useful tools
import isEmpty from '../../helpers/is-empty'

const DropdownMenu = ({
  menuId,
  menuList,
  anchorEl,
  updateAnchorEl,
  handleClose,
  classes
}) => {
  return (
    <React.Fragment>
      <Button
        id={menuId}
        aria-owns={anchorEl ? `menu-${menuId}` : undefined}
        aria-haspopup='true'
        size='small'
        onClick={event => {
          updateAnchorEl(event.currentTarget)
        }}
        classes={{
          root: classes.buttonMore,
          label: classes.buttonMoreLabel
        }}
      >
        <MoreVert />
      </Button>
      <Menu
        id={`menu-${menuId}`}
        anchorEl={anchorEl}
        open={!isEmpty(anchorEl) && anchorEl.id === menuId}
        onClose={handleClose}
      >
        {menuList.map(menuItem => (
          <MenuItem onClick={menuItem.handler} key={menuItem.label}>
            <ListItemIcon className={classes.icon}>
              {menuItem.icon}
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              inset
              primary={menuItem.label}
            />
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  )
}

DropdownMenu.propTypes = {
  menuId: PropTypes.string.isRequired,
  menuList: PropTypes.arrayOf(PropTypes.object).isRequired,
  // anchorEl: PropTypes.node.isRequired,
  updateAnchorEl: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
}

export default DropdownMenu
