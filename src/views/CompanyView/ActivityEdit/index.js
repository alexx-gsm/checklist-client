import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
import {
  getActivityById,
  storeActivity,
  saveActivity,
  updateActivity
} from '../../../redux/modules/activity'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import ActivityEdit from './ActivityEdit'

export default compose(
  connect(
    ({ activityStore }) => {
      const { activity, error, loading } = activityStore
      return {
        activity,
        error,
        loading
      }
    },
    {
      getActivityById,
      storeActivity,
      saveActivity,
      updateActivity
    }
  ),
  withState('isColorPicker', 'setIsColorPicker', false),
  withRouter,
  withHandlers({
    onReload: ({ getActivityById, match }) => () =>
      getActivityById(match.params.id),
    onChange: ({ activity, storeActivity }) => index => event => {
      if (index === 'color') {
        storeActivity({
          ...activity,
          [index]: event.hex
        })
      } else {
        storeActivity({
          ...activity,
          [index]: event.target.value
        })
      }
    },
    onSave: ({
      activity,
      saveActivity,
      updateActivity,
      history,
      basePath
    }) => () => {
      if (activity.id) {
        updateActivity(activity, () => history.push(basePath))
      } else {
        saveActivity(activity, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getActivityById(id)
      }
    }
  }),
  withStyles(theme => ({ ...CardStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(ActivityEdit)
