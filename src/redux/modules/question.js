import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'questionStore'

// --- ACTIONS ---
export const QUESTION_LOADING = `${appName}/${moduleName}/QUESTION_LOADING`
export const QUESTION_STORE = `${appName}/${moduleName}/QUESTION_STORE`
export const QUESTION_SAVE = `${appName}/${moduleName}/QUESTION_SAVE`
export const QUESTION_GET_ALL = `${appName}/${moduleName}/QUESTION_GET_ALL`
export const QUESTION_ERROR = `${appName}/${moduleName}/QUESTION_ERROR`

const initialQuestion = {
  title: '',
  target: 'common',
  answerId: ''
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  questions: {},
  question: initialQuestion,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case QUESTION_LOADING:
      return state.set('loading', true)
    case QUESTION_STORE:
      return state
        .set('loading', false)
        .set('question', payload)
        .set('error', {})
    case QUESTION_GET_ALL:
      return state
        .set('loading', false)
        .set('questions', payload)
        .set('error', {})
    case QUESTION_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE QUESTION
 */
export const storeQuestion = payload => ({
  type: QUESTION_STORE,
  payload
})
/**
 * * SAVE QUESTION
 */
export const saveQuestion = (payload, cb) => dispatch => {
  console.log('AC --- SAVE QUESTION')
  axios
    .post(`${HOST}/api/questions`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: QUESTION_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE QUESTION
 */
export const updateQuestion = (payload, cb) => dispatch => {
  console.log('AC --- PUT QUESTION')
  axios
    .put(`${HOST}/api/questions`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: QUESTION_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL QUESTIONS
 */
export const getQuestions = () => dispatch => {
  dispatch({
    type: QUESTION_LOADING
  })
  axios
    .post(`${HOST}/api/questions/all`)
    .then(res => {
      console.log('AC --- GET ALL QUESTIONS: ok')
      dispatch({
        type: QUESTION_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL QUESTIONS: err')
      dispatch({
        type: QUESTION_ERROR,
        error
      })
    })
}

/**
 * * GET QUESTION BY ID
 */
export const getQuestionById = id => dispatch => {
  dispatch({
    type: QUESTION_LOADING
  })
  axios
    .post(`${HOST}/api/questions/${id}`)
    .then(res => {
      dispatch({
        type: QUESTION_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: QUESTION_ERROR,
        error
      })
    )
}

/**
 * * UPLOAD CRITERIAS
 */
export const uploadCriterias = payload => dispatch => {
  if (payload.file) {
    const formData = new FormData()
    Object.keys(payload).map(key => formData.append(key, payload[key]))
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    dispatch({
      type: QUESTION_LOADING
    })
    axios
      .post(`${HOST}/api/questions/upload/criterias`, formData, config)
      .then(res =>
        dispatch({
          type: QUESTION_STORE,
          payload: res.data
        })
      )
      .catch(error => {
        dispatch({
          type: QUESTION_ERROR,
          error
        })
      })
  }
}

/**
 * * CLEAR QUESTION
 */
export const clearQuestion = () => ({
  type: QUESTION_STORE,
  payload: initialQuestion
})

export default reducer
