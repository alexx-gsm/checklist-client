import React from 'react'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import { List, ListItem, ListItemText } from '@material-ui/core'

const Appbar = ({ isOpen, toggleDrawer }) => {
  return (
    <Drawer anchor='right' open={isOpen} onClose={toggleDrawer(false)}>
      <div
        tabIndex={0}
        role='button'
        onClick={toggleDrawer(false)}
        onKeyDown={toggleDrawer(false)}
      >
        <h2>Side</h2>
      </div>
    </Drawer>
  )
}

export default Appbar
