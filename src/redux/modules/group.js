import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'groupStore'

// --- ACTIONS ---
export const GROUP_LOADING = `${appName}/${moduleName}/GROUP_LOADING`
export const GROUP_STORE = `${appName}/${moduleName}/GROUP_STORE`
export const GROUP_SAVE = `${appName}/${moduleName}/GROUP_SAVE`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  groups: [],
  group: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case GROUP_LOADING:
      return state.set('loading', true)
    case GROUP_STORE:
      return state
        .set('loading', false)
        .set('group', payload)
        .set('error', {})
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE GROUP
 */
export const storeGroup = payload => ({
  type: GROUP_STORE,
  payload
})
/**
 * * SAVE GROUP
 */
export const saveGroup = payload => dispatch => {
  axios.post(`${HOST}/api/groups/`)
}

export default reducer
