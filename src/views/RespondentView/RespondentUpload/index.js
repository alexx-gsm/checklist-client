import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  uploadRespondents,
  clearUploadedRespondents
} from '../../../redux/modules/respondent'
import { getCompanies } from '../../../redux/modules/company'
import { getDepartments } from '../../../redux/modules/department'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'

import RespondentUpload from './RespondentUpload'
import isEmpty from '../../../helpers/is-empty'

export default compose(
  connect(
    ({ respondentStore, companyStore, departmentStore }) => {
      const { uploadedRespondents } = respondentStore
      const { companies } = companyStore
      const { departments } = departmentStore
      return {
        uploadedRespondents,
        companies,
        departments,
        loading: companyStore.loading || departmentStore.loading
      }
    },
    {
      uploadRespondents,
      clearUploadedRespondents,
      getCompanies,
      getDepartments
    }
  ),
  withState('companyId', 'setCompanyId', null),
  withState('departmentId', 'setDepartmentId', null),
  withState('uploadedFile', 'setUploadedFile', null),
  withHandlers({
    onChange: ({
      setCompanyId,
      setDepartmentId,
      setUploadedFile
    }) => index => event => {
      switch (index) {
        case 'companyId':
          setCompanyId(event.target.value)
          break
        case 'departmentId':
          setDepartmentId(event.target.value)
          break
        case 'file':
          setUploadedFile(event.target.files[0])
          break
        default:
          break
      }
    },
    onUpload: ({
      companyId,
      departmentId,
      uploadedFile,
      uploadedRespondents,
      setCompanyId,
      setDepartmentId,
      setUploadedFile,
      uploadRespondents
    }) => () => {
      if (!isEmpty(uploadedRespondents)) {
        console.log('--- upload')
        setCompanyId(null)
        setDepartmentId(null)
        setUploadedFile(null)
        clearUploadedRespondents()
      } else {
        uploadRespondents({ companyId, departmentId, file: uploadedFile })
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearUploadedRespondents()
      this.props.getCompanies()
      this.props.getDepartments()
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(RespondentUpload)
