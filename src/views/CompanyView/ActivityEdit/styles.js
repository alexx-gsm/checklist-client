export default theme => ({
  ActivityEdit: {},
  Form: {
    padding: '20px'
  },
  PaperInnerBlock: {
    margin: '20px',
    padding: '0 20px 20px'
  },
  ButtonColorPicker: {
    marginTop: '15px'
  }
})
