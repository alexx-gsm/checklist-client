import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'activityStore'

// --- ACTIONS ---
export const ACTIVITY_LOADING = `${appName}/${moduleName}/ACTIVITY_LOADING`
export const ACTIVITY_STORE = `${appName}/${moduleName}/ACTIVITY_STORE`
export const ACTIVITY_SAVE = `${appName}/${moduleName}/ACTIVITY_SAVE`
export const ACTIVITY_GET_ALL = `${appName}/${moduleName}/ACTIVITY_GET_ALL`
export const ACTIVITY_ERROR = `${appName}/${moduleName}/ACTIVITY_ERROR`

const initialActivity = {
  title: '',
  prefix: '',
  color: ''
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  activities: {},
  activity: initialActivity,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case ACTIVITY_LOADING:
      return state.set('loading', true)
    case ACTIVITY_STORE:
      return state
        .set('loading', false)
        .set('activity', payload)
        .set('error', {})
    case ACTIVITY_GET_ALL:
      return state
        .set('loading', false)
        .set('activities', payload)
        .set('error', {})
    case ACTIVITY_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE ACTIVITY
 */
export const storeActivity = payload => ({
  type: ACTIVITY_STORE,
  payload
})
/**
 * * SAVE ACTIVITY
 */
export const saveActivity = (payload, cb) => dispatch => {
  console.log('AC --- SAVE ACTIVITY')
  axios
    .post(`${HOST}/api/activities`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ACTIVITY_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE ACTIVITY
 */
export const updateActivity = (payload, cb) => dispatch => {
  console.log('AC --- PUT ACTIVITY')
  axios
    .put(`${HOST}/api/activities`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ACTIVITY_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL ACTIVITIES
 */
export const getActivitys = () => dispatch => {
  dispatch({
    type: ACTIVITY_LOADING
  })
  axios
    .post(`${HOST}/api/activities/all`)
    .then(res => {
      console.log('AC --- GET ALL ACTIVITIES: ok')
      dispatch({
        type: ACTIVITY_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL ACTIVITIES: err')
      dispatch({
        type: ACTIVITY_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ACTIVITY BY ID
 */
export const getActivityById = id => dispatch => {
  dispatch({
    type: ACTIVITY_LOADING
  })
  axios
    .post(`${HOST}/api/activities/${id}`)
    .then(res => {
      dispatch({
        type: ACTIVITY_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: ACTIVITY_ERROR,
        error
      })
    )
}

/**
 * * clear activity
 */
export const clearActivity = () => ({
  type: ACTIVITY_STORE,
  payload: initialActivity
})

export default reducer
