import { compose, withState } from 'recompose'

import DayPicker from './DayPicker'

export default compose(
  withState('range', 'setRange', { from: undefined, to: undefined }),
  withState('openDialog', 'setOpenDialog', true)
)(DayPicker)
