import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'answerStore'

// --- ACTIONS ---
export const ANSWER_LOADING = `${appName}/${moduleName}/ANSWER_LOADING`
export const ANSWER_STORE = `${appName}/${moduleName}/ANSWER_STORE`
export const ANSWER_SAVE = `${appName}/${moduleName}/ANSWER_SAVE`
export const ANSWER_GET_ALL = `${appName}/${moduleName}/ANSWER_GET_ALL`
export const ANSWER_ERROR = `${appName}/${moduleName}/ANSWER_ERROR`

const initialAnswer = {
  title: '',
  isMembersInQuestion: false,
  isMembersInAnswer: false,
  whatMembersInQuestion: 'department',
  whatMembersInAnswer: 'department',
  variants: [
    {
      title: '',
      weight: 0
    }
  ],
  withWeight: false
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  answers: {},
  answer: initialAnswer,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case ANSWER_LOADING:
      return state.set('loading', true)
    case ANSWER_STORE:
      return state
        .set('loading', false)
        .set('answer', payload)
        .set('error', {})
    case ANSWER_GET_ALL:
      return state
        .set('loading', false)
        .set('answers', payload)
        .set('error', {})
    case ANSWER_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**
 * * STORE ANSWER
 */
export const storeAnswer = payload => ({
  type: ANSWER_STORE,
  payload
})
/**
 * * SAVE ANSWER
 */
export const saveAnswer = (payload, cb) => dispatch => {
  console.log('AC --- SAVE ANSWER')
  axios
    .post(`${HOST}/api/answers`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ANSWER_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * UPDATE ANSWER
 */
export const updateAnswer = (payload, cb) => dispatch => {
  console.log('AC --- PUT ANSWER')
  axios
    .put(`${HOST}/api/answers`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: ANSWER_ERROR,
        error: error.response.data
      })
    })
}
/**
 * * GET ALL ANSWERS
 */
export const getAnswers = () => dispatch => {
  dispatch({
    type: ANSWER_LOADING
  })
  axios
    .post(`${HOST}/api/answers/all`)
    .then(res => {
      console.log('AC --- GET ALL ANSWERS: ok')
      dispatch({
        type: ANSWER_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL ANSWERS: err')
      dispatch({
        type: ANSWER_ERROR,
        error
      })
    })
}
/**
 * * GET ANSWER BY ID
 */
export const getAnswerById = id => dispatch => {
  dispatch({
    type: ANSWER_LOADING
  })
  axios
    .post(`${HOST}/api/answers/${id}`)
    .then(res => {
      dispatch({
        type: ANSWER_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: ANSWER_ERROR,
        error
      })
    )
}

/**
 * * clear answer
 */
export const clearAnswer = () => ({
  type: ANSWER_STORE,
  payload: initialAnswer
})

export default reducer
