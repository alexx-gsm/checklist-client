import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'

const COLOR = 'orange'

const QuestionTypeList = ({
  qtypes,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  classes
}) => {
  return (
    <React.Fragment>
      <ToolPanel
        title=''
        color={COLOR}
        link={basePath}
        loading={loading}
        onReload={onReload}
      />
      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              Question Types
            </Typography>
          </Grid>
        </Paper>
        <Paper className={classes.Table}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.TableHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding='checkbox'
                  >
                    <Typography variant='caption'>Название</Typography>
                  </TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(qtypes).map(key => {
                  const qtype = qtypes[key]
                  return (
                    <TableRow key={key} className={classes.TableRow}>
                      <TableCell padding='checkbox' style={{ width: '100%' }}>
                        <Typography variant='title'>{qtype.title}</Typography>
                      </TableCell>
                      <TableCell padding='none'>
                        <DropdownMenu
                          anchorEl={anchorEl}
                          updateAnchorEl={updateAnchorEl}
                          menuId={key}
                          menuList={[
                            {
                              icon: <IconEdit />,
                              label: 'Edit',
                              handler: editHandler(key)
                            },
                            {
                              icon: <IconDelete />,
                              label: 'Delete',
                              handler: deleteHandler(key)
                            }
                          ]}
                        />
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </React.Fragment>
  )
}

export default QuestionTypeList
