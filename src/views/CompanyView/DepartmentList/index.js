import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import {
  getDepartments,
  clearDepartment
} from '../../../redux/modules/department'
import { getCompanies } from '../../../redux/modules/company'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

import DepartmentList from './DepartmentList'

const FILTER_KEY = 'DEPARTMENT'

export default compose(
  connect(
    ({ departmentStore, companyStore, filterStore }) => {
      const { departments, loading, error } = departmentStore
      const { companies } = companyStore
      const { filter } = filterStore
      const filteredDepartments =
        filter[FILTER_KEY] === ''
          ? departments
          : Object.keys(departments).reduce((acc, key) => {
              const item = departments[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.title.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        departments: filteredDepartments,
        companies,
        loading,
        error
      }
    },
    { getDepartments, clearDepartment, getCompanies }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getDepartments }) => () => getDepartments(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearDepartment()
      this.props.getDepartments()
      this.props.getCompanies()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(DepartmentList)
