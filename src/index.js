import React from 'react'
import ReactDOM from 'react-dom'
import store from './redux/store'
import { Provider } from 'react-redux'
// components
import Routes from './routes'
//
import * as serviceWorker from './serviceWorker'
// styles
import './styles.css'
// useful tools
import { checkAuthToken } from './helpers/authHelpers'
// only for authorized users
checkAuthToken()

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
