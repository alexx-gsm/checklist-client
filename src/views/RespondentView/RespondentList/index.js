import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
// HOCs
import withFilter from '../../../hocs/withFilter'
// AC
import {
  getRespondents,
  clearRespondent
} from '../../../redux/modules/respondent'
import { getCompanies } from '../../../redux/modules/company'
import { getDepartments } from '../../../redux/modules/department'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'
import ListTableStyles from '../../../styles/ListTableStyles'

import RespondentList from './RespondentList'

const FILTER_KEY = 'RESPONDENT'

export default compose(
  connect(
    ({ respondentStore, filterStore, companyStore, departmentStore }) => {
      const { respondents, loading, error } = respondentStore
      const { companies } = companyStore
      const { departments } = departmentStore
      const { filter } = filterStore
      const filteredRespondents =
        filter[FILTER_KEY] === ''
          ? respondents
          : Object.keys(respondents).reduce((acc, key) => {
              const item = respondents[key]
              const regexp = new RegExp(filter[FILTER_KEY], 'i')
              return item.fio.search(regexp) !== -1
                ? {
                    ...acc,
                    [key]: item
                  }
                : acc
            }, {})
      return {
        respondents: filteredRespondents,
        companies,
        departments,
        filter,
        loading: loading || companyStore.loading || departmentStore.loading,
        error
      }
    },
    { getRespondents, clearRespondent, getCompanies, getDepartments }
  ),
  withRouter,
  withFilter(FILTER_KEY),
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getRespondents }) => () => getRespondents(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearRespondent()
      this.props.getRespondents()
      this.props.getCompanies()
      this.props.getDepartments()
    }
  }),
  withStyles(
    theme => ({
      ...ListTableStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(RespondentList)
