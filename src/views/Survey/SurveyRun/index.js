import React from 'react'

import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getSurveyById,
  storeSurvey,
  saveSurvey,
  storeAndUpdateSurvey,
  updateSurvey,
  setSurveyStage,
  storeStageResult,
  storePrevResult
} from '../../../redux/modules/survey'
import { getQuestions } from '../../../redux/modules/question'
import { getAnswers } from '../../../redux/modules/answer'
import {
  getRespondents,
  getRespondentById,
  clearRespondent
} from '../../../redux/modules/respondent'
// Drag and Drop Helpers
import arrayMove from 'array-move'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import СardStyles from '../../../styles/CardStyles'
import CommonStyles from '../../../styles/CommonStyles'
import CardTableStyles from '../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import SurveyRun from './SurveyRun'

export default compose(
  connect(
    ({ surveyStore, questionStore, answerStore, respondentStore }) => {
      const { survey, stage, result, error, loading } = surveyStore
      const { questions } = questionStore
      const { answers } = answerStore
      const { respondent } = respondentStore

      const question =
        stage > -1 && questions && survey.questions
          ? questions[survey.questions[stage]]
          : {}

      const answer =
        !isEmpty(question) && answers && question.answerId
          ? answers[question.answerId]
          : {}

      return {
        survey,
        stage,
        result,
        question,
        answer,
        surveyQuestions: survey.questions ? survey.questions : {},
        questionList: questions,
        answers,
        respondent,
        error: { ...error, ...respondentStore.error },
        loading: loading || questionStore.loading || respondentStore.loading
      }
    },
    {
      getSurveyById,
      storeSurvey,
      saveSurvey,
      storeAndUpdateSurvey,
      updateSurvey,
      setSurveyStage,
      storeStageResult,
      storePrevResult,
      getQuestions,
      getAnswers,
      getRespondents,
      getRespondentById,
      clearRespondent
    }
  ),
  withRouter,
  withState('showSelect', 'setShowSelect', false),
  withHandlers({
    onReload: ({ getSurveyById, match }) => () =>
      getSurveyById(match.params.id),
    onChange: ({ survey, storeSurvey }) => index => event => {
      if (index === 'checkBox') {
        storeSurvey({
          ...survey,
          [event.target.name]: event.target.checked
        })
      } else {
        storeSurvey({
          ...survey,
          [index]: event.target.value
        })
      }
    },
    onFind: ({ survey, getRespondentById }) => e => {
      e.preventDefault()
      getRespondentById(survey.respondentId)
    },
    onClick: ({ getRespondentById }) => id => {
      getRespondentById(id)
    },
    handleStepChange: () => step => console.log('step', step),
    handleNext: ({
      survey,
      stage,
      result,
      answer,
      surveyQuestions,
      setSurveyStage,
      storeStageResult,
      storePrevResult,
      storeAndUpdateSurvey,
      respondent
    }) => () => {
      if (answer && answer.isMembersInQuestion) {
        storePrevResult(result)
      } else {
        storePrevResult([])
      }

      if (stage < surveyQuestions.length - 1) {
        const { results = {} } = survey
        const id = respondent.code

        storeAndUpdateSurvey({
          ...survey,
          results: {
            ...results,
            [id]: {
              ...results[id],
              [stage]: result
            }
          }
        })

        const nextResult =
          survey &&
          survey.results &&
          survey.results[respondent.code] &&
          survey.results[respondent.code][stage + 1]
            ? survey.results[respondent.code][stage + 1]
            : []

        storeStageResult(nextResult)
        setSurveyStage(stage + 1)
      }
    },
    handleBack: ({
      stage,
      setSurveyStage,
      survey,
      result,
      respondent,
      answers,
      questionList,
      storeStageResult,
      storePrevResult,
      storeAndUpdateSurvey
    }) => () => {
      if (stage > 0) {
        const { results = {} } = survey
        const id = respondent.code

        storeAndUpdateSurvey({
          ...survey,
          results: {
            ...results,
            [id]: {
              ...results[id],
              [stage]: result
            }
          }
        })

        const prevResult =
          survey &&
          survey.results &&
          survey.results[respondent.code] &&
          survey.results[respondent.code][stage - 1]
            ? survey.results[respondent.code][stage - 1]
            : []

        storeStageResult(prevResult)
        setSurveyStage(stage - 1)

        if (stage - 2 >= 0) {
          const prevQuestionId = survey ? survey.questions[stage - 1] : null
          const prevAnswer = prevQuestionId
            ? answers[questionList[prevQuestionId].answerId]
            : null

          const prevPrevQuestionId = survey ? survey.questions[stage - 2] : null
          const prevPrevAnswer = prevPrevQuestionId
            ? answers[questionList[prevPrevQuestionId].answerId]
            : null

          if (
            prevAnswer &&
            prevAnswer.isMembersInQuestion &&
            prevPrevAnswer &&
            prevPrevAnswer.isMembersInQuestion
          ) {
            const prevPrevResult = survey.results[respondent.code][stage - 2]
            storePrevResult(prevPrevResult)
          } else {
            storePrevResult([])
          }
        }
      }
    },
    handleFinish: ({
      survey,
      result,
      respondent,
      stage,
      storeAndUpdateSurvey,
      history
    }) => () => {
      const { results = {} } = survey
      const id = respondent.code

      storeAndUpdateSurvey({
        ...survey,
        results: {
          ...results,
          [id]: {
            ...results[id],
            [stage]: result
          }
        }
      })

      history.push('/home/surveys')
    },
    onSave: ({ survey, saveSurvey, updateSurvey, history, basePath }) => () => {
      if (survey.id) {
        updateSurvey(survey, () => history.push(basePath))
      } else {
        saveSurvey(survey, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getSurveyById(id)
      }
      this.props.getQuestions()
      this.props.getAnswers()
      this.props.getRespondents()
      this.props.clearRespondent()
      this.props.setSurveyStage(-1) // -1 !!!
    },
    componentDidUpdate(prevProps) {
      if (
        (isEmpty(prevProps.respondent) || isEmpty(prevProps.survey)) &&
        !isEmpty(this.props.respondent) &&
        !isEmpty(this.props.survey)
      ) {
        const { respondent, survey } = this.props

        const initialStage = 0
        const result =
          survey &&
          survey.results &&
          survey.results[respondent.code] &&
          survey.results[respondent.code][initialStage]
            ? survey.results[respondent.code][initialStage]
            : []

        this.props.storeStageResult(result)
        this.props.setSurveyStage(initialStage)
      }
    }
  }),
  withStyles(
    theme => {
      return {
        ...СardStyles(theme),
        ...CommonStyles(theme),
        ...styles(theme)
      }
    },
    {
      theme: true
    }
  )
)(SurveyRun)
