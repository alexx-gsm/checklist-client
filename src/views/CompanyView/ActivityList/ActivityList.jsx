import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import DropdownMenu from '../../../components/DropdownMenu'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import IconEdit from '@material-ui/icons/Edit'
import IconDelete from '@material-ui/icons/Delete'
// react icon kit
import { Icon } from 'react-icons-kit'
import { circle } from 'react-icons-kit/fa/circle'

const ActivityList = ({
  activities,
  anchorEl,
  updateAnchorEl,
  loading,
  basePath,
  editHandler,
  deleteHandler,
  onReload,
  filter,
  onSearch,
  handleFilterClear,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        label={'Название'}
        link={basePath}
        loading={loading}
        onReload={onReload}
        search={{ filter, onSearch, handleFilterClear }}
      />
      <Paper className={classes.PaperTable}>
        <Grid container className={classes.gridTable}>
          <Table>
            <TableHead className={classes.TableHead}>
              <TableRow>
                <TableCell className={classes.headCellData} padding='checkbox'>
                  <Typography variant='caption'>Название</Typography>
                </TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(activities).map(key => {
                const activity = activities[key]
                return (
                  <TableRow key={key} className={classes.TableRow}>
                    <TableCell
                      padding='checkbox'
                      className={classes.CellBordered}
                    >
                      <Grid
                        container
                        direction='row'
                        alignItems='center'
                        wrap='nowrap'
                      >
                        <Grid item>
                          <div
                            style={{
                              width: 24,
                              height: 24,
                              margin: '0 10px 5px 0',
                              color: activity.color
                            }}
                          >
                            <Icon size={'100%'} icon={circle} />
                          </div>
                          <Typography variant='caption'>
                            {activity.prefix}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant='h6' className={classes.Title}>
                            {activity.title}
                          </Typography>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell padding='none'>
                      <DropdownMenu
                        anchorEl={anchorEl}
                        updateAnchorEl={updateAnchorEl}
                        menuId={key}
                        menuList={[
                          {
                            icon: <IconEdit />,
                            label: 'Edit',
                            handler: editHandler(key)
                          },
                          {
                            icon: <IconDelete />,
                            label: 'Delete',
                            handler: deleteHandler(key)
                          }
                        ]}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

ActivityList.defaultProps = {
  activities: {},
  companies: {}
}

export default ActivityList
