import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../../../components/ToolPanel'
import ColorPicker from '../../../components/ColorPicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import {
  Switch,
  Checkbox,
  FormGroup,
  FormControlLabel
} from '@material-ui/core'
import { Radio, FormControl, FormLabel, RadioGroup } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
// useful tools
import isEmpty from '../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const AnswerEdit = ({
  answer,
  loading,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  onReload,
  onChangeItem,
  onAddRow,
  onDeleteRow,
  theme,
  basePath
}) => {
  const {
    id,
    title,
    isCriterias,
    isMembersInQuestion,
    isMembersInAnswer,
    whatMembersInQuestion,
    whatMembersInAnswer,
    variants,
    withWeight
  } = answer
  return (
    <MuiThemeProvider theme={theme}>
      <ToolPanel
        title=''
        link={basePath}
        returnButton={true}
        onReload={onReload}
        loading={loading}
      />
      <Paper className={classNames(classes.Card, classes.AnswerEdit)}>
        {/* Header */}
        <Paper className={classNames(classes.CardHeader)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='h4' className={classes.typoCardHeader}>
              {isEmpty(id) && (
                <Typography variant='caption' className={classes.iconNewWrap}>
                  <FiberNew classes={{ root: classes.iconNew }} />
                </Typography>
              )}
              Answer
            </Typography>
          </Grid>
        </Paper>
        {/* Form */}
        <form className={classes.Form} noValidate autoComplete='off'>
          <TextField
            fullWidth
            id='title'
            label='Название'
            value={isEmpty(title) ? '' : title}
            onChange={onChange('title')}
            margin='dense'
            InputProps={{
              className: classes.Input40px
            }}
            error={Boolean(error.title)}
            helperText={error.title ? error.title : null}
          />
          <Paper className={classes.PaperSelectLinkAnswer} square>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Switch
                    checked={Boolean(isCriterias)}
                    onChange={onChange('checkBox')}
                    name='isCriterias'
                    value='isCriterias'
                  />
                }
                label='КРИТЕРИИ'
                classes={{
                  label: classes.LabelTargetSwitch
                }}
              />
            </FormGroup>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Switch
                    checked={Boolean(isMembersInQuestion)}
                    onChange={onChange('checkBox')}
                    name='isMembersInQuestion'
                    value='isMembersInQuestion'
                  />
                }
                label='ВОПРОС => список сотрудников...'
                classes={{
                  label: classes.LabelTargetSwitch
                }}
              />
            </FormGroup>
            {isMembersInQuestion && (
              <FormControl
                component='fieldset'
                className={classes.FormControlRadio}
              >
                <RadioGroup
                  aria-label='Gender'
                  name='whatMembersInQuestion'
                  className={classes.group}
                  value={whatMembersInQuestion}
                  onChange={onChange('whatMembersInQuestion')}
                >
                  <FormControlLabel
                    value='company'
                    control={<Radio color='primary' />}
                    label='Компании'
                  />
                  <FormControlLabel
                    value='department'
                    control={<Radio color='primary' />}
                    label='Отдела'
                  />
                </RadioGroup>
              </FormControl>
            )}
            <FormGroup row>
              <FormControlLabel
                control={
                  <Switch
                    checked={Boolean(isMembersInAnswer)}
                    onChange={onChange('checkBox')}
                    name='isMembersInAnswer'
                    value='isMembersInAnswer'
                  />
                }
                label='ОТВЕТ => список сотрудников...'
                classes={{
                  label: classes.LabelTargetSwitch
                }}
              />
            </FormGroup>
            {isMembersInAnswer && (
              <FormControl
                component='fieldset'
                className={classes.FormControlRadio}
              >
                <RadioGroup
                  aria-label='Gender'
                  name='whatMembersInAnswer'
                  className={classes.group}
                  value={whatMembersInAnswer}
                  onChange={onChange('whatMembersInAnswer')}
                >
                  <FormControlLabel
                    value='company'
                    control={<Radio color='primary' />}
                    label='Компании'
                  />
                  <FormControlLabel
                    value='department'
                    control={<Radio color='primary' />}
                    label='Отдела'
                  />
                </RadioGroup>
              </FormControl>
            )}
          </Paper>
          {!isMembersInAnswer && (
            <Table className={classes.Table}>
              <TableHead>
                <TableRow className={classes.TableHeadRow}>
                  <TableCell />
                  <TableCell className={classes.TableHeadCell} width='100%'>
                    <Grid
                      container
                      direction='row'
                      wrap='nowrap'
                      justify='space-between'
                      alignItems='center'
                    >
                      <Grid item>Варианты ответа</Grid>
                      <Grid item>
                        <Typography
                          variant='caption'
                          noWrap
                          className={classNames(
                            classes.typoWithWeight,
                            withWeight ? 'is-checked' : ''
                          )}
                        >
                          with Weight
                        </Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                  <TableCell padding='checkbox'>
                    <Switch
                      checked={withWeight}
                      onChange={onChange('checkBox')}
                      name='withWeight'
                      value='withWeight'
                      classes={{ switchBase: classes.autoHeight }}
                    />
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {!isEmpty(variants) &&
                  variants.map((variant, index) => {
                    return (
                      <TableRow key={index} className={classes.TableBodyRow}>
                        <TableCell
                          className={classNames(
                            classes.removeCell,
                            'icon-remove'
                          )}
                        >
                          <RemoveCircleOutline
                            className={classes.removeIcon}
                            onClick={onDeleteRow(index)}
                          />
                        </TableCell>
                        <TableCell
                          className={classes.TableCellTitle}
                          colSpan={withWeight ? '1' : '2'}
                        >
                          <TextField
                            fullWidth
                            error={Boolean(
                              !isEmpty(error.variant) && error.variant.title
                            )}
                            className={classes.selectItem}
                            value={!isEmpty(variant.title) ? variant.title : ''}
                            onChange={onChangeItem(index)}
                            name='title'
                            InputProps={{
                              classes: {
                                underline: classNames(
                                  classes.underlineMainCardColor,
                                  isEmpty(variant.title) ? 'is-empty' : ''
                                )
                              }
                            }}
                            helperText={
                              !isEmpty(error.dishes) ? error.dishes : ''
                            }
                            FormHelperTextProps={{
                              className: classes.selectError
                            }}
                            margin='none'
                          />
                        </TableCell>
                        {withWeight && (
                          <TableCell
                            className={classes.TableCellWeight}
                            align='right'
                          >
                            <TextField
                              type='tel'
                              error={Boolean(error.weight)}
                              className={classes.selectItem}
                              value={
                                !isEmpty(variant.weight) ? variant.weight : ''
                              }
                              onChange={onChangeItem(index)}
                              name='weight'
                              InputProps={{
                                classes: {
                                  input: classes.textCenter,
                                  underline: classNames(
                                    classes.underlineMainCardColor,
                                    isEmpty(variant.title) ? 'is-empty' : ''
                                  )
                                }
                              }}
                              helperText={
                                !isEmpty(error.dishes) ? error.dishes : ''
                              }
                              FormHelperTextProps={{
                                className: classes.selectError
                              }}
                              margin='none'
                            />
                          </TableCell>
                        )}
                      </TableRow>
                    )
                  })}
                <TableRow className={classes.TableBodyRow}>
                  <TableCell
                    className={classNames(classes.CellAddButton, 'icon-add')}
                  >
                    <AddCircle className={classes.addIcon} onClick={onAddRow} />
                  </TableCell>
                  <TableCell />
                </TableRow>
              </TableBody>
            </Table>
          )}
        </form>

        <Divider />

        <Grid container className={classes.CardFooter}>
          <Grid item>
            <Fab
              size='small'
              aria-label='Delete'
              className={classes.CardButtonDelete}
              onClick={onDelete}
            >
              <Icon size={18} icon={trashO} />
            </Fab>
          </Grid>
          <Grid item>
            <Button
              to={basePath}
              component={Link}
              aria-label='Back'
              variant='contained'
            >
              Отмена
            </Button>
            <Button
              onClick={onSave}
              aria-label='Save'
              variant='contained'
              color='primary'
              className={classes.CardButtonSave}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </MuiThemeProvider>
  )
}

AnswerEdit.defaultProps = {
  answer: {},
  companies: {}
}

export default AnswerEdit
