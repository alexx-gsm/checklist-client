import { Record } from 'immutable'
import { appName } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'sidebar'

// --- ACTIONS ---
export const SIDEBAR_TOGGLE = `${appName}/${moduleName}/TOGGLE`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  isOpen: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type }) => {
  switch (type) {
    case SIDEBAR_TOGGLE:
      return state.set('isOpen', !state.isOpen)
    default:
      return state
  }
}

// --- AC ---
export const toggleSidebar = () => ({
  type: SIDEBAR_TOGGLE
})

export default reducer
