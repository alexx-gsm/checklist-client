import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
import {
  getAnswerById,
  storeAnswer,
  saveAnswer,
  updateAnswer
} from '../../../redux/modules/answer'
// Styles
import { withStyles } from '@material-ui/core/styles'
import theme from './theme'
import styles from './styles'
import CardStyles from '../../../styles/CardStyles'
import CardTableStyles from '../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../helpers/is-empty'

import AnswerEdit from './AnswerEdit'

export default compose(
  connect(
    ({ answerStore }) => {
      const { answer, error, loading } = answerStore
      return {
        answer,
        error,
        loading
      }
    },
    {
      getAnswerById,
      storeAnswer,
      saveAnswer,
      updateAnswer
    }
  ),
  withProps({
    theme
  }),
  withRouter,
  withHandlers({
    onReload: ({ getAnswerById, match }) => () =>
      getAnswerById(match.params.id),
    onChange: ({ answer, storeAnswer }) => index => event => {
      if (index === 'checkBox') {
        storeAnswer({
          ...answer,
          [event.target.name]: event.target.checked
        })
      } else {
        storeAnswer({
          ...answer,
          [index]: event.target.value
        })
      }
    },
    onChangeItem: ({ answer, storeAnswer }) => index => event => {
      storeAnswer({
        ...answer,
        variants: [
          ...answer.variants.slice(0, index),
          {
            ...answer.variants[index],
            [event.target.name]: event.target.value
          },
          ...answer.variants.slice(index + 1)
        ]
      })
    },
    onAddRow: ({ answer, storeAnswer }) => () => {
      storeAnswer({
        ...answer,
        variants: [
          ...answer.variants.filter(item => !isEmpty(item.title)),
          { title: '', weight: 0 }
        ]
      })
    },
    onDeleteRow: ({ answer, storeAnswer }) => index => () => {
      storeAnswer({
        ...answer,
        variants: [
          ...answer.variants.slice(0, index),
          ...answer.variants.slice(index + 1)
        ]
      })
    },
    onSave: ({ answer, saveAnswer, updateAnswer, history, basePath }) => () => {
      if (answer.id) {
        updateAnswer(answer, () => history.push(basePath))
      } else {
        saveAnswer(answer, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getAnswerById(id)
      }
    }
  }),
  withStyles(
    () => ({
      ...CardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(AnswerEdit)
