export default [
  {
    label: 'Surveys',
    link: ''
  },
  {
    label: 'Questions',
    link: 'questions'
  },
  {
    label: 'Criterias',
    link: 'criterias'
  },
  {
    label: 'Answers',
    link: 'answers'
  }
]
