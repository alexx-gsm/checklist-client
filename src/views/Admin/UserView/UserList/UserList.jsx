import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// @material-ui components
import { Grid, Typography, Button } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
import TopPanel from '../../../../components/TopPanel'

const UserList = ({
  users,
  basePath,
  loading,
  editHandler,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <TopPanel title='Пользователи' />
            <List>
              {Object.keys(users).map(key => {
                const user = users[key]
                return (
                  <ListItem
                    key={user.id}
                    button
                    divider
                    onClick={editHandler(key)}
                  >
                    <ListItemText primary={user.name} secondary={user.email} />
                  </ListItem>
                )
              })}
            </List>
            <Grid className={classes.MT20}>
              <Button
                variant='outlined'
                color='secondary'
                to={`${basePath}/edit`}
                component={Link}
              >
                Добавить
              </Button>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

UserList.defaultProps = {
  users: {}
}

UserList.propTypes = {
  users: PropTypes.object,
  loading: PropTypes.bool,
  handleEdit: PropTypes.func,
  classes: PropTypes.object
}

export default UserList
