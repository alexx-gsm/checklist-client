import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
import theme from './theme'

const SurveyList = lazy(() => import('./SurveyList'))
const SurveyRun = lazy(() => import('./SurveyRun'))

const basePath = '/home/surveys'

const GroupView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      exact
      path={`${basePath}`}
      render={() => <SurveyList basePath={'/survey'} theme={theme} />}
    />
    <Route
      path={`${basePath}/:id`}
      render={() => <SurveyRun basePath={'/survey'} theme={theme} />}
    />
  </Suspense>
)

export default GroupView
