import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import ToolPanel from '../ToolPanel'
import { Paper, Grid, Typography } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'

const COLOR = 'blue'

const Users = ({ staffs, loading, onReload, onClick, classes, error }) => {
  return (
    <div>
      <ToolPanel
        title=''
        color={COLOR}
        link='/admin/users'
        loading={loading}
        onReload={onReload}
      />

      <Paper className={classes.Card}>
        <Paper className={classNames(classes.CardHeader, COLOR)}>
          <Grid container className={classes.gridCardHeader}>
            <Typography variant='display1' className={classes.typoCardHeader}>
              Сотрудники
            </Typography>
          </Grid>
        </Paper>
        <Paper className={classes.Table}>
          <Grid container className={classes.gridTable}>
            <Table>
              <TableHead className={classes.TableHead}>
                <TableRow>
                  <TableCell
                    className={classes.headCellData}
                    padding='checkbox'
                  >
                    <Typography variant='caption'>ФИО</Typography>
                  </TableCell>

                  <TableCell
                    numeric
                    padding='checkbox'
                    className={classes.headerCellInfo}
                  >
                    Телефон
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(staffs).map(key => {
                  return (
                    <TableRow
                      key={key}
                      className={classes.TableRow}
                      onClick={onClick}
                    >
                      <TableCell
                        className={classes.cellData}
                        padding='checkbox'
                      >
                        <Typography variant='title'>{key}</Typography>
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Paper>
      </Paper>
    </div>
  )
}

Users.defaultProps = {
  staffs: {}
}

Users.propTypes = {
  staffs: PropTypes.object
}

export default Users
