import { compose, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { storeStaff } from '../../../redux/modules/staff'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import UserEdit from './UserEdit'

export default compose(
  connect(
    ({ staffStore }) => {
      const { staff, error, loading } = staffStore
      return {
        staff,
        error,
        loading
      }
    },
    { storeStaff }
  ),
  withHandlers({
    onChange: ({ staff, storeStaff }) => index => event => {
      console.log('index: ', index)
      storeStaff({
        ...staff,
        [index]: event.target.value
      })
    }
  }),
  withStyles(styles, { theme: true })
)(UserEdit)
