import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
import { getQtypes, clearQtype } from '../../../redux/modules/qtype'
// Material UI
import { withStyles } from '@material-ui/core'
// styles
import styles from './styles'

import QuestionTypeList from './QuestionTypeList'

export default compose(
  connect(
    ({ qtypeStore }) => {
      const { qtypes, loading, error } = qtypeStore
      return {
        qtypes,
        loading,
        error
      }
    },
    { getQtypes, clearQtype }
  ),
  withRouter,
  withState('anchorEl', 'updateAnchorEl', null),
  withHandlers({
    onReload: ({ getQtypes }) => () => getQtypes(),
    editHandler: ({ updateAnchorEl, history, basePath }) => id => () => {
      updateAnchorEl(null)
      history.push(`${basePath}/edit/${id}`)
    },
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.clearQtype()
      this.props.getQtypes()
    }
  }),
  withStyles(styles, { withTheme: true })
)(QuestionTypeList)
